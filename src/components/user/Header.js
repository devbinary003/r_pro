import React from 'react';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID} from '../../constants';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import profileImg from '../../images/profile_img.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Header extends React.Component {

  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        firstname:'',
        lastname:'',
        fullname:'',
        phone:'',
        email:'',
        profilepic:'',
        isprofilecomplete: '',
        signup_success:false,
        isactivationcomplete: '',
        locationCount: '',
        number_of_locations: '',
        reg_step_4: '',
        business_name:'',
    }

  handleClick() {
    toast.error("Welcome to Review Pro Solutions. Please first provide us some quick information about your business", {
              position: toast.POSITION.BOTTOM_RIGHT
            });
  }
    
  componentDidMount() {

    getUserDeatils()
      .then(res => {
          if(res.status===true){
              var userdata = res.data;
              this.setState({
                firstname: userdata.firstname,
                lastname: userdata.lastname,
                fullname: userdata.fullname,
                phone: userdata.phone,
                email: userdata.email,
                profilepic: userdata.profilepic,
                isprofilecomplete: userdata.isprofilecomplete,
                isactivationcomplete: userdata.isactivationcomplete,
                number_of_locations: userdata.number_of_locations,
                reg_step_4: userdata.reg_step_4,
                locationCount: res.locationCount,
                business_name: res.business_name,
              });
              this.setState({ forwarduId: res.data.id}); 
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });

  }

    render() {
      let remaningLocationMsg = ''; 
      let remaningLocation = this.state.number_of_locations - this.state.locationCount;
      if(remaningLocation > 1) {
        remaningLocationMsg = remaningLocation+' more locations';
      } else {
        remaningLocationMsg = remaningLocation+' more location';
      }
      let user_profile_section='';
      /*if (this.state.profilepic!='') {
        let user_profile_url = ADMIN_IMAGE_URL+'uploads/userprofileimage/thumbs/'+this.state.profilepic;
        user_profile_section = <img src={user_profile_url} className="dynamic_profile_pic"/>;
      } else {*/
        user_profile_section = <img src={profileImg} alt="User Profile"/>;
      /*}*/
      
      return (
        <>
          <div className="dashbord_header"> 
          <div className="dashbord_container">
            <ul className="dashbord_logo">
              <li>{/*<img src={dbImg} />*/}</li>
              <li className="dropdown pull-right">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  {user_profile_section}{this.state.business_name}
                </a>
              { localStorage.getItem(USER_ROLE)=='2' 
                ?
                  this.state.isprofilecomplete == 1 ?
                  (<div className="dropdown-menu">
                    <a className="dropdown-item" href={`/accountsettings`}> <i className="fa fa-user-circle-o"></i> Account Settings</a>
                    <a className="dropdown-item" href={`/systemsettings`}> <i className="fa fa-gear"></i> System Settings</a>
                    <a className="dropdown-item" href={`/logout`}> <i className="fa fa-power-off"></i> Logout</a>
                  </div>)
                  :
                  (<div className="dropdown-menu">
                    <a onClick={this.handleClick} className="dropdown-item" href="#"> <i className="fa fa-user-circle-o"></i> Account Settings</a>
                    <a onClick={this.handleClick} className="dropdown-item" href="#"> <i className="fa fa-gear"></i> System Settings</a>
                    <a className="dropdown-item" href={`/logout`}> <i className="fa fa-power-off"></i> Logout</a>
                  </div>)
                :
                  <div className="dropdown-menu">
                    {/*<a className="dropdown-item" href={`/accountsettings`}> <i className="fa fa-user-circle-o"></i> Account Settings</a>
                    <a className="dropdown-item" href={`/systemsettings`}> <i className="fa fa-gear"></i> System Settings</a>*/}
                    <a className="dropdown-item" href={`/logout`}> <i className="fa fa-power-off"></i> Logout</a>
                  </div>
              }



              </li>
            </ul>
          </div>
        </div>
        { this.state.isactivationcomplete != 1 ?
          (<div className="alert alert-info header_alert_msg">
            Your account verification is not completed. Please check your email to verify your account. 
          </div>)
          :
          <></>
        }
        { this.state.reg_step_4 != '' && this.state.number_of_locations > this.state.locationCount ?
          (<div className="alert alert-info header_alert_msg">
            You can add {remaningLocationMsg}. Please click here to <a href="/addlocation">Add location</a>
          </div>)
          :
          <></>
        }
        </>
        );
    }
}

export default Header;
