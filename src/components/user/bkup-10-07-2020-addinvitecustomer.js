import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';


const AddNewInviteCustomer = (data) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/invitecustomer/addinvitecustomer', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
const getallBusinessLocation = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getallbusinesslocation', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class addstaff extends React.Component {
  state = {
        fields: {},
        errors: {},
        invite_customer_business_location: '',
        invite_customer_name: '',
        invite_customer_email: '',
        invite_customer_description: '',
        business_location: [],

        signup_success: false,
        disableBaddr: false,
        disableBemail: false,
        
        disablePurl: false,
        disablePurls: false,
        copied: false,
        Loading: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      /*if (!this.state.invite_customer_business_location) {
        formIsValid = false;
        errors["invite_customer_business_location"] = "*Please select your business location.";
      }*/

      if (!this.state.invite_customer_name) {
        formIsValid = false;
        errors["invite_customer_name"] = "*Please enter name.";
      }

      if (!this.state.invite_customer_email) {
        formIsValid = false;
        errors["invite_customer_email"] = "*Please enter email address.";
      }

      if (typeof this.state.invite_customer_email !== "undefined") {
        if (!this.state.invite_customer_email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["invite_customer_email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.invite_customer_description) {
        formIsValid = false;
        errors["invite_customer_description"] = "*Please enter description.";
      }
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.setState({ Loading: true }); 
      this.savedata();
    }
    
}

savedata = () => {
  const data = {
      businesslocation_id: this.state.invite_customer_business_location,
      customer_name:this.state.invite_customer_name,
      customer_email:this.state.invite_customer_email,
      user_id: localStorage.getItem(USER_ID),
      customer_desc: this.state.invite_customer_description,
  }
  AddNewInviteCustomer(data)
      .then(res => {
          if (res.status==true) {
              this.setState({
              // forwarduId: res.data.user_id,
               signup_success: true,
              });
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }

  refreshBusinessLocationListing = () => {
    const data = {
        user_id: localStorage.getItem(USER_ID),
    }
    getallBusinessLocation(data)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ business_location: records });
          } else {
            this.setState({ business_location: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }  
    
  diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }
    
  componentDidMount() {
    this.refreshBusinessLocationListing();
  }

render(){

const {Loading} = this.state;

  
return (

  <div className="dashboard_body">
    <Route component={Header} />
    <div className="container-fluid useflex">
      <Route component={StaffLeftsidebar} />
      <div className="content_body">
        <div className="row">
          <div className="col-md-12">
            <form className="needs-validation step_form mb-4" >
              <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center">Add Invite Customer</h1>                  
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <select className="form-control" id="invite_customer_business_location" value={this.state.invite_customer_business_location} onChange={this.handleChange} name="invite_customer_business_location">
                      <option value="">Select Your Business Location</option>
                      {
                        this.state.business_location.length > 0
                      ?  
                        this.state.business_location.map(business_location_data => {
                          return(
                            <option value={business_location_data.id}>{business_location_data.find_business_location}</option>
                          )
                        })
                      :
                        <option></option>
                      }
                    </select>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="invite_customer_name" name="invite_customer_name" placeholder="Enter Name" value={this.state.invite_customer_name} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.invite_customer_name}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="invite_customer_email" name="invite_customer_email" placeholder="Enter Email" value={this.state.invite_customer_email} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.invite_customer_email}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <textarea className="form-control input_custom_style" id="invite_customer_description" name="invite_customer_description" placeholder="Enter description" value={this.state.invite_customer_description} onChange={this.handleChange}></textarea>
                    <span style={alertStyle}>{this.state.errors.invite_customer_description}</span>
                  </div>
                </div>
              </div>
              <div className="text-center">
                <button type="button" className="blue_btn_box mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Add Invite Customer</button>
              </div>
            </form>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
      </div>
    <Route component={Footer} />
  </div>
  <ToastContainer autoClose={5000} />
</div>
  );
 }
}
export default addstaff;
