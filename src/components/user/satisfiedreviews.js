import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import StarRatingComponent from 'react-star-rating-component';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import ReadMoreAndLess from 'react-read-more-less';
import Popup from "reactjs-popup";

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import LivloadngImg from '../../images/lvrlbilling.gif';

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getDefInternalreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/satisfiedreviewsdef/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getInternalreviews = (id, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/satisfiedreviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class satisfiedreviews extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        reviews: [],
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        reviewer_name:'',
        reviewer_email:'',
        reviewer_rating:'',
        reviewer_description:'',
        is_complete:'',
        business_name:'',
        business_address:'',
        business_review_link:'',
        activePage: 1,
        enableShdo: false,
    }

handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshReviewsList(pageNumber);
  }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshDefReviewsList(tValue);
      }
  }    

refreshBlList = (frwdBlid) => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          if(frwdBlid!=''){
            this.setState({ businesslocationlists: res.data, blId: frwdBlid, blIdnew: frwdBlid });
            this.refreshSingleBlDeatils(frwdBlid);
            this.refreshDefReviewsList(frwdBlid);
          } else {
            this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
            this.refreshSingleBlDeatils(res.data[0].id);
            this.refreshDefReviewsList(res.data[0].id);
          }
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    }  

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshDefReviewsList = (id) => {
  this.setState({ enableShdo: true, });
  getDefInternalreviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ reviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
        } else {
          this.setState({ reviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshReviewsList = (page) => {
    this.setState({ enableShdo: true, });
    var id = this.state.blId;
    const data = {
        page: page,
    }
  getInternalreviews(id, data)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ reviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
        } else {
          this.setState({ reviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }  

  componentDidMount() {
    if(this.props.match.params.id) {
      var frwdBlid = this.props.match.params.id;
      this.setState({ dbBlid: this.props.match.params.id, });
    } else {
      var frwdBlid = '';
      this.setState({ dbBlid: '', });
    }
    this.refreshBlList(frwdBlid);
  }

render(){

  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }

return (
  <>
  <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12 row">
        <div className="col-sm-6">
          <h1 className="black_bk_col fontweight500 font_22 mb-3">Internal Reviews</h1>
        </div>
        <div className="col-sm-6">
          <ul className="nav nav-tabs pull-right">
            <li className="nav-item">
              <a className="nav-link" href={'/internalreviews/' + this.state.blId}>All</a>
            </li>
            <li className="nav-item">
              <a className="nav-link active" href="#menu1">Satisfied</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href={'/unsatisfiedreviews/' + this.state.blId}>Unsatisfied</a>
            </li>
          </ul>
        </div>
      </div>

      <div className="col-sm-12 row">
        <div className="btn-group">
          <button type="button" className="btn left_btn">Select business location</button>
          <div className="btn-group">
            <select name="blIdnew" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100">
              {
              this.state.businesslocationlists.length > 0
              ?  
              this.state.businesslocationlists.map(bllist => {
              return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
              })
              :
              <option> Select Business Location</option>
              }
            </select> 
          </div>
        </div>
      </div>

      <div className="col-sm-12 tab-content">
        <div id="menu1" className="tab-pane active"><br />
          <table className="table table-borderless">
            <thead className="border_bottom">
              <tr>
                <th className="font_12 heading_col fontweight700 wth_20">FULL NAME</th>
                <th className="font_12 heading_col fontweight700 wth_5">EMAIL</th>
                <th className="font_12 heading_col fontweight700 wth_60">REVIEW</th>
                <th className="font_12 heading_col fontweight700 wth_15">RATING</th>
              </tr>
            </thead>
            <tbody>

            {
              this.state.reviews.length > 0
              ?  
              this.state.reviews.map(review => {
              if(review.is_complete == 1) { 
              return <tr className="highLightedtr">
                <td className="font_12 txt_col_drk fontweight_normal wth_20">{review.reviewer_name}</td>
                 <td className="font_12 txt_col fontweight400 wth_5">
                 <Popup trigger={<i style={{cursor:'pointer'}} class="fa fa-envelope" aria-hidden="true" data-toggle="tooltip" data-placement="top" title={review.reviewer_email}></i>} position="top center">
                  <div>{review.reviewer_email}</div>
                  </Popup>
                 </td>
                <td className="font_12 txt_col_drk fontweight_normal wth_60">
                    <ReadMoreAndLess
                        ref={this.ReadMore}
                        className="read-more-content"
                        charLimit={80}
                        readMoreText="Read more"
                        readLessText="Read less"
                        >
                      {review.reviewer_description}
                    </ReadMoreAndLess>
                </td>
                <td className="wth_15">
                <StarRatingComponent name="rate2" editing={false} starCount={5} value={review.reviewer_rating} />
                </td>
              </tr>
              } else {
              return <tr>
                <td className="font_12 txt_col_drk fontweight_normal wth_20">{review.reviewer_name}</td>
                 <td className="font_12 txt_col fontweight400 wth_5">
                 <Popup trigger={<i style={{cursor:'pointer'}} class="fa fa-envelope" aria-hidden="true" data-toggle="tooltip" data-placement="top" title={review.reviewer_email}></i>} position="top center">
                  <div>{review.reviewer_email}</div>
                  </Popup>
                 </td>
                <td className="font_12 txt_col_drk fontweight_normal wth_60">
                    <ReadMoreAndLess
                        ref={this.ReadMore}
                        className="read-more-content"
                        charLimit={80}
                        readMoreText="Read more"
                        readLessText="Read less"
                        >
                      {review.reviewer_description}
                    </ReadMoreAndLess>
                </td>
                <td className="wth_15">
                 <StarRatingComponent name="rate2" editing={false} starCount={5} value={review.reviewer_rating} />
                </td>
              </tr>
               }
              })
              :
              <tr>
                <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 ">{this.state.enableShdo ? <img src={LivloadngImg} />  : ''} {this.state.total == '0' ? 'There are currently no Internal Reviews.' : ''}</td>
              </tr>
              }
              
            </tbody>
          </table>

        { pageNumbers.length > 1 ?

          <Pagination
          activePage={this.state.activePage}
          totalItemsCount={this.state.total}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange.bind(this)}
          />

        : ''
        }      
        </div>
        
      </div>

      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default satisfiedreviews;
