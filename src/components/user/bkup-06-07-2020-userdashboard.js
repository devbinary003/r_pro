import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import { SketchPicker } from 'react-color';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getLiveRating = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/liverating/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getTopLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/toplivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/all/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const updateReviewLink = (data, cblid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/user/editreviewlink/'+cblid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/mylivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyInternalreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/myinternalreviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerwithmonth', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}


const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class userdashboard extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        liverating: '',
        toplivereviews: [],
        allreviews: [],
        business_review_link: '',
        reviewer_name:'',
        reviewer_email:'',
        reviewer_rating:'',
        reviewer_description:'',
        business_name:'',
        business_address:'',
        fields: {},
        errors: {},
        copied: false,
        searchreview: '',
        enableShdoLive: false,
        totalLive: '',
        enableShdoIntAll: false,
        totalIntAll: '',
        invitecustomer:[],
        invitecustomercount:'',
        reviewgivencount:'',
    }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshLiveRating(tValue);
          this.refreshTopLiveReviews(tValue);
          this.refreshAllReviews(tValue);
      }
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter your business review link.";
      }

      if (typeof this.state.business_review_link !== "undefined") {
        if (!this.state.business_review_link.match(/^[A-Za-z_-][A-Za-z0-9_-]*$/)) {
          formIsValid = false;
          errors["business_review_link"] = "*Please enter alphanumeric, dash and underscore only.";
        }
      }

      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.savedata();
    }
  }

  savedata = () => {
    var cblid = this.state.blId;
    const data = {
        business_review_link: this.state.business_review_link,
    }
    updateReviewLink(data, cblid)
        .then(res => {
            if (res.status==true) {
                  toast.success(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              } else {
                  toast.error(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              }
        })
        .catch(err => {
            console.log(err);
        });
    }

  validateSearchReviewForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.searchreview) {
        formIsValid = false;
        errors["searchreview"] = "*Enter search key word.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }  
  
  validateSearchReview = event => {
    if (this.validateSearchReviewForm()==true) {
       this.handleSearchReview();
    }
  } 

  handleSearchReview = () => {
    var cblid = this.state.blId;
    var searchKey = this.state.searchreview;
    if(cblid!='' && searchKey!='') {
      window.location.href = '/search/'+ cblid +'/?query='+ searchKey;
    }
  } 

refreshBlList = () => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
          this.refreshSingleBlDeatils(res.data[0].id);
          this.refreshLiveRating(res.data[0].id);
          if(res.data[0].lrvstatus==0) {
            this.refreshmyLiveReviews(res.data[0].id);
            this.handelWrnMsg();
          } else {
            this.refreshTopLiveReviews(res.data[0].id);
          }
          this.refreshAllReviews(res.data[0].id);
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    } 

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshLiveRating = (id) => {
  getLiveRating(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ liverating: records });
        } else {
          this.setState({ liverating: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshTopLiveReviews = (id) => {
 // this.setState({ enableShdoLive: true, });
  getTopLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
      //  this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshAllReviews = (id) => {
//  this.setState({ enableShdoIntAll: true, });
  getAllreviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalIntAll: res.data.length });
          } else {
            this.setState({ totalIntAll: 0 });
          }
          this.setState({ allreviews: records });
        } else {
          this.setState({ allreviews: '' });
        }
      //  this.setState({ enableShdoIntAll: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshmyLiveReviews = (id) => {
  this.setState({ enableShdoLive: true, });
  getmyLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshmyInternalReviews = (id) => {
  this.setState({ enableShdoIntAll: true, });
  getmyInternalreviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalIntAll: res.data.length });
          } else {
            this.setState({ totalIntAll: 0 });
          }
          this.setState({ allreviews: records });
        } else {
          this.setState({ allreviews: '' });
        }
        this.setState({ enableShdoIntAll: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshgetAllInviteCustomerWithMonth = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        this.setState({ invitecustomer: records });
      } else {
        this.setState({ invitecustomer: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }

handelWrnMsg() {
    if(this.state.enableShdoLive==true) {
     window.WrnMsg();
    } else {
    }
}

constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
  
componentDidMount() {
    window.scrollTo(0, 0);
    this.refreshBlList();
    this.refreshgetAllInviteCustomerWithMonth();
    this.refreshgetAllInviteCustomerReviewGivenCount();
  }

  render(){
    const column_options = {
      animationEnabled: true,
      exportEnabled: false,
      title: {
        text: "Invite Customer and Given Reviews Report",
        fontFamily: "verdana"
      },
      axisY: {
        //title: "in Eur",
        //prefix: "€",
        //suffix: "k"
      },
      toolTip: {
        shared: true,
        reversed: true
      },
      legend: {
        verticalAlign: "center",
        horizontalAlign: "right",
        reversed: true,
        cursor: "pointer",
        itemclick: this.toggleDataSeries
      },
      data: [
      {
        type: "stackedColumn",
        name: "Invite Customer",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
          { label: "Jan", y: 14 },
          { label: "Feb", y: 12 },
          { label: "Mar", y: 14 },
          { label: "Apr", y: 13 },
          { label: "May", y: 13 },
          { label: "Jun", y: 13 },
          { label: "Jul", y: 14 },
          { label: "Aug", y: 14 },
          { label: "Sept", y: 13 },
          { label: "Oct", y: 14 },
          { label: "Nov", y: 14 },
          { label: "Dec", y: 14 }
        ]
      },
      {
        type: "stackedColumn",
        name: "Review Given",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
          { label: "Jan", y: 13 },
          { label: "Feb", y: 13 },
          { label: "Mar", y: 15 },
          { label: "Apr", y: 16 },
          { label: "May", y: 17 },
          { label: "Jun", y: 17 },
          { label: "Jul", y: 18 },
          { label: "Aug", y: 18 },
          { label: "Sept", y: 17 },
          { label: "Oct", y: 18 },
          { label: "Nov", y: 18 },
          { label: "Dec", y: 18 }
        ]
      }]
    }
    const pie_options = {
      animationEnabled: true,
      title: {
        text: "Invite Customer and Given Reviews Report"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "center",
        fontSize: 35,
        dockInsidePlotArea: true
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}",
        yValueFormatString: "#,###",
        dataPoints: [
          { name: "Invite Customer", y: this.state.invitecustomercount },
          { name: "Review Given", y: this.state.reviewgivencount }
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    return(
      
  <div className="dashboard_body">

  <Route component={Header} />

  <div className="container-fluid useflex">

    <Route component={Leftsidebar} />

    <div className="content_body">
      <h1 className="black_bk_col fontweight500 font_22 mb-3">Business Dashboard</h1>
      <div className="row chart_section">
        <div className="col-8 left_chart_section">
          <CanvasJSChart options = {column_options} 
            /* onRef={ref => this.chart = ref} */
          />
        </div>
        <div className="col-4" right_chart_section>
          <CanvasJSChart options = {pie_options} 
            /* onRef={ref => this.chart = ref} */
          />
        </div>
      </div>
      <div className="box_row mb-4">
        <div className="box_column frst">
          <div className="row m-0 height_100">
            <div className="col-1 box_icon">
              <i className="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div className="col-11 box_txt">
              <p className="font_12 heading_col fontweight700">Select Business Location</p>
              <select name="blIdnew" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100">
              {
              this.state.businesslocationlists.length > 0
              ?  
              this.state.businesslocationlists.map(bllist => {
              return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
              })
              :
              <option> Select Business Location</option>
              }
              
              </select>  
            </div>
          </div>
        </div>
        <div className="box_column review">
          <div className="row box_txt border_radius_20 m-0 height_100">
            <div className="col-10">
              <p className="font_12 heading_col fontweight700">Search Reviews</p>
              <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100" />
              <p style={alertStyle}>{this.state.errors.searchreview}</p>
            </div>
            <div className="col-2 search box_icon">
              <i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i> 
            </div>
          </div>
        </div>
        <div className="box_column rating">
          <div className="row m-0 height_100">
            <div className="col-2 box_icon">
              <i className="fa fa-star-o" aria-hidden="true"></i>
            </div>
            <div className="col-10 box_txt">
              <p className="font_12 heading_col fontweight700 mb-0">Live Google Rating</p>
              <ul className="ratting_starr">
                <StarRatingComponent name="rate1" editing={false} starCount={5} value={this.state.liverating} />
              </ul>
              <h6 className="star_rating font_22 yellow_bk_col fontweight400 m-0">{this.state.liverating}</h6>
            </div>
          </div>
        </div>
      </div>
      <div className="your_review mb-3">
        <div className="row align-items-center">
        
          <div className="col-md-3">
            <h2 className="font_12 heading_col fontweight700 ">Your ReviewThisCompany Link</h2>
            <p className="font_12 txt_col fontweight700 ">Share it to grow your reviews!</p>
          </div>
          <div className="col-md-6">
            <div className="review_business_link">
              <span className="green_input green_span rvlnkinput">{PROFILE_URL+this.state.business_review_link}</span>
            { /* <input type="text" className="green_input" name="business_review_link" value={this.state.business_review_link} onChange={this.handleChange} />*/}
            </div>
            <p style={alertStyle}>{this.state.errors.business_review_link}</p>
          </div>
          <div className="col-md-3">
         
          <CopyToClipboard text={PROFILE_URL+this.state.business_review_link}
            onCopy={this.diasbleCopyMsg.bind(this)}>
            <a className="inline_border_btn mr-1" style={{cursor:'pointer'}}>Copy</a>
          </CopyToClipboard>      

        { /* <a onClick={this.ValidateBasic} style={{cursor:'pointer'}} className="inline_border_btn mr-3">Update Link</a> */ }
          
          <p style={alertStyle}></p>
          
          {this.state.copied ? <p className="myCopytxt" style={{color: 'green'}}>Copied.</p> : null}

          </div>
        
        </div>
      </div>

      <div className="your_review latest_reviews">
        <div className="row">
          <div className="col-md-12">
            <div className="row">
              <div className="col-md-6">
                <h3 className="font_12 heading_col fontweight700 ">Latest Live Reviews</h3>
              </div>
              <div className="col-md-6 text-right">
                <a onClick={this.refreshmyLiveReviews.bind(this, this.state.blId)} href="javascript:void(0)" className="inline_comndb_border_btn"> Download Latest Reviews</a>
              </div>
            </div>
            <table className="table table-borderless">
              <thead className="border_bottom">
                <tr>
                  <th className="font_12 heading_col fontweight700 wth_15">FULL NAME</th>
                  <th className="font_12 heading_col fontweight700 wth_15">DATE</th>
                  <th className="font_12 heading_col fontweight700 wth_55">REVIEW</th>
                  <th className="font_12 heading_col fontweight700 wth_15">RATING</th>
                </tr>
              </thead>
              <tbody>
              {
              this.state.toplivereviews.length > 0
              ?  
              this.state.toplivereviews.map(toplivereview => {
              return <tr>
                <td className="font_12 txt_col_drk fontweight_normal wth_15">{toplivereview.live_review_reviewer}</td>
                <td className="font_12 txt_col fontweight400 wth_15">
                <Moment format="MM/DD/YYYY">
                 {toplivereview.live_review_datetime}
                </Moment>
                </td>
                <td className="font_12 txt_col_drk fontweight_normal wth_55">
                    <ReadMoreAndLess
                        ref={this.ReadMore}
                        className="read-more-content"
                        charLimit={70}
                        readMoreText="Read more"
                        readLessText="Read less"
                        >
                      {toplivereview.live_review_text}
                    </ReadMoreAndLess>
                </td>
                <td className="wth_15">
                 <StarRatingComponent name="rate2" editing={false} starCount={5} value={toplivereview.live_review_rating} />
                </td>
              </tr>
              })
              :
              <tr>
                <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
              </tr>
              }
              </tbody>
            </table>
            <div className="row">
              <div className="col-md-6">
                <a href={'/livereviews/' + this.state.blId} className="refresh_btn font_12 fontweight400 green_col"> See all reviews <i className="fa fa-long-arrow-right" aria-hidden="true"></i> </a>
              </div>
              <div className="col-md-6 text-right">
              </div>
            </div>
          </div>
        </div>
      </div>


  <div className="your_review latest_reviews mt-3">
    <div className="row">
    <div className="col-md-12">

      <div className="row mt-3">
        <div className="col-sm-6">
          <h1 className="font_12 heading_col fontweight700 ">Internal Reviews</h1>
        </div>
        <div className="col-md-6 text-right">
          <a onClick={this.refreshmyInternalReviews.bind(this, this.state.blId)} href="javascript:void(0)" className="inline_comndb_border_btn"> Download Latest Reviews</a>
        </div>
      </div>

          <table className="table table-borderless">
            <thead className="border_bottom">
              <tr>
                <th className="font_12 heading_col fontweight700 wth_20">FULL NAME</th>
                <th className="font_12 heading_col fontweight700 wth_5">EMAIL</th>
                <th className="font_12 heading_col fontweight700 wth_60">REVIEW</th>
                <th className="font_12 heading_col fontweight700 wth_15">RATING</th>
              </tr>
            </thead>
            <tbody>
             {
              this.state.allreviews.length > 0
              ?  
              this.state.allreviews.map(allreview => {
              return <tr>
                <td className="font_12 txt_col_drk fontweight_normal wth_20">{allreview.reviewer_name}</td>
                <td className="font_12 txt_col fontweight400 wth_5">
                <Popup trigger={<i style={{cursor:'pointer'}} class="fa fa-envelope" aria-hidden="true" data-toggle="tooltip" data-placement="top" title={allreview.reviewer_email}></i>} position="top center">
                  <div>{allreview.reviewer_email}</div>
                  </Popup>
                </td>
                
                  <td className="font_12 txt_col_drk fontweight_normal wth_60">
                   <ReadMoreAndLess
                        ref={this.ReadMore}
                        className="read-more-content"
                        charLimit={80}
                        readMoreText="Read more"
                        readLessText="Read less"
                        >
                      {allreview.reviewer_description}
                    </ReadMoreAndLess>
                  </td>

                <td className="wth_15">
                <StarRatingComponent name="rate2" editing={false} starCount={5} value={allreview.reviewer_rating} />
                </td>
              </tr>
              })
              :
              <tr>
              <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalIntAll == '0' ? 'There are currently no Internal Reviews.' : ''}</td>
              </tr>
              }
            </tbody>
          </table>

          <div className="row">
            <div className="col-md-6">
              <a href={'/internalreviews/' + this.state.blId} className="refresh_btn font_12 fontweight400 green_col"> See all reviews <i className="fa fa-long-arrow-right" aria-hidden="true"></i> </a>
            </div>
            <div className="col-md-6 text-right">
            </div>
          </div>

       </div>   
      </div>
     </div>


      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>

    </div>

    <Route component={Footer} /> 

  </div>
 
 <ToastContainer autoClose={5000} />
<div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
<div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>    
</div>
    
    );

  }
}

export default userdashboard;
