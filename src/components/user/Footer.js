import React from 'react';

class Footer extends React.Component {

    render() {

        return (  
        <>  
        <footer className="dashboard_footer"></footer>
        <div className="modal" id="gethelpModal">
		  <div className="modal-dialog">
		    <div className="modal-content">
		   
		      <div className="modal-header">
		        <h4 className="modal-title">Get Help</h4>
		        <button type="button" className="close" data-dismiss="modal">&times;</button>
		      </div>
		     
		      <div className="modal-body">
		        To get help from live agent for any of your concerns, please click on the messenger icon in the bottom right corner. Otherwise, feel free to contact us anytime at <a href="mailto:support@reviewgrowth.com">support@reviewgrowth.com</a>. Thank you!
		      </div>

		      <div className="modal-footer"></div>

		    </div>
		  </div>
		</div>
        </>
        );
    }
}

export default Footer;
