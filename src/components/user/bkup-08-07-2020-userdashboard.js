import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import { SketchPicker } from 'react-color';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import graphImg from '../../images/reviewpro/graph.jpg';
import graphImgTwo from '../../images/reviewpro/graph-2.jpg';
import googleLogo from '../../images/reviewpro/google-logo.png';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getLiveRating = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/liverating/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getTopLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/toplivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/all/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const updateReviewLink = (data, cblid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/user/editreviewlink/'+cblid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/mylivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyInternalreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/myinternalreviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerwithmonth', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const getAverageLiveReviewRating = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/review/averagelivereviewsrating', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}


const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class userdashboard extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        liverating: '',
        toplivereviews: [],
        allreviews: [],
        business_review_link: '',
        reviewer_name:'',
        reviewer_email:'',
        reviewer_rating:'',
        reviewer_description:'',
        business_name:'',
        business_address:'',
        fields: {},
        errors: {},
        copied: false,
        searchreview: '',
        enableShdoLive: false,
        totalLive: '',
        enableShdoIntAll: false,
        totalIntAll: '',
        invitecustomer:[],
        invitecustomercount:'',
        average_live_review_rating:'',
        reviewgivencount:'',
        total_live_review_rating:'',
    }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshLiveRating(tValue);
          this.refreshTopLiveReviews(tValue);
          this.refreshAllReviews(tValue);
      }
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter your business review link.";
      }

      if (typeof this.state.business_review_link !== "undefined") {
        if (!this.state.business_review_link.match(/^[A-Za-z_-][A-Za-z0-9_-]*$/)) {
          formIsValid = false;
          errors["business_review_link"] = "*Please enter alphanumeric, dash and underscore only.";
        }
      }

      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.savedata();
    }
  }

  savedata = () => {
    var cblid = this.state.blId;
    const data = {
        business_review_link: this.state.business_review_link,
    }
    updateReviewLink(data, cblid)
        .then(res => {
            if (res.status==true) {
                  toast.success(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              } else {
                  toast.error(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              }
        })
        .catch(err => {
            console.log(err);
        });
    }

  validateSearchReviewForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.searchreview) {
        formIsValid = false;
        errors["searchreview"] = "*Enter search key word.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }  
  
  validateSearchReview = event => {
    if (this.validateSearchReviewForm()==true) {
       this.handleSearchReview();
    }
  } 

  handleSearchReview = () => {
    var cblid = this.state.blId;
    var searchKey = this.state.searchreview;
    if(cblid!='' && searchKey!='') {
      window.location.href = '/search/'+ cblid +'/?query='+ searchKey;
    }
  } 

refreshBlList = () => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
          this.refreshSingleBlDeatils(res.data[0].id);
          this.refreshLiveRating(res.data[0].id);
          if(res.data[0].lrvstatus==0) {
            this.refreshmyLiveReviews(res.data[0].id);
            this.handelWrnMsg();
          } else {
            this.refreshTopLiveReviews(res.data[0].id);
          }
          this.refreshAllReviews(res.data[0].id);
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    } 

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshLiveRating = (id) => {
  getLiveRating(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ liverating: records });
        } else {
          this.setState({ liverating: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshTopLiveReviews = (id) => {
 // this.setState({ enableShdoLive: true, });
  getTopLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
          console.log(this.state.toplivereviews);
        } else {
          this.setState({ toplivereviews: '' });
        }
      //  this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshAllReviews = (id) => {
//  this.setState({ enableShdoIntAll: true, });
  getAllreviews(id)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        if(res.data.length > 0) {
          this.setState({ totalIntAll: res.data.length });
        } else {
          this.setState({ totalIntAll: 0 });
        }
        this.setState({ allreviews: records });
      } else {
        this.setState({ allreviews: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
  }

refreshmyLiveReviews = (id) => {
  this.setState({ enableShdoLive: true, });
  getmyLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshmyInternalReviews = (id) => {
  this.setState({ enableShdoIntAll: true, });
  getmyInternalreviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalIntAll: res.data.length });
          } else {
            this.setState({ totalIntAll: 0 });
          }
          this.setState({ allreviews: records });
        } else {
          this.setState({ allreviews: '' });
        }
        this.setState({ enableShdoIntAll: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshgetAllInviteCustomerWithMonth = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        this.setState({ invitecustomer: records });
      } else {
        this.setState({ invitecustomer: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAverageLiveReviewRating = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAverageLiveReviewRating(data)
    .then(res => {
      if(res.status==true){
        this.setState({ average_live_review_rating: res.success });
        this.setState({ total_live_review_rating: res.total_review_rating });
      } else {
        this.setState({ average_live_review_rating: '' });
        this.setState({ total_live_review_rating: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}


diasbleCopyMsg() {
  this.setState({ copied: true });
  this.change = setTimeout(() => {
    this.setState({copied: false})
  }, 5000)
}

handelWrnMsg() {
    if(this.state.enableShdoLive==true) {
     window.WrnMsg();
    } else {
    }
}

constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
  
componentDidMount() {
  window.scrollTo(0, 0);
  this.refreshBlList();
  this.refreshgetAllInviteCustomerWithMonth();
  this.refreshgetAllInviteCustomerReviewGivenCount();
  this.refreshgetAverageLiveReviewRating();
}

  render(){
    const column_options = {
      animationEnabled: true,
      exportEnabled: false,
      title: {
        text: "Invite Customer and Given Reviews Report",
        fontFamily: "verdana"
      },
      axisY: {
        //title: "in Eur",
        //prefix: "€",
        //suffix: "k"
      },
      toolTip: {
        shared: true,
        reversed: true
      },
      legend: {
        verticalAlign: "center",
        horizontalAlign: "right",
        reversed: true,
        cursor: "pointer",
        itemclick: this.toggleDataSeries
      },
      data: [
      {
        type: "stackedColumn",
        name: "Invite Customer",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
          { label: "Jan", y: 14 },
          { label: "Feb", y: 12 },
          { label: "Mar", y: 14 },
          { label: "Apr", y: 13 },
          { label: "May", y: 13 },
          { label: "Jun", y: 13 },
          { label: "Jul", y: 14 },
          { label: "Aug", y: 14 },
          { label: "Sept", y: 13 },
          { label: "Oct", y: 14 },
          { label: "Nov", y: 14 },
          { label: "Dec", y: 14 }
        ]
      },
      {
        type: "stackedColumn",
        name: "Review Given",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
          { label: "Jan", y: 13 },
          { label: "Feb", y: 13 },
          { label: "Mar", y: 15 },
          { label: "Apr", y: 16 },
          { label: "May", y: 17 },
          { label: "Jun", y: 17 },
          { label: "Jul", y: 18 },
          { label: "Aug", y: 18 },
          { label: "Sept", y: 17 },
          { label: "Oct", y: 18 },
          { label: "Nov", y: 18 },
          { label: "Dec", y: 18 }
        ]
      }]
    }
    const average_live_review_pie_options = {
      animationEnabled: true,
      title: {
        text: "Review By Star Rating"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Average Star Rating", y: this.state.average_live_review_rating, color: "#ffdc04" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    const reviews_source_pie_options = {
      animationEnabled: true,
      title: {
        text: "Total Reviews"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Total Reviews", y: this.state.total_live_review_rating, color: "#6954bb" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    return(
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        {/*<div className="bg-color"></div>*/} 
        <div className="button-part">
          <button className="part-of-btn">All Reviews</button>
          <button className="part-of-btn">New Reviews</button>
          <select name="cars" className="part-of-btn" id="cars">
            <option value="volvo">All Sources</option>
            <option value="saab">Saab</option>
            <option value="mercedes">Mercedes</option>
            <option value="audi">Audi</option>
          </select> 
          <select name="cars" className="part-of-btn" id="cars">
            <option value="volvo">All Time</option>
            <option value="saab">3 month</option>
            <option value="mercedes">6 month</option>
            <option value="audi">12 month</option>
          </select>
          <button className="part-of-btn"> Reset</button>
        </div>
        <div className="left-part-design"> 
          <div className="container ">
            <div className="row">
              <div className="col-sm-4 shot-part">
                <h2>Overview</h2>
                <p>All-Time Rating & Reviews</p>
                <div className="average">
                  <p>Average Star Rating</p>
                  <h2>{this.state.average_live_review_rating}</h2>
                  <StarRatingComponent name="rate2" editing={false} starCount={5} value={this.state.average_live_review_rating} />
                </div>
                <div className="average part-aver">
                  <p>Total Reviews</p>
                  <h2>{this.state.total_live_review_rating}</h2> 
                </div>
              </div>
              <div className="col-sm-8 big-part">
                <div className="row">
                  <div className="col-sm-6">
                   <h2>Collected Reviews</h2> 
                  </div>
                  <div className="col-sm-6">
                   <p><a href="#">Get more reviews</a></p>
                  </div>
                </div>
                <img src={graphImg} alt="graph" className="img-responsive" />
              </div>
            </div>
          </div>
        </div>
        <div className="row chart_section">
          <div className="col-sm-6 left_chart_section graph-img">
            <CanvasJSChart options = {average_live_review_pie_options} />
          </div>
          <div className="col-sm-6 right_chart_section graph-img">
            <CanvasJSChart options = {reviews_source_pie_options} />
          </div>
        </div>
        <div className="search-part">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 search-bar">
                <div className="row">
                  <div className="col-sm-2 fa-icon"><i className="fa fa-map-marker" aria-hidden="true"></i></div>
                  <div className="col-sm-10 location">
                    <p><b>Select Business Location</b></p>
                    <select className="part-of-btn" id="cars" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange}>
                      {
                        this.state.businesslocationlists.length > 0
                        ?  
                          this.state.businesslocationlists.map(bllist => {
                            return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
                          })
                        :
                          <option> Select Business Location</option>
                      } 
                    </select> 
                  </div>
                </div>
              </div>
              <div className="col-sm-4 search-reviews">
                <div className="row">
                  <div className="col-sm-9 location-2">
                    <p><b>Search Reviews</b></p>
                    <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="ser-part" />
                    <p style={alertStyle}>{this.state.errors.searchreview}</p>
                  </div>
                  <div className="col-sm-3 fa-icon-2"><i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="table-part">
          <div className="row button-part">
            <div className="col-sm-3">
              <p>Showing 1 to 4 Results</p>
            </div>
            <div className="col-sm-9"> 
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Sources</option> 
              </select>
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Ratings</option> 
              </select>
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Time</option>
                <option value="saab">3 month</option>
                <option value="mercedes">6 month</option>
                <option value="audi">12 month</option>
              </select>
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Reset</option> 
              </select>
              <select name="cars" className="part-of-btn right" id="cars">
                <option value="volvo">CSV</option> 
              </select>
            </div>
          </div>
          <table>
            <tr>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Date</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Source</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Rating</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Review</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Status</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Actions</th>
            </tr>
              {
                this.state.toplivereviews.length > 0
                ?  
                  this.state.toplivereviews.map(toplivereview => {
                    return <tr>
                              <td>
                                <Moment format="LL">
                                  {toplivereview.live_review_datetime}
                                </Moment>
                              </td>
                              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
                              <td><StarRatingComponent name="rate2" editing={false} starCount={5} value={toplivereview.live_review_rating} /></td>
                              <td>What is Lorem Ipsum Lorem Ipsum  </td>
                              <td>Unread</td>
                              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
                          </tr>
                  })
                :
                <tr>
                  <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                </tr>
              }
            {/*<tr>
              <td>14 Aprile 2020</td>
              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
              <td><i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i></td>
              <td>What is Lorem Ipsum Lorem Ipsum  </td>
              <td>Unread</td>
              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
            </tr>
            <tr>
              <td>14 Aprile 2020</td>
              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
              <td><i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i></td>
              <td>What is Lorem Ipsum Lorem Ipsum  </td>
              <td>Unread</td>
              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
            </tr>
            <tr>
              <td>14 Aprile 2020</td>
              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
              <td><i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i></td>
              <td>What is Lorem Ipsum Lorem Ipsum  </td>
              <td>Unread</td>
              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
            </tr>
            <tr>
              <td>14 Aprile 2020</td>
              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
              <td><i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i></td>
              <td>What is Lorem Ipsum Lorem Ipsum  </td>
              <td>Unread</td>
              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
            </tr>
            <tr>
              <td>14 Aprile 2020</td>
              <td> <img src={googleLogo} alt="google-logo" className="img-responsive" /> Google</td>
              <td><i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i> <i className="fa fa-star" aria-hidden="true"></i></td>
              <td>What is Lorem Ipsum Lorem Ipsum  </td>
              <td>Unread</td>
              <td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
            </tr>*/}
          </table>
          <div className="row bot-area">
            <div className="col-sm-6 right-show"><p>Showing 1 to 4 of Result</p></div>
            <div className="col-sm-6 left-show">
              <p><a href="#">20 per page</a></p>
            </div>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div> 
        {/*<ToastContainer autoClose={5000} />
        <div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
        <div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>*/}   
      </div>
      </>
    );
  }
}

export default userdashboard;
