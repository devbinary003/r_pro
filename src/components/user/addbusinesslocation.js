import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAvAdrDeatils = (data) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/businesslocation/getautofilldetails', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const AddNewBusinessLocation = (data, uid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/businesslocation/addnewbusinesslocation/'+uid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class addbusinesslocation extends React.Component {
  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        phone: '',
        email: '',
        find_business_location: '',
        lat: '',
        lng: '',
        business_name: '',
        business_address: '',
        business_review_link: '',
        facebook_link: '',
        twitter_link: '',
        linkedin_link: '',
        instagram_link: '',
        client_satisfaction: '',

        signup_success: false,
        disableBaddr: false,
        disableBemail: false,
        
        disablePurl: false,
        disablePurls: false,
        copied: false,
        Loading: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.find_business_location) {
        formIsValid = false;
        errors["find_business_location"] = "*Please enter business name.";
      }

      if (!this.state.business_name) {
        formIsValid = false;
        errors["business_name"] = "*Please enter business name.";
      }

      if (!this.state.business_address) {
        formIsValid = false;
        errors["business_address"] = "*Please enter business address.";
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.phone) {
        formIsValid = false;
        errors["phone"] = "*Please enter phone number.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/

      if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter business slug.";
      }

      if (typeof this.state.business_review_link !== "undefined") {
        if (!this.state.business_review_link.match(/^[A-Za-z_-][A-Za-z0-9_-]*$/)) {
          formIsValid = false;
          errors["business_review_link"] = "*Please enter alphanumeric, dash and underscore only.";
        }
      }

      if (this.state.facebook_link) {
      if (typeof this.state.facebook_link !== "undefined") {
        if (!this.state.facebook_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["facebook_link"] = "*Please enter link only.";
        }
      }
    }

    if (this.state.twitter_link) {  
      if (typeof this.state.twitter_link !== "undefined") {
        if (!this.state.twitter_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["twitter_link"] = "*Please enter link only.";
        }
      }
    }  

    if (this.state.linkedin_link) {
      if (typeof this.state.linkedin_link !== "undefined") {
        if (!this.state.linkedin_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["linkedin_link"] = "*Please enter link only.";
        }
      }
    }  

    if (this.state.instagram_link) {
      if (typeof this.state.instagram_link !== "undefined") {
        if (!this.state.instagram_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["instagram_link"] = "*Please enter link only.";
        }
      }
    }  

    if (!this.state.client_satisfaction) {
      formIsValid = false;
      errors["client_satisfaction"] = "*Please select client satisfaction.";
    }
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
       if (this.validateForm()==true) {
          this.setState({ Loading: true }); 
          this.savedata();
    }
}

savedata = () => {
  var uid = localStorage.getItem(USER_ID);
  const data = {
      find_business_location: this.state.find_business_location,
      lat:this.state.lat,
      lng:this.state.lng,
      user_id: this.state.forwarduId,
      phone: this.state.phone,
      business_name: this.state.business_name,
      business_address: this.state.business_address,
      business_review_link: this.state.business_review_link,
      facebook_link: this.state.facebook_link,
      twitter_link: this.state.twitter_link,
      linkedin_link: this.state.linkedin_link,
      instagram_link: this.state.instagram_link,
      client_satisfaction: this.state.client_satisfaction,
  }
  AddNewBusinessLocation(data, uid)
      .then(res => {
          if (res.status==true) {
              this.setState({
              // forwarduId: res.data.user_id,
               signup_success: true,
              });
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }

  handleAdrChange = find_business_location => {
    this.setState({ find_business_location });
    this.setState({ errors : ''});
  };

  handleSelect = find_business_location => {
    this.setState({ find_business_location:find_business_location });
    geocodeByAddress(find_business_location)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        //  console.log('Success', latLng);
          this.setState({
              find_business_location: find_business_location, 
              lat: latLng.lat,
              lng: latLng.lng,
            });
          this.refreshAcAdr(find_business_location);
       })
      .catch(error => console.error('Error', error));
  };

refreshAcAdr = (find_business_location) => {
  const data = {
      find_business_location: find_business_location,
  }
  getAvAdrDeatils(data)
      .then(res => {
        if(res.status==true){
              this.setState({
              business_name: res.data.business_name,
              business_address: res.data.business_address,
              });
        } else {
           toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshUserDeatils = () => {
  getUserDeatils()
      .then(res => {
        if(res.status==true){
          var userdata = res.data;
              this.setState({
                forwarduId: userdata.id,
                email: userdata.email,
              });
        } else {
           /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    }
    
  diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }
    
  componentDidMount() {
    this.refreshUserDeatils();
  }

render(){

const {Loading} = this.state;

if (this.state.signup_success) return <Redirect to={'/userdashboard'} />
  
return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="row">
        <div className="col-md-12 form_section">
        <form className="needs-validation step_form mb-4" >
        
        <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center">Find your business <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></i> </h1>
        
        <div className="row">
                          <div className="col-md-12">
                            <div className="form-group">

                          <PlacesAutocomplete
                          value={this.state.find_business_location}
                          onChange={this.handleAdrChange}
                          onSelect={this.handleSelect}
                          >
                            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                              <div>
                                <input
                                  {...getInputProps({
                                    placeholder: 'Type your business name, city, zip code',
                                    className: 'form-control input_custom_style',
                                  })}
                                />
                                <div className="autocomplete-dropdown-container">
                                  {loading && <div>Loading...</div>}
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                      ? 'suggestion-item--active'
                                      : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                      : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style,
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            )}
                    </PlacesAutocomplete>
                    <span style={alertStyle}>{this.state.errors.find_business_location}</span>
                              
                            </div>
                          </div>
                        </div>

       <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Complete your business information <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></i></h1>                  
        
        <div className="row">
        <div className="col-md-12">
          <div className="form-group">
           <input type="text" className="form-control input_custom_style" id="business_name" name="business_name" placeholder="Business name" value={this.state.business_name} onChange={this.handleChange} />
           <span style={alertStyle}>{this.state.errors.business_name}</span>
          </div>
        </div>
      </div>

      <div className="row">
          <div className="col-md-12">
            <div className="form-group">

            <input disabled={!this.state.disableBaddr} type="text" className="form-control input_custom_style" id="business_address" name="business_address" placeholder="Business address" value={this.state.business_address} onChange={this.handleChange} />
            <span style={alertStyle}>{this.state.errors.business_address}</span>

            </div>
          </div>
        </div>


    <div className="row">
          <div className="col-sm-6">
            <div className="form-group">

            <input disabled={!this.state.disableBemail} type="text" className="form-control input_custom_style" id="email" name="email" placeholder="Business email" value={this.state.email} onChange={this.handleChange} />
            <span style={alertStyle}>{this.state.errors.email}</span>

            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group">

            <PhoneInput
            country="US"
            showCountrySelect={ false }
            name="phone"
            limitMaxLength={ true }
            placeholder="Enter phone number"
            className="businessPhone"
            value={ this.state.phone }
            onChange={ phone => this.setState({ phone }) } 
            />
          <span style={alertStyle}>{this.state.errors.phone}</span>
          
           { /*<input type="text" className="form-control input_custom_style" id="phone" name="phone" placeholder="Phone number" value={this.state.phone} onChange={this.handleChange}  />
                       <span style={alertStyle}>{this.state.errors.phone}</span>*/}

            </div>
          </div>
        </div>


          <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Choose your main location's Business Review Link <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></i> </h1>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <input disabled={!this.state.disablePurl} type="text" className="form-control input_custom_style" id="purl" name="purl" value={PROFILE_URL} />
                            </div>
                          </div>
                          
                          <div className="col-md-6">
                            <div className="form-group">
                          <input type="text" className="form-control input_custom_style" id="business_review_link" name="business_review_link" placeholder="business-name" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "business-name"} value={this.state.business_review_link} onChange={this.handleChange} />
                          <span style={alertStyle}>{this.state.errors.business_review_link}</span>
                            </div>
                          </div>

                        </div>
                        
                        <h1 className="black_bk_col fontweight500 font_20 mb-4 mt-3 text-center"> Choose your review requirements for satisfied customers <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></i> </h1>
                        <ul className="step_review">

                          <li className="step_review_li">
                            <div className="custom-control custom-radio text-right">

                              {this.state.client_satisfaction == '1'
                                ? <input type="radio" className="custom-control-input" id="customRadio2" name="client_satisfaction" value="1" checked onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                              : <input type="radio" className="custom-control-input" id="customRadio2" name="client_satisfaction" value="1" checked={this.state.client_satisfaction === '1'} onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                              }  

                            <label className="custom-control-label" htmlFor="customRadio2"></label>
                            </div>
                            <ul className="ratting_starr m-0">
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star-o" aria-hidden="true"></i></li>
                            </ul>
                            <p className="review_txt">4 & 5 star reviews only</p>
                          </li>

                          <li className="step_review_li">
                            <div className="custom-control custom-radio text-right">
                            
                            {this.state.client_satisfaction == '0'
                             ? <input type="radio" className="custom-control-input" id="customRadio" name="client_satisfaction" value="0" checked onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                            : <input type="radio" className="custom-control-input" id="customRadio" name="client_satisfaction" value="0" checked={this.state.client_satisfaction === '0'} onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                            }  

                           <label className="custom-control-label" htmlFor="customRadio"></label>
                            </div>
                            <ul className="ratting_starr m-0">
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <p className="review_txt">5 star reviews only</p>
                          </li>

                      <span style={alertStyle}>{this.state.errors.client_satisfaction}</span>    
                        </ul>
                        
                        <h1 className="black_bk_col fontweight500 font_20 mb-4 mt-3 text-center"> Enter your social media accounts <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></i> </h1>
                        
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-facebook" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="facebook_link" name="facebook_link" placeholder="Facebook Url" value={this.state.facebook_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.facebook_link}</span>

                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-twitter" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="twitter_link" name="twitter_link" placeholder="Twitter Url" value={this.state.twitter_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.twitter_link}</span>

                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-instagram" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="instagram_link" name="instagram_link" placeholder="Instagram Url" value={this.state.instagram_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.instagram_link}</span> 
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-linkedin" aria-hidden="true"></i></span>
                              </div>

                          <input type="text" className="form-control" id="linkedin_link" name="linkedin_link" placeholder="Linkedin Url" value={this.state.linkedin_link} onChange={this.handleChange} />
                          <span style={alertStyle}>{this.state.errors.linkedin_link}</span>

                            </div>
                          </div>
                        </div>


        <div className="text-center">
        <button type="button" className="blue_btn_box mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Add Business Location</button>
        </div>
      </form>
    </div>
    </div>
    <div>
      <MessengerCustomerChat
        pageId="415979359011219"
        appId="448749972524859"
      />
    </div>
    <ToastContainer autoClose={5000} />
  </div> 
</>
  );
 }
}

export default addbusinesslocation;
