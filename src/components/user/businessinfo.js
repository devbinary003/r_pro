import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';


const getBusinessLocation = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const AddBusinessInfo = (data, blid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/businesslocation/addbusinessinfo/'+blid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class businessinfo extends React.Component {
  state = {
        fields: {},
        errors: {},
        blId: '',
        forwarduId: '',
        firstname:'',
        lastname:'',
        fullname:'',
        phone:'',
        email:'',
        business_page_id:'',
        business_name:'',
        business_address:'',
        number_of_locations: '',
        signup_success:false,
        disableBaddr: false,
        disableBemail: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.business_name) {
        formIsValid = false;
        errors["business_name"] = "*Please enter business name.";
      }

      if (!this.state.business_address) {
        formIsValid = false;
        errors["business_address"] = "*Please enter business address.";
      }

      if (!this.state.firstname) {
        formIsValid = false;
        errors["firstname"] = "*Please enter first name.";
      }

      if (typeof this.state.firstname !== "undefined") {
        if (!this.state.firstname.match(/^[a-zA-Z ]*$/)) {
          formIsValid = false;
          errors["firstname"] = "*Please enter alphabets characters only.";
        }
      }

      if (!this.state.lastname) {
        formIsValid = false;
        errors["lastname"] = "*Please enter last name.";
      }

      if (typeof this.state.lastname !== "undefined") {
        if (!this.state.lastname.match(/^[a-zA-Z ]*$/)) {
          formIsValid = false;
          errors["lastname"] = "*Please enter alphabets characters only.";
        }
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.phone) {
        formIsValid = false;
        errors["phone"] = "*Please enter phone number.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/

      if (!this.state.number_of_locations) {
        formIsValid = false;
        errors["number_of_locations"] = "*Please select business locations.";
      }
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
       if (this.validateForm()==true) {
          this.savedata();
    }
}

savedata = () => {
  var blid = this.state.blId;
  const data = {
      user_id: this.state.forwarduId,
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      phone: this.state.phone,
      email: this.state.email,
      business_page_id: this.state.business_page_id,
      business_name: this.state.business_name,
      business_address: this.state.business_address,
      number_of_locations: this.state.number_of_locations,
  }
  AddBusinessInfo(data, blid)
      .then(res => {
          if (res.status==true) {
              this.setState({
                 blId: res.data.id,
                 forwarduId: res.data.user_id,
                 signup_success: true,
              });
              toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
      })
      .catch(err => {
          console.log(err);
      });

  }

  componentDidMount() {
    getBusinessLocation()
      .then(res => {
          if(res.status==true){
              this.setState({
                blId: res.data.id,
                forwarduId: res.data.user_id,
                business_page_id: res.data.business_page_id,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
                phone: res.data.phone,
                number_of_locations: res.user.number_of_locations,
                firstname: res.user.firstname,
                lastname: res.user.lastname,
                fullname: res.user.fullname,
                email: res.user.email,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

render(){

if (this.state.signup_success) return <Redirect to={'/customizereview'} />

return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12">
        <h1 className="black_bk_col fontweight500 font_16 mb-4 pb-1">Complete Account Sign Up</h1>
      </div>
      <div className="container">
          <div id="app">     
            <ol className="step-indicator">
               <li className="">
                  <div className="step_name">Step <span>1</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>FIND YOUR BUSINESS</span></div>
               </li>
               <li className="active">
                  <div className="step_name">Step <span>2</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BUSINESS INFORMATION</span></div>
               </li>
               <li className="">
                  <div className="step_name">Step <span>3</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>CUSTOMIZE REVIEW SYSTEM</span></div>
               </li>
               <li className="">
                  <div className="step_name">Step <span>4</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BILLING TO STRIPE</span></div>
               </li>
            </ol>

                  <div className="row step_two">
                    <div className="col-md-12">
                      <form className="needs-validation mb-4" >
                          <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Complete your business information below  </h1>
                        <div className="row">
                          <div className="col-md-12">
                            <div className="form-group">
                          <input type="text" className="form-control input_custom_style" id="business_name" name="business_name" placeholder="Business name" value={this.state.business_name} onChange={this.handleChange} />
                          <span style={alertStyle}>{this.state.errors.business_name}</span>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-12">
                            <div className="form-group">

                            <input disabled={!this.state.disableBaddr} type="text" className="form-control input_custom_style" id="business_address" name="business_address" placeholder="Business address" value={this.state.business_address} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.business_address}</span>

                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-sm-6">
                            <div className="form-group">

                            <input type="text" className="form-control input_custom_style" id="firstname" name="firstname" placeholder="First name" value={this.state.firstname} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.firstname}</span>

                            </div>
                          </div>
                          <div className="col-sm-6">
                            <div className="form-group">

                            <input type="text" className="form-control input_custom_style" id="lastname" name="lastname" placeholder="Last name" value={this.state.lastname} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.lastname}</span>

                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-sm-6">
                            <div className="form-group">

                            <input disabled={!this.state.disableBemail} type="text" className="form-control input_custom_style" id="email" name="email" placeholder="Business email" value={this.state.email} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.email}</span>

                            </div>
                          </div>
                          <div className="col-sm-6">
                            <div className="form-group">

                          <PhoneInput
                            country="US"
                            showCountrySelect={ false }
                            name="phone"
                            limitMaxLength={ true }
                            placeholder="Enter phone number"
                            className="businessPhone"
                            value={ this.state.phone }
                            onChange={ phone => this.setState({ phone }) } 
                            />
                          <span style={alertStyle}>{this.state.errors.phone}</span>
            
                          {  /*<input type="text" className="form-control input_custom_style" id="phone" name="phone" placeholder="Phone number" value={this.state.phone} onChange={this.handleChange}  />
                                                      <span style={alertStyle}>{this.state.errors.phone}</span>*/}

                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-sm-6">
                            <p className="font_14 text-black fontweight500">How many locations does your business have?</p>
                          </div>
                          <div className="col-sm-6">
                            <div className="form-group">

                          <select className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100" id="number_of_locations" name="number_of_locations" value={this.state.number_of_locations} onChange={this.handleChange}> 
                            <option value="">Select location</option>
                            <option value="1">1 Location</option>
                            <option value="2">2 Locations</option>
                            <option value="3">3 Locations</option>
                            <option value="4">4 Locations</option>
                            <option value="5">5 Locations</option>
                            {/*<option value="6">6 Locations</option>
                            <option value="7">7 Locations</option>
                            <option value="8">8 Locations</option>
                            <option value="9">9 Locations</option>
                            <option value="10">10 Locations</option>*/}
                          </select>

                          <span style={alertStyle}>{this.state.errors.number_of_locations}</span>

                            </div>
                          </div>
                        </div>
                        <div className="text-center mt-4">
                          <button type="button" className="blue_btn_box" onClick={this.ValidateBasic}>Next</button>
                          <p className="m-0"><a href={`/findbusiness`}>Back</a></p>
                        </div>
                      </form>
                    </div>
                  </div>

          </div>
      </div>
      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default businessinfo;
