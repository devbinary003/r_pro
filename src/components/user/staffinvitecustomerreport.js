import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import LivloadngImg from '../../images/lvrlbilling.gif';

const getallStaffInviteCustomerList = (data, id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/staff/getstaffinvitecustomerreportlist/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
const txtCenter = {
    textAlign: 'center',
};

class staffinvitecustomerreport extends React.Component {
    state = {
        staffInviteCustomerList: [],
        total: '',
        staffid: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
    }
  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshTopLiveReviews(pageNumber);
  }
  refreshStaffInviteCustomerListing = (id, page) => {
    const data = {
        page: page,
        user_id: localStorage.getItem(USER_ID),
    }
    getallStaffInviteCustomerList(data, id)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ staffInviteCustomerList: records.data });
            this.setState({ staffInviteCustomerList: records.data });
            this.setState({ total: res.success.total });
            this.setState({ currentPage: res.success.current_page });
            this.setState({ PerPage: res.success.per_page });
            this.setState({ FirstPageUrl: res.success.first_page_url });
            this.setState({ NextPageUrl: res.success.next_page_url });
            this.setState({ PrevPageUrl: res.success.prev_page_url });
            this.setState({ LastPageUrl: res.success.last_page_url });
            this.setState({ LastPage: res.success.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          } else {
            this.setState({ staffInviteCustomerList: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }
  componentDidMount() {
    var id = this.props.match.params.id;
    this.refreshStaffInviteCustomerListing(id);
  }  
render(){
  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }
  return (
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        <div className="col-sm-12 row">
          <div className="col-sm-6">
            <h1 className="black_bk_col fontweight500 font_22 mb-3">Invited Customer Report</h1>
          </div>
        </div>
        <div className="col-sm-12 tab-content">
          <div id="home" className="tab-pane active"><br />
            <table className="table table-borderless">
              <thead className="border_bottom">
                <tr>
                  {/*<th className="font_12 heading_col fontweight700 wth_15">ID</th>*/}
                  <th className="font_12 heading_col fontweight700 wth_15">NAME</th>
                  <th className="font_12 heading_col fontweight700 wth_15">EMAIL</th>
                  <th className="font_12 heading_col fontweight700 wth_15">REVIEW GIVEN(Yes/No)</th>
                  <th className="font_12 heading_col fontweight700 wth_15">INVITE DATE</th>
                  {/*<th className="font_12 heading_col fontweight700 wth_15">ACTION</th>*/}
                </tr>
              </thead>
              <tbody>
                {
                  this.state.staffInviteCustomerList.length > 0
                  ?  
                  this.state.staffInviteCustomerList.map(staffInviteCustomerListdata => {
                  return(
                    <tr>
                      {/*<td className="font_12 txt_col_drk fontweight_normal wth_15">{staffInviteCustomerListdata.id}</td>*/}
                      <td className="font_12 txt_col_drk fontweight_normal wth_15">{staffInviteCustomerListdata.customer_name}</td>
                      <td className="font_12 txt_col fontweight400 wth_15">{staffInviteCustomerListdata.customer_Email}</td>
                      <td className="font_12 txt_col fontweight400 wth_15">
                      {
                        staffInviteCustomerListdata.review_given == '1' 
                        ?
                         'Yes'

                        :
                        'No'
                      }
                      </td>
                      <td className="font_12 txt_col_drk fontweight_normal wth_15">
                        <Moment format="MM/DD/YYYY">
                          {staffInviteCustomerListdata.created_at}
                        </Moment>
                      </td>
                      {/*<td className="wth_15">
                        <a href={'./editstaff/' + stafflisting.id} className="btn_icon_section"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="#" className="btn_icon_section" onClick={(e) => { if (window.confirm('Are you sure you wish to delete this list ?')) this.delete(stafflisting.id)} }><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <a href="#" className="btn_icon_section" onClick={(e) => { if (window.confirm('Are you sure you wish to deactivate this account ?')) this.deactivate(stafflisting.id)} }><i class="fa fa-check" aria-hidden="true"></i></a>
                        <a href="#" className="btn_icon_section"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>
                      </td>*/}
                    </tr>
                  );
                  })
                  :
                  <tr>
                    <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.total == '0' ? 'There are currently no found.' : ''}</td>
                  </tr>
                }
              </tbody>
            </table>
            { pageNumbers.length > 1 ?
              <Pagination
              activePage={this.state.activePage}
              totalItemsCount={this.state.total}
              pageRangeDisplayed={5}
              onChange={this.handlePageChange.bind(this)}
              />
            : ''
            } 
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
        <ToastContainer autoClose={5000} />
      </div>
  </>
  );
}
}
export default staffinvitecustomerreport;
