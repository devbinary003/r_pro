import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import googleLogo from '../../images/reviewpro/google-logo.png';
import facebookLogo from '../../images/reviewpro/facebook_logo.png';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import LivloadngImg from '../../images/lvrlbilling.gif';

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getDefLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/livereviewsdef/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getTopLivereviews = (id, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/livereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/mylivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const txtCenter = {
  textAlign: 'center',
};
const alertStyle = {
  color: 'red',
};
class livereviews extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        toplivereviews: [],
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        business_name:'',
        business_address:'',
        business_review_link:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
        searchreview: '',
    }

handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshTopLiveReviews(pageNumber);
  }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshDefLiveReviews(tValue);
      }
  }    

validateSearchReviewForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.searchreview) {
        formIsValid = false;
        errors["searchreview"] = "*Enter search key word.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }  
  
  validateSearchReview = event => {
    if (this.validateSearchReviewForm()==true) {
       this.handleSearchReview();
    }
  } 

  handleSearchReview = () => {
    var cblid = this.state.blId;
    var searchKey = this.state.searchreview;
    if(cblid!='' && searchKey!='') {
      window.location.href = '/search/'+ cblid +'/?query='+ searchKey;
    }
  } 

refreshBlList = (frwdBlid) => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          if(frwdBlid!=''){
            this.setState({ businesslocationlists: res.data, blId: frwdBlid, blIdnew: frwdBlid });
            this.refreshSingleBlDeatils(frwdBlid);
            this.refreshDefLiveReviews(frwdBlid);
          } else {
            this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
            this.refreshSingleBlDeatils(res.data[0].id);
            this.refreshDefLiveReviews(res.data[0].id);
          }
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    }  

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    } 

refreshDefLiveReviews = (id) => {
  this.setState({ enableShdo: true, });
  getDefLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ toplivereviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
        } else {
          this.setState({ toplivereviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshTopLiveReviews = (page) => {
  this.setState({ enableShdo: true, });
  var id = this.state.blId;
  const data = {
      page: page,
  }
  getTopLivereviews(id, data)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ toplivereviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshmyLiveReviews = (id) => {
  this.setState({ enableShdoLive: true, });
  getmyLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ total: res.data.length });
          } else {
            this.setState({ total: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

componentDidMount() {
    if(this.props.match.params.id) {
      var frwdBlid = this.props.match.params.id;
      this.setState({ dbBlid: this.props.match.params.id, });
    } else {
      var frwdBlid = '';
      this.setState({ dbBlid: '', });
    }
    this.refreshBlList(frwdBlid);
  }        

render(){

  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }

return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      {/*<div className="bg-color"></div> */}
      <div className="button-part">
        {/*<button className="part-of-btn">All Reviews</button>
        <button className="part-of-btn">New Reviews</button> */} 
        {/*<select name="cars" className="part-of-btn" id="cars">
          <option value="volvo">All Sources</option>
          <option value="saab">Saab</option>
          <option value="mercedes">Mercedes</option>
          <option value="audi">Audi</option>
        </select>
        <select name="cars" className="part-of-btn" id="cars">
            <option value="">All Time</option>
            <option value="1">1 month</option>
            <option value="3">3 month</option>
            <option value="6">6 month</option>
            <option value="12">12 month</option>
            <option value="custom_date">Custom Dates</option>
        </select>
        <button className="part-of-btn"> Reset</button>*/}
      </div>
      <div className="search-button">
        <div className="container">
          <div className="row">
            <div className="col-sm-10">
              <div className="search-part">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-8 search-bar">
                      <div className="row">
                        <div className="col-sm-2 fa-icon"><i className="fa fa-map-marker" aria-hidden="true"></i></div>
                        <div className="col-sm-10 location">
                          <p><b>Select Business Location</b></p>
                          {/*<input type="text" name="" placeholder="Select Business Location" className="ser-part" />*/}
                          <select name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} className="ser-part" disabled="disabled" id="live_bs">
                            {
                              this.state.businesslocationlists.length > 0
                              ?  
                                this.state.businesslocationlists.map(bllist => {
                                  return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
                                })
                              :
                                <option> Select Business Location</option>
                            }
                          </select> 
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4 search-reviews">
                      <div className="row">
                        <div className="col-sm-9 location-2">
                          <p><b>Search Reviews</b></p>
                          <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="ser-part" />
                          {/*<p style={alertStyle}>{this.state.errors.searchreview}</p>*/}
                        </div>
                        <div className="col-sm-3 fa-icon-2"><i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i></div>
                      </div>
                    </div>
                   {/* <div className="col-sm-4 search-reviews">
                      <div className="row">
                        <div className="col-sm-9 location-2">
                          <p><b>Search Reviews</b></p>
                          <input type="text" name="" className="ser-part" />
                        </div>
                        <div className="col-sm-3 fa-icon-2"><i className="fa fa-search" aria-hidden="true"></i></div>
                      </div>
                    </div>*/}
                  </div>
                </div>
              </div>
            </div>
            {/*<div className="col-sm-2 get-more">
              <a href="/sendreviewinvite" className="get-more-reviews">Send Review Invite</a>
            </div>*/}
          </div>
        </div>
      </div>
      <div className="table-part">
        <div className="row button-part">
          <div className="col-sm-3">
            <p>Showing 1 to 4 Results</p>
          </div>
          <div className="col-sm-9"> 
            <select name="cars" className="part-of-btn" id="cars">
              <option value="volvo">All Sources</option> 
            </select>
            <select name="cars" className="part-of-btn" id="cars">
              <option value="volvo">All Ratings</option> 
            </select>
            <select name="cars" className="part-of-btn" id="cars">
              <option value="">All Time</option>
              <option value="1">1 month</option>
              <option value="3">3 months</option>
              <option value="6">6 months</option>
              <option value="12">12 months</option>
              {/*<option value="custom_date">Custom Dates</option>*/}
            </select>
            <button className="part-of-btn">Reset</button>
            <button className="part-of-btn">CSV</button>
            {/*<select name="cars" className="part-of-btn" id="cars">
              <option value="volvo">All Reset</option> 
            </select>
            <select name="cars" className="part-of-btn right" id="cars">
              <option value="volvo">CSV</option> 
            </select>*/}
          </div>
        </div>
        <table>
          <tr>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Date</th>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Source</th>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Rating</th>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Review</th>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Status</th>
            <th style={{color: "#a5a5a5", fontWeight: "200"}} cols="col-3">Actions</th>
          </tr>
          {
            this.state.toplivereviews.length > 0
              ?  
                this.state.toplivereviews.map(toplivereview => {
                  return(
                    <tr>
                      <td cols="col-3">
                        <Moment format="LL">
                          {toplivereview.live_review_datetime}
                        </Moment>
                      </td>
                      <td cols="col-3"> 
                        { 
                          toplivereview.review_type=='Google'
                          ?
                            <img src={googleLogo} alt="google-logo" className="img-responsive" />
                          :
                            <img src={facebookLogo} alt="facebook-logo" className="img-responsive" />
                        }
                        {toplivereview.review_type=='Google' ? 'Google' : 'Facebook'}
                      </td>
                      <td cols="col-3"><StarRatingComponent name="rate2" editing={false} starCount={5} value={toplivereview.live_review_rating} /></td>
                      <td cols="col-3">
                        <ReadMoreAndLess
                          ref={this.ReadMore}
                          className="read-more-content"
                          charLimit={25}
                          readMoreText="Read more"
                          readLessText="Read less"
                        >
                          {toplivereview.live_review_text}
                        </ReadMoreAndLess> 
                      </td>
                      <td cols="col-3">Unread</td>
                      <td cols="col-3"><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
                    </tr>
                  )
                })
              :
                <tr>
                 <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 ">{this.state.enableShdo ? <img src={LivloadngImg} />  : ''} {this.state.total == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                </tr>
          }
        </table>
        { pageNumbers.length > 1 ?
          <Pagination
          activePage={this.state.activePage}
          totalItemsCount={this.state.total}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange.bind(this)}
          />
        : ''
        } 
        <div className="row bot-area">
          <div className="col-sm-6 right-show"><p>Showing 1 to {this.state.total} of Result</p></div>
          <div className="col-sm-6 left-show">
            <p><a href="#">20 per page</a></p>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
      </div>
    </div>
    </>
  );
 }
}

export default livereviews;
