import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import Header from './Header.js';
import Footer from './Footer.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';

import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getallInviteUser = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getallinvitecustomer', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerwithmonth', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class staffdashboard extends React.Component {
  state = {
        inviteusercustomer: [],
        total: '',
        inviteuserid: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
        invite_customer_id:'',
        invitecustomer:[],
        invitecustomercount:'',
        reviewgivencount:'',
        inviteCustomerchartdata:'',
        x:{},
    }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });  
  }
  refreshInviteUserListing = (page) => {
    const data = {
        page: page,
        user_id: localStorage.getItem(USER_ID),
    }
    getallInviteUser(data)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ inviteusercustomer: records.data });
            this.setState({ total: res.success.total });
            this.setState({ currentPage: res.success.current_page });
            this.setState({ PerPage: res.success.per_page });
            this.setState({ FirstPageUrl: res.success.first_page_url });
            this.setState({ NextPageUrl: res.success.next_page_url });
            this.setState({ PrevPageUrl: res.success.prev_page_url });
            this.setState({ LastPageUrl: res.success.last_page_url });
            this.setState({ LastPage: res.success.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          } else {
            this.setState({ inviteusercustomer: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }  
refreshgetAllInviteCustomerWithMonth = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        /*let newState = this.state;
        newState.inviteCustomerchartdata[0] = res.data;
        this.setState({ ...newState });
        console.log('yours',res.data);
        console.log('mine',this.state.inviteCustomerchartdata);*/
        var records = res.data
        this.setState({ inviteCustomerchartdata: records });
        console.log(this.state.inviteCustomerchartdata);
      } else {
        this.setState({ inviteCustomerchartdata: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}
constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
componentDidMount() {
    this.refreshInviteUserListing();
    this.refreshgetAllInviteCustomerWithMonth();
    this.refreshgetAllInviteCustomerReviewGivenCount();
  }

openmodal(invite_custo_id){
    this.setState({invite_customer_id:invite_custo_id});
    alert(this.state.invite_customer_id);
    }

  render(){
    const column_options = {
      animationEnabled: true,
      exportEnabled: false,
      title: {
        text: "Invite Customer and Given Reviews Report",
        fontFamily: "verdana"
      },
      axisY: {
        //title: "in Eur",
        //prefix: "€",
        //suffix: "k"
      },
      toolTip: {
        shared: true,
        reversed: true
      },
      legend: {
        verticalAlign: "center",
        horizontalAlign: "right",
        reversed: true,
        cursor: "pointer",
        itemclick: this.toggleDataSeries
      },
      data: [
      {
        type: "stackedColumn",
        name: "Invite Customer",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
        //this.state.inviteCustomerchartdata
          { label: "Jan", y: 14 },
          { label: "Feb", y: 12 },
          { label: "Mar", y: 14 },
          { label: "Apr", y: 13 },
          { label: "May", y: 13 },
          { label: "Jun", y: 13 },
          { label: "Jul", y: 14 },
          { label: "Aug", y: 14 },
          { label: "Sept", y: 13 },
          { label: "Oct", y: 14 },
          { label: "Nov", y: 14 },
          { label: "Dec", y: 14 }
        ]
          //this.state.inviteCustomerchartdata
          /*{ 
            label: "Jan", 
            y: this.state.invitecustomer.map(invitecustomerdata => invitecustomerdata.total_invite) 
          }*/
          /*{ label: "Jan", y: 14 },
          { label: "Feb", y: 12 },
          { label: "Mar", y: 14 },
          { label: "Apr", y: 13 },
          { label: "May", y: 13 },
          { label: "Jun", y: 13 },
          { label: "Jul", y: 14 },
          { label: "Aug", y: 14 },
          { label: "Sept", y: 13 },
          { label: "Oct", y: 14 },
          { label: "Nov", y: 14 },
          { label: "Dec", y: 14 }*/
        
      },
      {
        type: "stackedColumn",
        name: "Review Given",
        showInLegend: true,
        yValueFormatString: "#,###",
        dataPoints: [
          { label: "Jan", y: 13 },
          { label: "Feb", y: 13 },
          { label: "Mar", y: 15 },
          { label: "Apr", y: 16 },
          { label: "May", y: 17 },
          { label: "Jun", y: 17 },
          { label: "Jul", y: 18 },
          { label: "Aug", y: 18 },
          { label: "Sept", y: 17 },
          { label: "Oct", y: 18 },
          { label: "Nov", y: 18 },
          { label: "Dec", y: 18 }
        ]
      }]
    }
    const pie_options = {
      animationEnabled: true,
      title: {
        text: "Invite Customer and Given Reviews Report"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "center",
        fontSize: 35,
        dockInsidePlotArea: true
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}",
        yValueFormatString: "#,###",
        dataPoints: [
          { name: "Invite Customer", y: this.state.invitecustomercount },
          { name: "Review Given", y: this.state.reviewgivencount }
        ]
      }]
    }
    return(
      <div className="dashboard_body">
        <Route component={Header} />
        <div className="container-fluid useflex">
        <Route component={StaffLeftsidebar} />
          <div className="content_body">
            <h1 className="black_bk_col fontweight500 font_22 mb-3">Staff Dashboard</h1>
            <div className="row chart_section">
              <div className="col-8 left_chart_section">
                <CanvasJSChart options = {column_options} 
                  /* onRef={ref => this.chart = ref} */
                />
              </div>
              <div className="col-4" right_chart_section>
                <CanvasJSChart options = {pie_options} 
                  /* onRef={ref => this.chart = ref} */
                />
              </div>
            </div>
            <div className="your_review latest_reviews">
              <div className="row">
                <div className="col-md-12">
                  <div className="row">
                    <div className="col-md-6">
                      <h3 className="font_12 heading_col fontweight700 ">Invited Customer for Review</h3>
                    </div>
                    <div className="col-md-6 text-right">
                      <a href={'/addinvitecustomer'} className="inline_comndb_border_btn"> Invited Customer for Review</a>
                    </div>
                  </div>
                  <table className="table table-borderless">
                    <thead className="border_bottom">
                      <tr>
                        <th className="font_12 heading_col fontweight700 wth_15">Invited User</th>
                        <th className="font_12 heading_col fontweight700 wth_15">Email</th>
                        <th className="font_12 heading_col fontweight700 wth_15">Invited Date</th>
                        <th className="font_12 heading_col fontweight700 wth_15">Business Name</th>
                        <th className="font_12 heading_col fontweight700 wth_15">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                      this.state.inviteusercustomer.length > 0
                      ?  
                        this.state.inviteusercustomer.map(inviteusercustomerdata => {
                          return <tr>
                            <td className="font_12 txt_col_drk fontweight_normal wth_15">{inviteusercustomerdata.customer_name}</td>
                            <td className="font_12 txt_col_drk fontweight_normal wth_15">{inviteusercustomerdata.customer_Email}</td>
                            <td className="font_12 txt_col fontweight400 wth_15">
                              <Moment format="MM/DD/YYYY">
                                {inviteusercustomerdata.created_at}
                              </Moment>
                            </td>
                            <td className="font_12 txt_col_drk fontweight_normal wth_15"></td>
                            <td className="wth_15">
                              <a data-toggle="modal" data-target="#exampleModal_{inviteusercustomerdata.id}" onClick={this.openmodal.bind(this, inviteusercustomerdata.id)} className="btn_icon_section"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                        })
                      :
                      <tr>
                        <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.total == '0' ? 'There are currently no found.' : ''}</td>
                      </tr>
                    }
                    </tbody>
                  </table>
                  <div className="row">
                    {/*<div className="col-md-6">
                      <a href={'/livereviews/' + this.state.blId} className="refresh_btn font_12 fontweight400 green_col"> See all reviews <i className="fa fa-long-arrow-right" aria-hidden="true"></i> </a>
                    </div>*/}
                    <div className="col-md-6 text-right">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <MessengerCustomerChat
                pageId="415979359011219"
                appId="448749972524859"
              />
            </div>
          </div>
        </div>
        <Route component={Footer} /> 
        <ToastContainer autoClose={5000} />
        <div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
        <div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>    
      </div>    
    );
  }
}

export default staffdashboard;
