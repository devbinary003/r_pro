import React from "react";
import { Route} from 'react-router-dom';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID } from '../../constants';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';
import MessengerCustomerChat from 'react-messenger-customer-chat';

const getInviteCustomerdata = (data, id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getbusinessreview/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class viewinvitecustomer extends React.Component {
    state = {
        invite_id: '',
        invite_customer_name:'',
        invite_customer_email:'',
        invite_customer_desc:'',
        invite_customer_review_given:'',
        invite_customer_business_name:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
    }
  refreshInviteCustomerData = (id) => {
    const data = {
        user_id: localStorage.getItem(USER_ID),
    }
    getInviteCustomerdata(data, id)
        .then(res => {
          if(res.status===true){
              this.setState({
                invite_id: res.data.id,
                invite_customer_name: res.data.customer_name,
                invite_customer_email: res.data.customer_Email,
                invite_customer_desc: res.data.ext_1,
                invite_customer_review_given: res.review_given_count,
                invite_customer_business_name: res.data.businesslocation_id,
                edit_password: res.data.hdpwd,
              });
          } else {
            
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }
  componentDidMount() {
    var id = this.props.match.params.id;
    this.refreshInviteCustomerData(id);
  }  
render(){
  return (
    <>
      <Route component={StaffLeftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        <div className="row">
          <div className="col-sm-12">
            <h1 className="black_bk_col fontweight500 font_22 mb-3">View Invite Customer Details</h1>
          </div>
        </div>
        <div className="tab-content">
          <div className="row">
            <div className="col-sm-3"></div>
            <div className="col-sm-6 invite_customer_details_section">
              <table style={{width: "100%"}}>
                <tr>
                  <th>Invite Customer Name:</th>
                  <td>{this.state.invite_customer_name}</td>
                </tr>
                <tr>
                  <th>Invite Customer Email:</th>
                  <td>{this.state.invite_customer_email}</td>
                </tr>
                <tr>
                  <th>Invite Customer Phone No:</th>
                  <td>{this.state.invite_customer_desc}</td>
                </tr>
                <tr>
                  <th>Invite Customer Given Review:</th>
                  <td>{this.state.invite_customer_review_given > '0' ? 'yes' : 'No'}</td>
                </tr>
              </table>
              {/*<ul className="invite_customer_details">
                <li><strong>Invite Customer Name:</strong> <span className="invitecustomertext">{this.state.invite_customer_name}</span></li>
                <li><strong>Invite Customer Email:</strong> <span className="invitecustomertext">{this.state.invite_customer_email}</span></li>
                <li><strong>Invite Customer Phone No:</strong> <span className="invitecustomertext">{this.state.invite_customer_desc}</span></li>
                <li><strong>Invite Customer Given Review:</strong> <span className="invitecustomertext">{this.state.invite_customer_review_given > '0' ? 'yes' : 'No'}</span></li>
              </ul>*/}
            </div>
            <div className="col-sm-3"></div>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
        <ToastContainer autoClose={5000} />
      </div>
    </>
  );
}
}
export default viewinvitecustomer;
