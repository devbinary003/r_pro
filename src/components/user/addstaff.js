import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';


const AddNewStaff = (data) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/staff/addstaff', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class addstaff extends React.Component {
  state = {
        fields: {},
        errors: {},
        first_name: '',
        last_name: '',
        phone: '',
        email: '',
        password: '',
        profile_pic:'',

        signup_success: false,
        disableBaddr: false,
        disableBemail: false,
        
        disablePurl: false,
        disablePurls: false,
        copied: false,
        Loading: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.first_name) {
        formIsValid = false;
        errors["first_name"] = "*Please enter first name.";
      }

      if (!this.state.last_name) {
        formIsValid = false;
        errors["last_name"] = "*Please enter last name.";
      }

      if (!this.state.password) {
        formIsValid = false;
        errors["password"] = "*Please enter password.";
      }

      if (typeof this.state.password !== "undefined") {
        if (this.state.password.length < 6) {
          formIsValid = false;
          errors["password"] = "*password must be at least 6 characters long.";
        }
      }

      if (!this.state.confirm_password) {
        formIsValid = false;
        errors["confirm_password"] = "*Please enter confirm password.";
      }

      if(this.state.confirm_password != this.state.password){
          formIsValid = false;
          errors["confirm_password"] = "*Password and confirm password must be same";
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.phone) {
        formIsValid = false;
        errors["phone"] = "*Please enter phone number.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.setState({ Loading: true }); 
      this.savedata();
    }
    //alert('hello');
}

savedata = () => {
  const data = {
      first_name: this.state.first_name,
      last_name:this.state.last_name,
      email:this.state.email,
      user_id: localStorage.getItem(USER_ID),
      phone: this.state.phone,
      password: this.state.password,
  }
  AddNewStaff(data)
      .then(res => {
          if (res.status==true) {
              this.setState({
              // forwarduId: res.data.user_id,
               signup_success: true,
              });
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }
    
  diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }
    
  componentDidMount() {
    //this.refreshUserDeatils();
  }

render(){

const {Loading} = this.state;

  
return (
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        <div className="row">
          <div className="col-md-12 form_section">
            <form className="needs-validation step_form mb-4" >
              <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center">Add Staff</h1>                  
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="first_name" name="first_name" placeholder="Enter First Name" value={this.state.first_name} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.first_name}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="last_name" name="last_name" placeholder="Enter Last Name" value={this.state.last_name} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.last_name}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="email" name="email" placeholder="Enter Email" value={this.state.email} onChange={this.handleChange} autoComplete="off"/>
                    <span style={alertStyle}>{this.state.errors.email}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="password" className="form-control input_custom_style" id="password" name="password" placeholder="Enter password" value={this.state.password} onChange={this.handleChange} autoComplete="off"/>
                    <span style={alertStyle}>{this.state.errors.password}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                  <input type="password" className="form-control input_custom_style" name="confirm_password" id="confirm_password" value={this.state.confirm_password} onChange={this.handleChange} placeholder="Confirm Password" required=""  />
                  <span style={alertStyle}>{this.state.errors.confirm_password}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <PhoneInput
                    country="US"
                    showCountrySelect={ false }
                    name="phone"
                    limitMaxLength={ true }
                    placeholder="Enter phone number"
                    className="businessPhone"
                    value={ this.state.phone }
                    onChange={ phone => this.setState({ phone }) } 
                    />
                    <span style={alertStyle}>{this.state.errors.phone}</span>
                  </div>
                </div>
              </div>
              <div className="text-center">
                <button type="button" className="blue_btn_box add_staff_btn mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Add Staff</button>
              </div>
            </form>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
        <ToastContainer autoClose={5000} />
      </div>
  </>
  );
 }
}
export default addstaff;
