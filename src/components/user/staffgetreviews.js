import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import Header from './Header.js';
import Footer from './Footer.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';

import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getallInviteUser = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getallinvitecustomer', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerwithmonth', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class staffgetreviews extends React.Component {
  state = {
        inviteusercustomer: [],
        total: '',
        inviteuserid: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
        invite_customer_id:'',
        invitecustomer:[],
        invitecustomercount:'',
        reviewgivencount:'',
        inviteCustomerchartdata:'',
        x:{},
    }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });  
  }
  refreshInviteUserListing = (page) => {
    const data = {
        page: page,
        user_id: localStorage.getItem(USER_ID),
    }
    getallInviteUser(data)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ inviteusercustomer: records.data });
            this.setState({ total: res.success.total });
            this.setState({ currentPage: res.success.current_page });
            this.setState({ PerPage: res.success.per_page });
            this.setState({ FirstPageUrl: res.success.first_page_url });
            this.setState({ NextPageUrl: res.success.next_page_url });
            this.setState({ PrevPageUrl: res.success.prev_page_url });
            this.setState({ LastPageUrl: res.success.last_page_url });
            this.setState({ LastPage: res.success.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          } else {
            this.setState({ inviteusercustomer: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }  
refreshgetAllInviteCustomerWithMonth = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        /*let newState = this.state;
        newState.inviteCustomerchartdata[0] = res.data;
        this.setState({ ...newState });
        console.log('yours',res.data);
        console.log('mine',this.state.inviteCustomerchartdata);*/
        var records = res.data
        this.setState({ inviteCustomerchartdata: records });
        console.log(this.state.inviteCustomerchartdata);
      } else {
        this.setState({ inviteCustomerchartdata: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}
constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
componentDidMount() {
    this.refreshInviteUserListing();
    this.refreshgetAllInviteCustomerWithMonth();
    this.refreshgetAllInviteCustomerReviewGivenCount();
  }

openmodal(invite_custo_id){
    this.setState({invite_customer_id:invite_custo_id});
    alert(this.state.invite_customer_id);
    }

  render(){
    const currentPage = this.state.currentPage;
    const previousPage = currentPage - 1;
    const NextPage = currentPage + 1;
    const LastPage = this.state.LastPage;
    const pageNumbers = [];
    for (let i = 1; i <= this.state.TotalPages; i++) {
      pageNumbers.push(i);
    }
    return(
        <>
        <Route component={StaffLeftsidebar} />
          <div className="left-bar">
            <Route component={Header} />
            <h1 className="black_bk_col fontweight500 font_22 mb-3">Get Reviews</h1>
            <div className="latest_reviews">
              <div className="row">
                <div className="col-md-12">
                  <div className="table-list-part">
                    <div className="row button-part">
                      <div className="col-sm-5 result_shows_section">
                        <p className="left_content_section">Showing 1 to {this.state.inviteusercustomer.length} Results <a href={'/sendreviewinvite'} className="part-of-btn add_invite_customer_btn">Invite Customer</a></p>
                      </div>
                      <div className="col-sm-7 filter-section"> 
                        <select name="cars" className="part-of-btn" id="cars">
                          <option value="all">All Sources</option> 
                          <option value="Google">Google</option>
                          <option value="Facebook">Facebook</option>
                        </select>
                        <select name="cars" className="part-of-btn" id="cars">
                          <option value="all">All Ratings</option> 
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                        <select name="cars" className="part-of-btn" id="cars">
                          <option value="">All Time</option>
                          <option value="1">1 month</option>
                          <option value="3">3 months</option>
                          <option value="6">6 months</option>
                          <option value="12">12 months</option>
                          <option value="custom_date">Custom Dates</option>
                        </select>
                        <button className="part-of-btn">Reset</button>
                        {/*<select name="cars" className="part-of-btn" id="cars">
                          <option value="volvo">All Reset</option> 
                        </select>*/}
                        
                        {/*<button className="part-of-btn">CSV</button>*/}
                        {/*<select name="cars" className="part-of-btn right" id="cars">
                          <option value="volvo">CSV</option> 
                        </select>*/}
                      </div>
                    </div>
                    <table className="table-responsive">
                      <tr>
                        <th style={{color: "#a5a5a5", fontWeight: "200"}}>Invited User</th>
                        <th style={{color: "#a5a5a5", fontWeight: "200"}}>Email</th>
                        <th style={{color: "#a5a5a5", fontWeight: "200"}}>Invited Date</th>
                        <th style={{color: "#a5a5a5", fontWeight: "200"}}>Phone No.</th>
                        <th style={{color: "#a5a5a5", fontWeight: "200"}}>Actions</th>
                      </tr>
                      {
                        this.state.inviteusercustomer.length > 0
                        ?  
                          this.state.inviteusercustomer.map(inviteusercustomerdata => {
                            return <tr>
                                    <td>{inviteusercustomerdata.customer_name}</td>
                                    <td>{inviteusercustomerdata.customer_Email}</td>
                                    <td>
                                      <Moment format="MM/DD/YYYY">
                                        {inviteusercustomerdata.created_at}
                                      </Moment>
                                    </td>
                                    <td>{inviteusercustomerdata.ext_1}</td>
                                    <td><a href={'/viewinvitecustomer/' + inviteusercustomerdata.id} className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
                                </tr>
                          })
                        :
                        <tr>
                          <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                        </tr>
                      }
                    </table>
                    { 
                      pageNumbers.length > 1 ?
                        <Pagination
                        activePage={this.state.activePage}
                        totalItemsCount={this.state.total}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)}
                        />
                      : ''
                    }
                    <div className="row bot-area">
                      <div className="col-sm-6 right-show"><p>Showing 1 to {this.state.inviteusercustomer.length} of Result</p></div>
                      <div className="col-sm-6 left-show">  
                        <p><a href="#">20 per page</a></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <MessengerCustomerChat
                pageId="415979359011219"
                appId="448749972524859"
              />
            </div>
            <ToastContainer autoClose={5000} />
            <div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
            <div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>    
          </div>
          </>    
    );
  }
}

export default staffgetreviews;
