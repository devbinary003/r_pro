import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

const getBusinessLocation = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const AddFindBusiness = (data, uid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/businesslocation/addfindbusiness/'+uid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

class findbusiness extends React.Component {
  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        blId: '',
        find_business_location:'',
        lat: '',
        lng: '',
        signup_success:false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.find_business_location) {
        formIsValid = false;
        errors["find_business_location"] = "*Please enter business name, city, zip code.";
      }

      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
       if (this.validateForm()==true) {
          this.savedata();
    }
}

savedata = () => {
  var uid = localStorage.getItem(USER_ID);
  const data = {
      find_business_location: this.state.find_business_location,
      lat:this.state.lat,
      lng:this.state.lng,
      bl_id:this.state.blId,
  }
  AddFindBusiness(data, uid)
      .then(res => {
          if (res.status==true) {
              this.setState({
               blId: res.data.id,
               forwarduId: res.data.user_id,
               signup_success: true,
              });
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
      })
      .catch(err => {
          console.log(err);
      });
  }

  handleAdrChange = find_business_location => {
    this.setState({ find_business_location });
    this.setState({ errors : ''});
  };

  handleSelect = find_business_location => {
    this.setState({ find_business_location:find_business_location });
    geocodeByAddress(find_business_location)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        //  console.log('Success', latLng);
          this.setState({
              find_business_location: find_business_location, 
              lat: latLng.lat,
              lng: latLng.lng,
            });
       })
      .catch(error => console.error('Error', error));
  };

  componentDidMount() {
    getBusinessLocation()
      .then(res => {
          if(res.status==true){
              this.setState({
                find_business_location: res.data.find_business_location,
                lat: res.data.lat,
                lng: res.data.lng,
                blId: res.data.id,
                forwarduId: res.data.user_id,
              });
          } else {
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

render(){

if (this.state.signup_success) return <Redirect to={'/businessinfo'} />

return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12">
        <h1 className="black_bk_col fontweight500 font_16 mb-4 pb-1">Complete Account Sign Up</h1>
      </div>
      <div className="container">
          <div id="app">
            <ol className="step-indicator">
           <li className="active">
              <div className="step_name">Step <span>1</span></div>
              <div className="step_border">
                 <div className="step"><i className="fa fa-circle"></i></div>
              </div>
              <div className="caption hidden-xs hidden-sm"><span>FIND YOUR BUSINESS</span></div>
           </li>
           <li className="">
              <div className="step_name">Step <span>2</span></div>
              <div className="step_border">
                 <div className="step"><i className="fa fa-circle"></i></div>
              </div>
              <div className="caption hidden-xs hidden-sm"><span>BUSINESS INFORMATION</span></div>
           </li>
           <li className="">
              <div className="step_name">Step <span>3</span></div>
              <div className="step_border">
                 <div className="step"><i className="fa fa-circle"></i></div>
              </div>
              <div className="caption hidden-xs hidden-sm"><span>CUSTOMIZE REVIEW SYSTEM</span></div>
           </li>
           <li className="">
              <div className="step_name">Step <span>4</span></div>
              <div className="step_border">
                 <div className="step"><i className="fa fa-circle"></i></div>
              </div>
              <div className="caption hidden-xs hidden-sm"><span>BILLING TO STRIPE</span></div>
           </li>
        </ol>

          <div className="row step_one">
            <div className="col-md-12">
              <form className="needs-validation mb-4" >
                  <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Find your business <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Please type in your full business name into the field below. This would be your registered company name."></i></h1>
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group">

                  <PlacesAutocomplete
                  value={this.state.find_business_location}
                  onChange={this.handleAdrChange}
                  onSelect={this.handleSelect}
                  >
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                      <div>
                        <input
                          {...getInputProps({
                            placeholder: 'Type your business name, city, zip code',
                            className: 'form-control input_custom_style',
                          })}
                        />
                        <div className="autocomplete-dropdown-container">
                          {loading && <div>Loading...</div>}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                              ? { backgroundColor: '#1a0061', cursor: 'pointer', color: '#ffffff' }
                              : { backgroundColor: '#ffffff', cursor: 'pointer' };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <span>{suggestion.description}</span>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
            </PlacesAutocomplete>
            <span style={alertStyle}>{this.state.errors.find_business_location}</span>
                      
                    </div>
                  </div>
                </div>
               <div className="text-center mt-4">    
                <button type="button" className="blue_btn_box" onClick={this.ValidateBasic}>Next</button>
               </div>
               <p className="contact_support_section"><a href="/contact" target="_blank">If you can't find your business, please contact customer support</a></p>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default findbusiness;
