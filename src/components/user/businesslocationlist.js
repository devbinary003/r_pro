import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import StarRatingComponent from 'react-star-rating-component';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import ReadMoreAndLess from 'react-read-more-less';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getBusinessLocationLists = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/bllists/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class businesslocationlist extends React.Component {
  state = {
        businesslocationlists: [],
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

refreshUserDeatils = () => {
  getUserDeatils()
      .then(res => {
        if(res.status==true){
          var userdata = res.data;
          this.setState({
                business_name: userdata.business_name,
                business_address: userdata.business_address,
              });
        } else {
          this.setState({ business_review_link: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshBlList = (page) => {
  const data = {
      page: page,
  }
  getBusinessLocationLists(data)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ businesslocationlists: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
        } else {
          this.setState({ businesslocationlists: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

  componentDidMount() {
    this.refreshUserDeatils();
    this.refreshBlList();
  }

render(){

  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }

return (
  <>
  <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12 row">
        <div className="col-sm-6">
          <h1 className="black_bk_col fontweight500 font_22 mb-3">Business locations</h1>
        </div>
        <div className="col-sm-6">
          <ul className="nav nav-tabs pull-right">
          </ul>
        </div>
      </div>

      <div className="col-sm-12 tab-content">
        <div id="home" className="tab-pane active"><br />
          <table className="table table-borderless">
            <thead className="border_bottom">
              <tr>
                <th className="font_12 heading_col fontweight700 wth_40">NAME</th>
                <th className="font_12 heading_col fontweight700 wth_40">Address</th>
                <th className="font_12 heading_col fontweight700 wth_20">Action</th>
              </tr>
            </thead>
            <tbody>

            {
              this.state.businesslocationlists.length > 0
              ?  
              this.state.businesslocationlists.map(bllist => {
              return <tr className="highLightedtr">
                <td className="font_12 txt_col fontweight700 wth_40">{bllist.business_name}</td>
                <td className="font_12 txt_col fontweight700 wth_40">{bllist.business_address}</td>
                <td className="wth_20"><a href={'./editbusinesslocation/' + bllist.id}>Edit</a></td>
              </tr>
              })
              :
              <tr>
                <td className="font_12 txt_col fontweight700 ">Finding Results</td>
              </tr>
              }
              
            </tbody>
          </table>

        { pageNumbers.length > 1 ?

          <ul className="pagination">

          {this.state.PrevPageUrl != null ? 
            <li><a style={{cursor:'pointer'}} onClick={this.refreshBlList.bind(this, previousPage)} className="font_12 heading_col fontweight700 "><i className="fa fa-long-arrow-left" aria-hidden="true"></i></a></li>
            : ''
            }

            {pageNumbers.map(page => {
              return (<li><a style={{cursor:'pointer'}} onClick={this.refreshBlList.bind(this, page)} className={currentPage == page ? 'font_12 heading_col fontweight700 active' : 'font_12 heading_col fontweight700 active' }>{page}</a></li>);
            })}

            {this.state.LastPage != null && currentPage!=LastPage? 
              <li><a style={{cursor:'pointer'}} onClick={this.refreshBlList.bind(this, LastPage)} className="font_12 heading_col fontweight700"><i className="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                : ''
            }
            
          </ul>

        : ''
        }       
      </div>
      </div>
      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default businesslocationlist;
