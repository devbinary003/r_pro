import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import LivloadngImg from '../../images/lvrlbilling.gif';

const getallStaff = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/staff/getbusinessstafflist', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
const deleteStaff = (data) => {

  return new Promise((resolve, reject) => {
      const req = scAxios.request('/staff/delete/'+data.staffid, {
          method: 'post',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          }

      });

      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const deactivateStaff = (data) => {

  return new Promise((resolve, reject) => {
      const req = scAxios.request('/staff/deactivate/'+data.staffid, {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          }

      });

      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const activateStaff = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/staff/activate/'+data.staffid, {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          }

      });

      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const txtCenter = {
    textAlign: 'center',
};

class managestaff extends React.Component {
    state = {
        stafflistingdata: [],
        total: '',
        staffid: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
    }
  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshTopLiveReviews(pageNumber);
  }
  refreshStaffListing = (page) => {
    this.setState({ enableShdo: true, });
    const data = {
        page: page,
        user_id: localStorage.getItem(USER_ID),
    }
    getallStaff(data)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ stafflistingdata: records.data });
            this.setState({ total: res.success.total });
            this.setState({ currentPage: res.success.current_page });
            this.setState({ PerPage: res.success.per_page });
            this.setState({ FirstPageUrl: res.success.first_page_url });
            this.setState({ NextPageUrl: res.success.next_page_url });
            this.setState({ PrevPageUrl: res.success.prev_page_url });
            this.setState({ LastPageUrl: res.success.last_page_url });
            this.setState({ LastPage: res.success.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          } else {
            this.setState({ stafflistingdata: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }
  delete(id){
  const data = {
          staffid: id
      }
    deleteStaff(data)
      .then(res => {
        if(res.status=='1'){
          toast.success(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }  else {
          toast.error(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.refreshStaffListing();
      })
      .catch(err => {
          console.log(err);
      });
  }
  deactivate(id){
  const data = {
          staffid: id
      }
    deactivateStaff(data)
      .then(res => {
        if(res.status==true){
          toast.success(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }  else {
          toast.error(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.refreshStaffListing();
      })
      .catch(err => {
          console.log(err);
      });
  }
  activate(id){
  const data = {
          staffid: id
      }
    activateStaff(data)
      .then(res => {
        if(res.status==true){
          toast.success(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }  else {
          toast.error(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.refreshStaffListing();
      })
      .catch(err => {
          console.log(err);
      });
  }
  componentDidMount() {
    this.refreshStaffListing();
  }  
render(){
  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }
  return (
  /*<div className="dashboard_body">
    
    <div className="container-fluid useflex">*/
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        <div className="col-sm-12">
          <div className="row">
            <div className="col-sm-6">
              <h1 className="black_bk_col fontweight500 font_22 mb-3">Manage Staff</h1>
            </div>
            <div className="col-sm-6 right_side_section">
             <a href={'/addstaff'} className="inline_comn_border_btn mb-3 part-of-btn reset_btn">Add Staff</a>
            </div>
          </div>
          <div className="tab-content">
            <div id="home" className="tab-pane active"><br />
              <table className="table table-borderless">
                <thead className="border_bottom">
                  <tr>
                    <th className="font_12 heading_col fontweight700 wth_15">FULL NAME</th>
                    <th className="font_12 heading_col fontweight700 wth_15">EMAIL</th>
                    <th className="font_12 heading_col fontweight700 wth_15">CONTACT</th>
                    <th className="font_12 heading_col fontweight700 wth_15">USERNAME</th>
                    <th className="font_12 heading_col fontweight700 wth_15">ACTION</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.state.stafflistingdata.length > 0
                    ?  
                    this.state.stafflistingdata.map(stafflisting => {
                    return(
                      <tr>
                        <td className="font_12 txt_col_drk fontweight_normal wth_15">{stafflisting.fullname}</td>
                        <td className="font_12 txt_col fontweight400 wth_15">{stafflisting.email}</td>
                        <td className="font_12 txt_col_drk fontweight_normal wth_15">{stafflisting.phone}</td>
                        <td className="font_12 txt_col_drk fontweight_normal wth_15">{stafflisting.fullname}</td>
                        <td className="wth_15">
                          <a href={'./editstaff/' + stafflisting.id} className="btn_icon_section"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          <a href="#" className="btn_icon_section" onClick={(e) => { if (window.confirm('Are you sure you wish to delete this list ?')) this.delete(stafflisting.id)} }><i class="fa fa-trash" aria-hidden="true"></i></a>
                          {
                            stafflisting.status=='0'
                            ?
                              <a href="#" className="btn_icon_section" onClick={(e) => { if (window.confirm('Are you sure you wish to activate this account ?')) this.activate(stafflisting.id)} }><i class="fa fa-times" aria-hidden="true"></i></a>
                            :
                              <a href="#" className="btn_icon_section" onClick={(e) => { if (window.confirm('Are you sure you wish to deactivate this account ?')) this.deactivate(stafflisting.id)} }><i class="fa fa-check" aria-hidden="true"></i></a> 
                          }
                          <a href={'/staffinvitecustomerreport/' + stafflisting.id} className="btn_icon_section"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>                        
                        </td>
                      </tr>
                    );
                    })
                    :
                    <tr>
                      <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.total == '0' ? 'There are currently no found.' : ''}</td>
                    </tr>
                  }
                </tbody>
              </table>
              { pageNumbers.length > 1 ?
                <Pagination
                activePage={this.state.activePage}
                totalItemsCount={this.state.total}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange.bind(this)}
                />
              : ''
              } 
            </div>
          </div>
          <div>
            <MessengerCustomerChat
              pageId="415979359011219"
              appId="448749972524859"
            />
          </div>
          <ToastContainer autoClose={5000} />
        </div>
      </div>
    {/*</div>
    <Route component={Footer} />*/}
  {/*</div>*/}
  </>
  );
}
}
export default managestaff;
