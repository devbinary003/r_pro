import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import { SketchPicker } from 'react-color';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import ReadMoreAndLess from 'react-read-more-less';
import queryString from 'query-string';

import Pagination from "react-js-pagination";
import Moment from 'react-moment';
import 'moment-timezone';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';
import LivloadngImg from '../../images/lvrlbilling.gif';

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getLiveRating = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/liverating/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getDefInternalreviews = (id, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/searchreviewdef/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getInternalreviews = (id, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/searchreview/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class search extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        liverating: '',
        reviews: [],
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        reviewer_name:'',
        reviewer_email:'',
        reviewer_rating:'',
        reviewer_description:'',
        business_name:'',
        business_address:'',
        business_review_link: '',
        fields: {},
        errors: {},
        copied: false,
        searchreview: '',
        activePage: 1,
        enableShdo: false,
    }

  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshReviewsList(pageNumber);
  }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
       let url = this.props.location.search;
       let params = queryString.parse(url);
       let searchKey = params.query; 
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshLiveRating(tValue);
          this.refreshDefReviewsList(tValue, searchKey);
      }
  }

refreshBlList = () => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          this.setState({ businesslocationlists: res.data });
        } else {
          this.setState({ businesslocationlists: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    } 

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshLiveRating = (id) => {
  getLiveRating(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ liverating: records });
        } else {
          this.setState({ liverating: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }


refreshDefReviewsList = (id, searchKey) => {
  this.setState({ enableShdo: true, });
  const data = {
      searchKey: searchKey,
  }
  getDefInternalreviews(id, data)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ reviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          this.setState({ searchreview: res.searchKey });
        } else {
          this.setState({ reviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshReviewsList = (page) => {
  this.setState({ enableShdo: true, });
  var id = this.state.blId;
  const data = {
      page: page,
      searchKey: this.state.searchreview,  
  }
  getInternalreviews(id, data)
      .then(res => {
        if(res.status==true){
          var records = res.success.data;
          this.setState({ reviews: records });
          this.setState({ total: res.success.total });
          this.setState({ currentPage: res.success.current_page });
          this.setState({ PerPage: res.success.per_page });
          this.setState({ FirstPageUrl: res.success.first_page_url });
          this.setState({ NextPageUrl: res.success.next_page_url });
          this.setState({ PrevPageUrl: res.success.prev_page_url });
          this.setState({ LastPageUrl: res.success.last_page_url });
          this.setState({ LastPage: res.success.last_page });
          this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          this.setState({ searchreview: res.searchKey });
        } else {
          this.setState({ reviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }   

  validateSearchReviewForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.searchreview) {
        formIsValid = false;
        errors["searchreview"] = "*Enter search key word.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }  
  
  validateSearchReview = event => {
    if (this.validateSearchReviewForm()==true) {
       this.handleSearchReview();
    }
  } 

  handleSearchReview = () => {
    var cblid = this.state.blId;
    var searchKey = this.state.searchreview;
    if(cblid!='' && searchKey!='') {
      window.location.href = '/search/'+ cblid +'/?query='+ searchKey;
    }
  }

componentDidMount() {
    let id = this.props.match.params.id;
    let url = this.props.location.search;
    let params = queryString.parse(url);
    let searchKey = params.query;
    this.setState({ blId: id, blIdnew: id, searchreview: searchKey });
    this.refreshBlList();
    this.refreshSingleBlDeatils(id);
    this.refreshLiveRating(id);
    this.refreshDefReviewsList(id, searchKey);
  }

  render(){

  const currentPage = this.state.currentPage;
  const previousPage = currentPage - 1;
  const NextPage = currentPage + 1;
  const LastPage = this.state.LastPage;
  const pageNumbers = [];
      for (let i = 1; i <= this.state.TotalPages; i++) {
        pageNumbers.push(i);
      }

    return(
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <h1 className="col-sm-12 black_bk_col fontweight500 font_22 mb-3">Business Dashboard</h1>
      <div className="col-sm-12 box_row mb-4">
        <div className="box_column frst">
          <div className="row m-0 height_100">
            <div className="col-1 box_icon">
              <i className="fa fa-map-marker" aria-hidden="true"></i>
            </div>
            <div className="col-11 box_txt">
              <p className="font_12 heading_col fontweight700">Select Business Location</p>
              <select name="blIdnew" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100">
              {
              this.state.businesslocationlists.length > 0
              ?  
              this.state.businesslocationlists.map(bllist => {
              return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
              })
              :
              <option> Select Business Location</option>
              }
            </select>  
            </div>
          </div>
        </div>
        <div className="box_column review">
          <div className="row box_txt border_radius_20 m-0 height_100">
            <div className="col-10">
              <p className="font_12 heading_col fontweight700">Search Reviews</p>
              <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="fontweight400 font_14 black_bk_col white_bk_col box_select_dgin width_100" />
              <p style={alertStyle}>{this.state.errors.searchreview}</p>
            </div>
            <div className="col-2 search box_icon">
              <i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i> 
            </div>
          </div>
        </div>
        <div className="box_column rating">
          <div className="row m-0 height_100">
            <div className="col-2 box_icon">
              <i className="fa fa-star-o" aria-hidden="true"></i>
            </div>
            <div className="col-10 box_txt">
              <p className="font_12 heading_col fontweight700 mb-0">Live Google Rating</p>
              <ul className="ratting_starr">
                <StarRatingComponent name="rate1" editing={false} starCount={5} value={this.state.liverating} />
              </ul>
              <h6 className="star_rating font_22 yellow_bk_col fontweight700 m-0">{this.state.liverating}</h6>
            </div>
          </div>
        </div>
      </div>

      <div className="col-sm-12 row mt-3">
        <div className="col-sm-12">
          <h1 className="black_bk_col fontweight500 font_22 mb-3">Search Results</h1>
        </div>
      </div>
     
      <div className="col-sm-12 tab-content">
        <div id="home" className="tab-pane active"><br />
          <table className="table table-borderless">
            <thead className="border_bottom">
              <tr>
                <th className="font_12 heading_col fontweight700 wth_15">FULL NAME</th>
                <th className="font_12 heading_col fontweight700 wth_15">DATE</th>
                <th className="font_12 heading_col fontweight700 wth_55">REVIEW</th>
                <th className="font_12 heading_col fontweight700 wth_15">RATING</th>
              </tr>
            </thead>
            <tbody>

             {
              this.state.reviews.length > 0
              ?  
              this.state.reviews.map(review => {
              return <tr>
                <td className="font_12 txt_col fontweight700 wth_15">{review.ruv_name}</td>
                <td className="font_12 txt_col fontweight700 wth_15">
                <Moment format="MM/DD/YYYY">
                 {review.ruv_datetime}
                </Moment></td>
                <td className="font_12 txt_col fontweight700 wth_55">
                    <ReadMoreAndLess
                        ref={this.ReadMore}
                        className="read-more-content"
                        charLimit={80}
                        readMoreText="Read more"
                        readLessText="Read less"
                        >
                      {review.ruv_desc}
                    </ReadMoreAndLess>
                </td>
                <td className="wth_15">
                 <StarRatingComponent name="rate2" editing={false} starCount={5} value={review.ruv_rating} />
                </td>
              </tr>
              })
              :
              <tr>
                <td colSpan="4" style={txtCenter} className="font_12 txt_col fontweight700 ">{this.state.enableShdo ? <img src={LivloadngImg} />  : ''} {this.state.total == '0' ? 'There are currently no result find.' : ''}</td>
              </tr>
              }

            </tbody>
          </table>

          { pageNumbers.length > 1 ?

          <Pagination
          activePage={this.state.activePage}
          totalItemsCount={this.state.total}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange.bind(this)}
          />

        : ''
        }

        </div>
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
    );

  }
}

export default search;
