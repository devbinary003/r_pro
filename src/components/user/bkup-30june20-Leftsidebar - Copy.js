import React, { Component } from 'react';
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, IMAGE_URL, USER_ID, USER_ROLE } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/lfts1.png';
import shoppingLlist from '../../images/lfts2.png';
import internalReview from '../../images/lfts3.png';
import settingsImg from '../../images/lfts4.png';
import lifesaverImg from '../../images/lfts5.png';
import dbfooterLogo from '../../images/Logo-01.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Leftsidebar extends React.Component {

  state = {
        isprofilecomplete: '',
    }
  
  handleClick() {
    toast.error("Welcome to Review Pro Solutions. Please first provide us some quick information about your business", {
              position: toast.POSITION.BOTTOM_RIGHT
            });
  }

  componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status==true){
              var userdata = res.data;
              this.setState({
                isprofilecomplete: userdata.isprofilecomplete,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

    render() {

      return (
        <>
        { this.state.isprofilecomplete == 1 ?
        (<ul className="side_bar">

          <li ><NavLink to="/userdashboard" activeClassName="active" className="sidebar_link"> <img src={menuImg} /> Dashboard</NavLink></li>
          <li ><NavLink to="/livereviews" activeClassName="active" className="sidebar_link"> <img src={shoppingLlist} /> Live Reviews</NavLink></li>
          <li ><NavLink to="/internalreviews" activeClassName="active" className="sidebar_link"> <img src={internalReview} /> Internal Reviews</NavLink></li>
          <li ><NavLink to="/managestaff" activeClassName="active" className="sidebar_link"> <img src={internalReview} /> Manage Staff</NavLink></li>
          <li ><NavLink to="/systemsettings" activeClassName="active" className="sidebar_link"> <img src={settingsImg} /> System Settings</NavLink></li>
          <li><a href="/#" className="sidebar_link" data-toggle="modal" data-target="#gethelpModal"> <img src={lifesaverImg} /> Get Help </a></li>  
          <li className="logo" ><img src={dbfooterLogo} /></li>
        </ul>)
        :
        (<ul className="side_bar">
          <li><a onClick={this.handleClick} href="#" className="sidebar_link"> <img src={menuImg} /> Dashboard </a></li>
          <li><a onClick={this.handleClick} href="#" className="sidebar_link"> <img src={shoppingLlist} /> Live Reviews </a></li>
          <li><a onClick={this.handleClick} href="#" className="sidebar_link"> <img src={internalReview} /> Internal Reviews </a></li>
          <li><a onClick={this.handleClick} href="#" className="sidebar_link"> <img src={settingsImg} /> System Settings </a></li>
          <li><a onClick={this.handleClick} href="#" className="sidebar_link"> <img src={lifesaverImg} /> Get Help </a></li>  
          <li className="logo"><img src={dbfooterLogo} /></li>
        </ul>)
        }     
        </>
        );
    }
}

export default Leftsidebar;
