import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';


const userupdate = (data, uid) => {

    return new Promise((resolve, reject) => {
       const req = scAxios.request('/user/accountsettings/'+uid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class accountsettings extends React.Component {
  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        firstname:'',
        lastname:'',
        fullname:'',
        phone:'',
        email:'',
        address_line_1:'',
        address_line_2:'',
        city:'',
        state:'',
        country:'',
        zipcode:'',
        signup_success:false,
        disableBemail: false,
        Loading: false,
        business_fb_page_id:'',
        business_fb_page_access_token:'',
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.firstname) {
        formIsValid = false;
        errors["firstname"] = "*Please enter first name.";
      }

      if (typeof this.state.firstname !== "undefined") {
        if (!this.state.firstname.match(/^[a-zA-Z ]*$/)) {
          formIsValid = false;
          errors["firstname"] = "*Please enter alphabets characters only.";
        }
      }

      if (!this.state.lastname) {
        formIsValid = false;
        errors["lastname"] = "*Please enter last name.";
      }

      if (typeof this.state.lastname !== "undefined") {
        if (!this.state.lastname.match(/^[a-zA-Z ]*$/)) {
          formIsValid = false;
          errors["lastname"] = "*Please enter alphabets characters only.";
        }
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.phone) {
        formIsValid = false;
        errors["phone"] = "*Please enter phone number.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/

      if (!this.state.address_line_1) {
        formIsValid = false;
        errors["address_line_1"] = "*Please enter address.";
      }

      if (!this.state.city) {
        formIsValid = false;
        errors["city"] = "*Please enter city.";
      }

      if (!this.state.state) {
        formIsValid = false;
        errors["state"] = "*Please enter state.";
      }

      if (!this.state.country) {
        formIsValid = false;
        errors["country"] = "*Please enter country.";
      }

      if (!this.state.zipcode) {
        formIsValid = false;
        errors["zipcode"] = "*Please enter zip code.";
      }

      if (typeof this.state.zipcode !== "undefined") {
        if (!this.state.zipcode.match(/(^\d{5}$)|(^\d{5}-\d{4}$)|(^\d{6}$)/)) {
          formIsValid = false;
          errors["zipcode"] = "*Please enter a valid zip code";
        }
      }
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
       if (this.validateForm()==true) {
          this.setState({ Loading: true });
          this.savedata();
    }
}

savedata = () => {
  var uid = this.state.forwarduId;
  const data = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      phone: this.state.phone,
      email: this.state.email,
      address_line_1: this.state.address_line_1,
      address_line_2: this.state.address_line_2,
      city: this.state.city,
      state: this.state.state,
      country: this.state.country,
      zipcode: this.state.zipcode,
      business_fb_page_id: this.state.business_fb_page_id,
      business_fb_page_access_token: this.state.business_fb_page_access_token,
  }
  userupdate(data, uid)
      .then(res => {
          if (res.status==true) {
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
              this.setState({ forwarduId: res.data.id,signup_success: true})  
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        this.setState({ Loading: false });      
      })
      .catch(err => {
          console.log(err);
      });

  }

  componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status==true){
              var userdata = res.data;
              this.setState({
                firstname: userdata.firstname,
                lastname: userdata.lastname,
                fullname: userdata.fullname,
                phone: userdata.phone,
                email: userdata.email,
                address_line_1: userdata.address_line_1,
                address_line_2: userdata.address_line_2,
                city: userdata.city,
                state: userdata.state,
                country: userdata.country,
                zipcode: userdata.zipcode,
                business_fb_page_id: userdata.business_fb_page_id,
                business_fb_page_access_token: userdata.business_fb_page_access_token
              });
            this.setState({ forwarduId: res.data.id})  
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });

  }

render(){

const {Loading} = this.state;

return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="row">
        <div className="col-md-12">
        <form className="needs-validation step_form mb-4" >
          <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center">Edit your profile information below </h1>
          <div className="row">
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="firstname" name="firstname" placeholder="First name" value={this.state.firstname} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.firstname}</span>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="lastname" name="lastname" placeholder="Last name" value={this.state.lastname} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.lastname}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="email" name="email" placeholder="Business email" value={this.state.email} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.email}</span>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group">
              <PhoneInput
                country="US"
                showCountrySelect={ false }
                name="phone"
                limitMaxLength={ true }
                placeholder="Enter phone number"
                value={ this.state.phone }
                onChange={ phone => this.setState({ phone }) } 
              />
              <span style={alertStyle}>{this.state.errors.phone}</span>
              { /* <input type="text" className="form-control input_custom_style" id="phone" name="phone" placeholder="Phone number" value={this.state.phone} onChange={this.handleChange}  />
              <span style={alertStyle}>{this.state.errors.phone}</span>*/}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="address_line_1" name="address_line_1" placeholder="Address line 1" value={this.state.address_line_1} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.address_line_1}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="address_line_2" name="address_line_2" placeholder="Address line 2" value={this.state.address_line_2} onChange={this.handleChange} />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="city" name="city" placeholder="City" value={this.state.city} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.city}</span>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="state" name="state" placeholder="State" value={this.state.state} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.state}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="country" name="country" placeholder="Country" value={this.state.country} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.country}</span>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="zipcode" name="zipcode" placeholder="Zipcode" value={this.state.zipcode} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.zipcode}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="business_fb_page_id" name="business_fb_page_id" placeholder="Business Facebook Page Id" value={this.state.business_fb_page_id} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.business_fb_page_id}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <div className="form-group">
              <input type="text" className="form-control input_custom_style" id="business_fb_page_access_token" name="business_fb_page_access_token" placeholder="Business Facebook Page Access Token" value={this.state.business_fb_page_access_token} onChange={this.handleChange} />
              <span style={alertStyle}>{this.state.errors.business_fb_page_access_token}</span>
            </div>
          </div>
        </div>
        <div className="text-center">
        <button type="button" className="blue_btn_box mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Update profile</button>
        </div>
      </form>
    </div>
    </div>
    <div>
      <MessengerCustomerChat
        pageId="415979359011219"
        appId="448749972524859"
      />
    </div>
    <ToastContainer autoClose={5000} />
  </div>
  </>
  );
 }
}

export default accountsettings;
