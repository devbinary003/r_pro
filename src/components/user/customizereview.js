import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

const getBusinessLocation = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const AddCustomizeReview = (data, blid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/businesslocation/addcustomizereview/'+blid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class customizereview extends React.Component {
  state = {
        fields: {},
        errors: {},
        blId: '',
        forwarduId: '',
        business_review_link:'',
        business_name:'',
        facebook_link:'',
        twitter_link:'',
        linkedin_link:'',
        instagram_link:'',
        client_satisfaction:'',
        signup_success:false,
        disablePurl: false,
        disablePurls: false,
        copied: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter business slug.";
      }

      if (typeof this.state.business_review_link !== "undefined") {
        if (!this.state.business_review_link.match(/^[A-Za-z_-][A-Za-z0-9_-]*$/)) {
          formIsValid = false;
          errors["business_review_link"] = "*Please enter alphanumeric, dash and underscore only.";
        }
      }

      /*if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter business slug.";
      }*/

    if (this.state.facebook_link) {
      if (typeof this.state.facebook_link !== "undefined") {
        if (!this.state.facebook_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["facebook_link"] = "*Please enter link only.";
        }
      }
    }

    if (this.state.twitter_link) {  
      if (typeof this.state.twitter_link !== "undefined") {
        if (!this.state.twitter_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["twitter_link"] = "*Please enter link only.";
        }
      }
    }  

    if (this.state.linkedin_link) {
      if (typeof this.state.linkedin_link !== "undefined") {
        if (!this.state.linkedin_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["linkedin_link"] = "*Please enter link only.";
        }
      }
    }  

    if (this.state.instagram_link) {
      if (typeof this.state.instagram_link !== "undefined") {
        if (!this.state.instagram_link.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
          formIsValid = false;
          errors["instagram_link"] = "*Please enter link only.";
        }
      }
    }  

    if (!this.state.client_satisfaction) {
      formIsValid = false;
      errors["client_satisfaction"] = "*Please select client satisfaction.";
    }

      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
       if (this.validateForm()==true) {
          this.savedata();
    }
}

savedata = () => {
  var blid = this.state.blId;
  const data = {
      user_id: this.state.forwarduId,
      business_review_link: this.state.business_review_link,
      facebook_link: this.state.facebook_link,
      twitter_link: this.state.twitter_link,
      linkedin_link: this.state.linkedin_link,
      instagram_link: this.state.instagram_link,
      client_satisfaction: this.state.client_satisfaction,
  }
  AddCustomizeReview(data, blid)
      .then(res => {
          if (res.status==true) {
                this.setState({
                 blId: res.data.id,
                 forwarduId: res.data.user_id,
                 signup_success: true,
                });
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
      })
      .catch(err => {
          console.log(err);
      });

  }

diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }
  
  componentDidMount() {
    getBusinessLocation()
      .then(res => {
          if(res.status==true){
              this.setState({
                blId: res.data.id,
                forwarduId: res.data.user_id,
                business_name: res.data.business_name,
                //business_review_link: res.data.business_review_link,
                //business_review_link: res.data.business_name,
                facebook_link: res.data.facebook_link,
                twitter_link: res.data.twitter_link,
                linkedin_link: res.data.linkedin_link,
                instagram_link: res.data.instagram_link,
                client_satisfaction: res.data.client_satisfaction,
              });
                var new_business_name = this.state.business_name.split(" ").join("");
                if(new_business_name.length > 10){ 
                  var new_str_business_name = new_business_name.substring(0,10);
                  this.setState({ business_review_link: new_str_business_name});
                }
              /*if(res.data.business_review_link==''){
                var new_business_name = this.state.business_name.split(" ").join("");
                if(new_business_name.length > 10){ 
                  var new_str_business_name = new_business_name.substring(0,10);
                  this.setState({ business_review_link: new_str_business_name});
                }
              } else {
                  this.setState({ business_review_link: res.data.business_review_link});
              }*/
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

render(){

if (this.state.signup_success) return <Redirect to={'/billing'} />

return (

  /*<div className="dashboard_body">

  <Route component={Header} />

  <div className="container-fluid useflex">*/
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12">
        <h1 className="black_bk_col fontweight500 font_16 mb-4 pb-1">Complete Account Sign Up</h1>
      </div>
      <div className="container">
          <div id="app">     
            <ol className="step-indicator">
               <li className="">
                  <div className="step_name">Step <span>1</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>FIND YOUR BUSINESS</span></div>
               </li>
               <li className="">
                  <div className="step_name">Step <span>2</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BUSINESS INFORMATION</span></div>
               </li>
               <li className="active">
                  <div className="step_name">Step <span>3</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>CUSTOMIZE REVIEW SYSTEM</span></div>
               </li>
               <li className="">
                  <div className="step_name">Step <span>4</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BILLING TO STRIPE</span></div>
               </li>
            </ol>

                  <div className="row step_two">
                    <div className="col-md-12">
                      <form className="needs-validation mb-4" >
                          <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Choose your main location's Business Review Link  <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="This is a specific URL hosted on the Review Pro Solutions website that would host your reviews. It is recommended to use your business name for this URL."></i> </h1>
                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <input disabled={!this.state.disablePurl} type="text" className="form-control input_custom_style" id="purl" name="purl" value={PROFILE_URL} />
                            </div>
                          </div>
                          
                          <div className="col-md-4">
                            <div className="form-group">
                          <input type="text" className="form-control input_custom_style" id="business_review_link" name="business_review_link" placeholder="business-name" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "business-name"} value={this.state.business_review_link} onChange={this.handleChange} />
                          <span style={alertStyle}>{this.state.errors.business_review_link}</span>
                            </div>
                          </div>
                          <div className="col-sm-2">
                            <CopyToClipboard text={PROFILE_URL+this.state.business_review_link}
                            onCopy={this.diasbleCopyMsg.bind(this)}>
                              <a className="inline_border_btn mr-1" style={{cursor:'pointer'}}>Copy</a>
                            </CopyToClipboard>      
                            {this.state.copied ? <span style={{color: 'green'}}>Copied.</span> : null}
                          </div>
                        </div>
                        
                        <h1 className="black_bk_col fontweight500 font_20 mb-4 mt-3 text-center"> Choose your review requirements for satisfied customers  <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="All your requested reviews will provide a star rating between 1 and 5. Please select whether you would like to include 4 & 5-star reviews or just 5-star reviews requesting a public review. Example: If you select “4 & 5-star reviews only”, all reviews with a 4 and 5-star review will be sent to Facebook & Google to leave a public review. However, if you choose “5 star reviews only”, the customers who provided a 4-star review will not be requested to leave a public review."></i> </h1>
                        <ul className="step_review">

                          <li className="step_review_li">
                            <div class="custom-control custom-radio text-right">

                              {this.state.client_satisfaction == '1'
                                ? <input type="radio" className="custom-control-input" id="customRadio2" name="client_satisfaction" value="1" checked onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                              : <input type="radio" className="custom-control-input" id="customRadio2" name="client_satisfaction" value="1" checked={this.state.client_satisfaction === '1'} onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                              }  

                            <label className="custom-control-label" htmlFor="customRadio2"></label>
                            </div>
                            <ul className="ratting_starr m-0">
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star-o" aria-hidden="true"></i></li>
                            </ul>
                            <p className="review_txt">4 & 5 star reviews only</p>
                          </li>

                          <li className="step_review_li">
                            <div class="custom-control custom-radio text-right">
                            
                            {this.state.client_satisfaction == '0'
                             ? <input type="radio" className="custom-control-input" id="customRadio" name="client_satisfaction" value="0" checked onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                            : <input type="radio" className="custom-control-input" id="customRadio" name="client_satisfaction" value="0" checked={this.state.client_satisfaction === '0'} onChange={(e) => this.setState({ client_satisfaction: e.target.value })} />
                            }  

                           <label className="custom-control-label" htmlFor="customRadio"></label>
                            </div>
                            <ul className="ratting_starr m-0">
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                              <li><i className="fa fa-star" aria-hidden="true"></i></li>
                            </ul>
                            <p className="review_txt">5 star reviews only</p>
                          </li>

                      <span style={alertStyle}>{this.state.errors.client_satisfaction}</span>    
                        </ul>
                        <h1 className="black_bk_col fontweight500 font_20 mb-4 mt-3 text-center"> Enter your social media accounts  <i style={{cursor:'pointer'}} className="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Please enter the full URL of your social media pages."></i> </h1>
                        
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-facebook" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="facebook_link" name="facebook_link" placeholder="Facebook Url" value={this.state.facebook_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.facebook_link}</span>

                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-twitter" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="twitter_link" name="twitter_link" placeholder="Twitter Url" value={this.state.twitter_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.twitter_link}</span>

                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-instagram" aria-hidden="true"></i></span>
                              </div>

                            <input type="text" className="form-control" id="instagram_link" name="instagram_link" placeholder="Instagram Url" value={this.state.instagram_link} onChange={this.handleChange} />
                            <span style={alertStyle}>{this.state.errors.instagram_link}</span> 
                            </div>
                          </div>
                          <div className="col-sm-12">
                            <div className="input-group mb-3">
                              <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-linkedin" aria-hidden="true"></i></span>
                              </div>

                          <input type="text" className="form-control" id="linkedin_link" name="linkedin_link" placeholder="Linkedin Url" value={this.state.linkedin_link} onChange={this.handleChange} />
                          <span style={alertStyle}>{this.state.errors.linkedin_link}</span>

                            </div>
                          </div>
                        </div>
                        <div className="text-center mt-4">
                        <button type="button" className="blue_btn_box" onClick={this.ValidateBasic}>Next</button>
                          <p className="m-0"><a href={`/businessinfo`}>Back</a></p>
                        </div>
                      </form>
                    </div>
                  </div>

          </div>
      </div>
      
      <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default customizereview;
