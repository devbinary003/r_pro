import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {CardElement, Elements, StripeProvider, CardNumberElement, CardExpiryElement, CardCVCElement, PostalCodeElement } from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import Alex133 from '../../images/Platform-for-Alex-xd_133.png';
import Alex135 from '../../images/Platform-for-Alex-xd_135.png';
import Alex137 from '../../images/Platform-for-Alex-xd_137.png';
import Alex139 from '../../images/Platform-for-Alex-xd_139.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class billing extends React.Component {
  state = {
        fields: {},
        errors: {},
        forwarduId: '',
        number_of_locations: '',
        item_qnty: '',
        total_sum: '',
        item_amt: 10,
        signup_success:false
    }

constructor(props) {
    super(props);
    this.state = {forwarduId: this.state.forwarduId};
  }
  
handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  } 

componentDidMount() {
    if(window.Stripe.setPublishableKey){
        //window.Stripe.setPublishableKey('pk_test_5BIIhP9MxnH5pkB2AnhdIAyR');
        window.Stripe.setPublishableKey('pk_live_oXya89obTzvBuki4idMNYMiV00dcFMi5CQ');
    }
    getUserDeatils()
      .then(res => {
          if(res.status==true){
              var userdata = res.data;
              this.setState({
                number_of_locations: userdata.number_of_locations,
                item_qnty: userdata.item_qnty,
                total_sum: userdata.total_sum,
              });
            this.setState({ forwarduId: res.data.id})  
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });

  } 

render(){

 if (this.state.signup_success) return <Redirect to={'/billing'} />

return (
    <>
    <Route component={Leftsidebar} />
    <div className="left-bar">
      <Route component={Header} />
      <div className="col-sm-12">
        <h1 className="black_bk_col fontweight500 font_16 mb-4 pb-1">Complete Account Sign Up</h1>
      </div>
      <div className="container">
          <div id="app">     
            <ol className="step-indicator">
               <li className="">
                  <div className="step_name">Step <span>1</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>FIND YOUR BUSINESS</span></div>
               </li>
               <li className="">
                  <div className="step_name">Step <span>2</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BUSINESS INFORMATION</span></div>
               </li>
               <li className="">  
                  <div className="step_name">Step <span>3</span></div>
                  <div className="step_border">
                     <div className="step_complete"><i className="fa fa-check-circle" aria-hidden="true"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>CUSTOMIZE REVIEW SYSTEM</span></div>
               </li>
               <li className="active">
                  <div className="step_name">Step <span>4</span></div>
                  <div className="step_border">
                     <div className="step"><i className="fa fa-circle"></i></div>
                  </div>
                  <div className="caption hidden-xs hidden-sm"><span>BILLING TO STRIPE</span></div>
               </li>
            </ol>
            <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center"> Pay with your credit card</h1>
                  <div className="row">

                    {/*<StripeProvider apiKey="pk_test_5BIIhP9MxnH5pkB2AnhdIAyR">*/}
                    <StripeProvider apiKey="pk_live_oXya89obTzvBuki4idMNYMiV00dcFMi5CQ">
                      <Elements>
                        <CheckoutForm forwarduId={this.state.forwarduId}/>
                      </Elements>
                    </StripeProvider>

                  </div>

          </div>
      </div>
      
       <div>
        <MessengerCustomerChat
          pageId="415979359011219"
          appId="448749972524859"
        />
      </div>
      <ToastContainer autoClose={5000} />
    </div>
    </>
  );
 }
}

export default billing;
