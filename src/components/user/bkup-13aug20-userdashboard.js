import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import StarRatingComponent from 'react-star-rating-component';
import ReadMoreReact from 'read-more-react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import { SketchPicker } from 'react-color';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import { LineChart, PieChart } from 'react-chartkick'
import 'chart.js'

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import ReadMoreAndLess from 'react-read-more-less';
import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import graphImg from '../../images/reviewpro/graph.jpg';
import graphImgTwo from '../../images/reviewpro/graph-2.jpg';
import googleLogo from '../../images/reviewpro/google-logo.png';
import facebook_logo from '../../images/reviewpro/facebook_logo.png';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getSingleBusinessLocation = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/singlebldetail/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getLiveRating = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/liverating/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getTopLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/toplivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/all/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const updateReviewLink = (data, cblid) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/user/editreviewlink/'+cblid, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyFacebookreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/getfacebookreviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyLivereviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/mylivereviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getmyInternalreviews = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/myinternalreviews/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/review/getalludbreview', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          }, 
          params: {
                ...data
          }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const getAverageLiveReviewRating = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/review/averagelivereviewsrating', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
          }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const getFilterLivereviews = (data, id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/getliveudbreview/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getFilterInternalreviews = (data, id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/getinternaludbreview/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}



const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class userdashboard extends React.Component {
  state = {
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        liverating: '',
        toplivereviews: [],
        allreviews: [],
        business_review_link: '',
        reviewer_name:'',
        reviewer_email:'',
        reviewer_rating:'',
        reviewer_description:'',
        business_name:'',
        business_address:'',
        fields: {},
        errors: {},
        copied: false,
        searchreview: '',
        enableShdoLive: false,
        totalLive: '',
        enableShdoIntAll: false,
        totalIntAll: '',
        invitecustomer:[],
        invitecustomercount:'',
        average_live_review_rating:'',
        reviewgivencount:'',
        total_live_review_rating:'',
        source_type:'',
        chart_type:'',
        all_time:'',
        start_date:'',
        end_date:'',
        fl_lv_source_type:'',
        fl_lv_all_time:'',
        fl_lv_all_rating:'',
        fltoplivereviews:[],
        fltopinternalreviews:[],
        //fl_in_source_type:'',
        fl_in_all_time:'',
        fl_in_all_rating:'',
    }

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
      var tName = event.target.name;
      var tValue = event.target.value;
      if(tName == 'blIdnew' && tValue!='') {
        this.setState({ blId: tValue });
          this.refreshSingleBlDeatils(tValue);
          this.refreshLiveRating(tValue);
          this.refreshTopLiveReviews(tValue);
          this.refreshAllReviews(tValue);
          //this.refreshgetAllInviteCustomerWithMonth(tValue);
      }
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.business_review_link) {
        formIsValid = false;
        errors["business_review_link"] = "*Please enter your business review link.";
      }

      if (typeof this.state.business_review_link !== "undefined") {
        if (!this.state.business_review_link.match(/^[A-Za-z_-][A-Za-z0-9_-]*$/)) {
          formIsValid = false;
          errors["business_review_link"] = "*Please enter alphanumeric, dash and underscore only.";
        }
      }

      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.savedata();
    }
  }

  savedata = () => {
    var cblid = this.state.blId;
    const data = {
        business_review_link: this.state.business_review_link,
    }
    updateReviewLink(data, cblid)
        .then(res => {
            if (res.status==true) {
                  toast.success(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              } else {
                  toast.error(res.message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                  });
              }
        })
        .catch(err => {
            console.log(err);
        });
    }

  handleAllTime = event  => {
    var id = this.state.blId;
    var all_time_val = event.target.value;
    this.setState({all_time: all_time_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ invitecustomer: records });
          console.log(this.state.invitecustomer);
        } else {
          this.setState({ invitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleSourceType = event  => {
    var id = this.state.blId;
    var source_type_val = event.target.value;
    this.setState({source_type: source_type_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ invitecustomer: records });
          console.log(this.state.invitecustomer);
        } else {
          this.setState({ invitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleChartType = event  => {
    var id = this.state.blId;
    var chart_type_val = event.target.value;
    this.setState({chart_type: chart_type_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ invitecustomer: records });
          console.log(this.state.invitecustomer);
        } else {
          this.setState({ invitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleCustomStartDate = event  => {
    var id = this.state.blId;
    var start_date_val = event.target.value;
    this.setState({start_date: start_date_val});
  } 

  handleCustomEndDate = event  => {
    var id = this.state.blId;
    var end_date_val = event.target.value;
    this.setState({end_date: end_date_val});
    var start_date = document.getElementById('start_date').value;
    var end_date = document.getElementById('end_date').value;
    if(start_date > end_date){
      alert('please select end data more than of start date.');
    } else if(start_date=='') {
      alert('please select start date');
    } else {
      const data = {
        all_time: document.getElementById('all_time').value,
        source_type: document.getElementById('source_type').value,
        chart_type: document.getElementById('chart_type').value,
        start_date: document.getElementById('start_date').value,
        end_date: document.getElementById('end_date').value,
        user_id: localStorage.getItem(USER_ID),
      }
      getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ invitecustomer: records });
          console.log(this.state.invitecustomer);
        } else {
          this.setState({ invitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }
  }
  ResetFilters = () => {
    this.setState({ source_type: '' });
    this.setState({ chart_type: '' });
    this.setState({ all_time: '' });
    this.setState({ start_date: '' });
    this.setState({ end_date: '' });
    this.setState({ invitecustomer: '' });
    
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      start_date: document.getElementById('start_date').value,
      end_date: document.getElementById('end_date').value,
      user_id: localStorage.getItem(USER_ID),
    }
    getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        this.setState({ invitecustomer: records });
        console.log(this.state.invitecustomer);
      } else {
        this.setState({ invitecustomer: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
  }
  handleFlLvSourceType = event => {
    var id = this.state.blId;
    var all_source_type_val = event.target.value;
    this.setState({ fl_lv_source_type : all_source_type_val});
    const data = {
      source_type: document.getElementById("fl_lv_source_type").value,
      all_rating: document.getElementById("fl_lv_all_rating").value,
      all_time: document.getElementById("fl_lv_all_time").value,
    }
    getFilterLivereviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data.data;
            this.setState({ fltoplivereviews: records });
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
        } else {
          this.setState({ fltoplivereviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  } 
  handleFlLvAllRating = event =>{
    var id = this.state.blId;
    var all_rating_val = event.target.value;
    this.setState({ fl_lv_all_rating : all_rating_val});
    const data = {
      source_type: document.getElementById("fl_lv_source_type").value,
      all_rating: document.getElementById("fl_lv_all_rating").value,
      all_time: document.getElementById("fl_lv_all_time").value,
    }
    getFilterLivereviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data.data;
            this.setState({ fltoplivereviews: records });
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
        } else {
          this.setState({ fltoplivereviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }
  handleFlLvAllTime = event =>{
    var id = this.state.blId;
    var all_time_val = event.target.value;
    this.setState({ fl_lv_all_time : all_time_val});
    const data = {
      source_type: document.getElementById("fl_lv_source_type").value,
      all_rating: document.getElementById("fl_lv_all_rating").value,
      all_time: document.getElementById("fl_lv_all_time").value,
    }
    getFilterLivereviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data.data;
            this.setState({ fltoplivereviews: records });
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
        } else {
          this.setState({ fltoplivereviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }
  ResetLiveReviewsFilters = () => {
    var id = this.state.blId;
    this.setState({ fl_lv_source_type: ''});
    this.setState({ fl_lv_all_rating: ''});
    this.setState({ fl_lv_all_time: ''});
    this.setState({ fltoplivereviews: '' });
    getmyLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }

  /*handleFlInSourceType = event => {
    var id = this.state.blId;
    var all_source_type_val = event.target.value;
    this.setState({ fl_in_source_type : all_source_type_val});
    const data = {
      intr_source_type: document.getElementById("fl_in_source_type").value,
      intr_all_rating: document.getElementById("fl_in_all_rating").value,
      intr_all_time: document.getElementById("fl_in_all_time").value,
    }
    getFilterInternalreviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data;
            this.setState({ fltopinternalreviews: records });*/
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
       /* } else {
          this.setState({ fltopinternalreviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }*/ 
  handleFlInAllRating = event =>{
    var id = this.state.blId;
    var all_rating_val = event.target.value;
    this.setState({ fl_in_all_rating : all_rating_val});
    const data = {
      /*intr_source_type: document.getElementById("fl_in_source_type").value,*/
      intr_all_rating: document.getElementById("fl_in_all_rating").value,
      intr_all_time: document.getElementById("fl_in_all_time").value,
    }
    getFilterInternalreviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data;
            this.setState({ fltopinternalreviews: records });
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
        } else {
          this.setState({ fltopinternalreviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }
  handleFlInAllTime = event =>{
    var id = this.state.blId;
    var all_time_val = event.target.value;
    this.setState({ fl_in_all_time : all_time_val});
    const data = {
      /*intr_source_type: document.getElementById("fl_in_source_type").value,*/
      intr_all_rating: document.getElementById("fl_in_all_rating").value,
      intr_all_time: document.getElementById("fl_in_all_time").value,
    }
    getFilterInternalreviews(data, id)
      .then(res => {
        if(res.status==true){
            var records = res.data;
            this.setState({ fltopinternalreviews: records });
            /*this.setState({ total: res.data.total });
            this.setState({ currentPage: res.data.current_page });
            this.setState({ PerPage: res.data.per_page });
            this.setState({ FirstPageUrl: res.data.first_page_url });
            this.setState({ NextPageUrl: res.data.next_page_url });
            this.setState({ PrevPageUrl: res.data.prev_page_url });
            this.setState({ LastPageUrl: res.data.last_page_url });
            this.setState({ LastPage: res.data.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });*/
        } else {
          this.setState({ fltopinternalreviews: '' });
          toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });
        }
        this.setState({ enableShdo: false, });
      })
      .catch(err => {
          console.log(err);
      });
  }
  ResetInternalReviewsFilters = () => {
    var id = this.state.blId;
    this.setState({ fl_in_all_rating: ''});
    this.setState({ fl_in_all_time: ''});
    this.setState({ fltopinternalreviews: '' });
    getAllreviews(id)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        if(res.data.length > 0) {
          this.setState({ totalIntAll: res.data.length });
        } else {
          this.setState({ totalIntAll: 0 });
        }
        this.setState({ allreviews: records });
      } else {
        this.setState({ allreviews: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
  }
refreshBlList = () => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
          this.refreshSingleBlDeatils(res.data[0].id);
          this.refreshLiveRating(res.data[0].id);
          this.refreshmyInternalReviews(res.data[0].id);
          //this.refreshgetAllInviteCustomerWithMonth(res.data[0].id);
          if(res.data[0].lrvstatus==0) {
            this.refreshmyLiveReviews(res.data[0].id);
            this.handelWrnMsg();
          } else {
            this.refreshTopLiveReviews(res.data[0].id);
          }
          this.refreshAllReviews(res.data[0].id);
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    } 

refreshSingleBlDeatils = (id) => {
    getSingleBusinessLocation(id)
      .then(res => {
          if(res.status==true){
              this.setState({
                business_review_link: res.data.business_review_link,
                business_name: res.data.business_name,
                business_address: res.data.business_address,
              });
          } else {
            this.setState({ business_review_link: '' });
            /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
          }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshLiveRating = (id) => {
  getLiveRating(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ liverating: records });
        } else {
          this.setState({ liverating: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshTopLiveReviews = (id) => {
 // this.setState({ enableShdoLive: true, });
  getTopLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
          console.log(this.state.toplivereviews);
        } else {
          this.setState({ toplivereviews: '' });
        }
      //  this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshAllReviews = (id) => {
//  this.setState({ enableShdoIntAll: true, });
  getAllreviews(id)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        if(res.data.length > 0) {
          this.setState({ totalIntAll: res.data.length });
        } else {
          this.setState({ totalIntAll: 0 });
        }
        this.setState({ allreviews: records });
      } else {
        this.setState({ allreviews: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
  }

refreshmyLiveReviews = (id) => {
  this.setState({ enableShdoLive: true, });
  getmyFacebookreviews(id)
  getmyLivereviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalLive: res.data.length });
          } else {
            this.setState({ totalLive: 0 });
          }
          this.setState({ toplivereviews: records });
        } else {
          this.setState({ toplivereviews: '' });
        }
        this.setState({ enableShdoLive: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshmyInternalReviews = (id) => {
  //this.setState({ enableShdoIntAll: true, });
  getmyInternalreviews(id)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          if(res.data.length > 0) {
            this.setState({ totalIntAll: res.data.length });
          } else {
            this.setState({ totalIntAll: 0 });
          }
          this.setState({ allreviews: records });
        } else {
          this.setState({ allreviews: '' });
        }
        this.setState({ enableShdoIntAll: false, });
      })
      .catch(err => {
          console.log(err);
      });
    }

refreshgetAllInviteCustomerWithMonth = () => {
   const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        this.setState({ invitecustomer: records });
      } else {
        this.setState({ invitecustomer: '' });
        
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAverageLiveReviewRating = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAverageLiveReviewRating(data)
    .then(res => {
      if(res.status==true){
        this.setState({ average_live_review_rating: res.success });
        this.setState({ total_live_review_rating: res.total_review_rating });
      } else {
        this.setState({ average_live_review_rating: '' });
        this.setState({ total_live_review_rating: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

diasbleCopyMsg() {
  this.setState({ copied: true });
  this.change = setTimeout(() => {
    this.setState({copied: false})
  }, 5000)
}

handelWrnMsg() {
    if(this.state.enableShdoLive==true) {
     window.WrnMsg();
    } else {
    }
}

constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
  
componentDidMount() {
  window.scrollTo(0, 0);
  this.refreshBlList();
  this.refreshgetAllInviteCustomerWithMonth();
  this.refreshgetAllInviteCustomerReviewGivenCount();
  this.refreshgetAverageLiveReviewRating();
  this.refreshmyInternalReviews();
}

  render(){
    const line_options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2", // "light1", "dark1", "dark2"
      title:{
        text: "Reviews Of Month"
      },
      axisY: {
        title: "Reviews",
        includeZero: false,
        suffix: "%"
      },
      axisX: {
        title: "Review of Month",
        prefix: "M",
        interval: 2
      },
      data: [{
        type: "line",
        toolTipContent: "Month {x}: {y}%",
        dataPoints: [
        
         /*this.state.invitecustomer*/
        {"x":12,"y":1},
        {"x":17,"y":1},
        {"x":19,"y":1},
        {"x":21,"y":5},
        {"x":25,"y":2}
         /* { x: 1, y: 64 },
          { x: 2, y: 61 },
          { x: 3, y: 64 },
          { x: 4, y: 62 },
          { x: 5, y: 64 },
          { x: 6, y: 60 },
          { x: 7, y: 58 },
          { x: 8, y: 59 },
          { x: 9, y: 53 },
          { x: 10, y: 54 },
          { x: 11, y: 61 },
          { x: 12, y: 60 },
          { x: 13, y: 55 },
          { x: 14, y: 60 },
          { x: 15, y: 56 },
          { x: 16, y: 60 },
          { x: 17, y: 59.5 },
          { x: 18, y: 63 },
          { x: 19, y: 58 },
          { x: 20, y: 54 },
          { x: 21, y: 59 },
          { x: 22, y: 64 },
          { x: 23, y: 59 }*/
        ]
      }]
    }
    const average_live_review_pie_options = {
      animationEnabled: true,
      title: {
        text: "Review By Star Rating"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Average Star Rating", y: this.state.average_live_review_rating, color: "#ffdc04" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    const reviews_source_pie_options = {
      animationEnabled: true,
      title: {
        text: "Total Reviews"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Total Reviews", y: this.state.total_live_review_rating, color: "#6954bb" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }

  let lv_list;
  let flt_data = this.state.fltoplivereviews.length;
  if(flt_data==0){
    lv_list =  this.state.toplivereviews.length > 0
                  ?
                  this.state.toplivereviews.map(toplivereview => {
                    return (
                      <tr>
                        <td>
                          <Moment format="LL"> 
                            {toplivereview.live_review_datetime}
                          </Moment>
                        </td>
                        <td> 
                          {
                            toplivereview.review_type=='Google' ?
                              <img src={googleLogo} alt="google-logo" className="img-responsive" />
                            :
                              <img src={facebook_logo} alt="facebook-logo" className="img-responsive" />
                          }
                          { toplivereview.review_type=='Google' ? 'Google' : 'Facebook'}
                        </td>
                        <td><StarRatingComponent name="rate2" editing={false} starCount={5} value={toplivereview.live_review_rating} /></td>
                        <td><ReadMoreAndLess ref={this.ReadMore} className="read-more-content" charLimit={30} readMoreText="Read more" readLessText="Read less">{toplivereview.live_review_text}</ReadMoreAndLess></td>
                        <td>{toplivereview.live_review_reviewer}</td>
                        {/*<td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>*/}
                      </tr>
                    );
                  })
                  :
                  <tr><td colspan="6" className="font_12 txt_col fontweight400 " style={txtCenter}> There are currently no Live Reviews.</td></tr>
  } else {
    lv_list = this.state.fltoplivereviews.length > 0
                  ?
                    this.state.fltoplivereviews.map(fl_toplivereviews => {
                      return (
                        <tr>
                          <td>
                            <Moment format="LL"> 
                              {fl_toplivereviews.live_review_datetime}
                            </Moment>
                          </td>
                          <td> 
                            {
                              fl_toplivereviews.review_type=='Google' ?
                                <img src={googleLogo} alt="google-logo" className="img-responsive" />
                              :
                                <img src={facebook_logo} alt="facebook-logo" className="img-responsive" />
                            }
                            { fl_toplivereviews.review_type=='Google' ? 'Google' : 'Facebook'}
                          </td>
                          <td><StarRatingComponent name="rate2" editing={false} starCount={5} value={fl_toplivereviews.live_review_rating} /></td>
                          <td><ReadMoreAndLess ref={this.ReadMore} className="read-more-content" charLimit={30} readMoreText="Read more" readLessText="Read less">{fl_toplivereviews.live_review_text}</ReadMoreAndLess></td>
                          <td>{fl_toplivereviews.live_review_reviewer}</td>
                          {/*<td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>*/}
                        </tr>
                      );
                    })
                  :
                    <tr><td colspan="6" className="font_12 txt_col fontweight400 " style={txtCenter}> There are currently no Live Reviews.</td></tr>
  }

  let in_list;
  let flt_in_data = this.state.fltopinternalreviews.length;
  if(flt_in_data==0){
    in_list =  this.state.allreviews.length > 0
                ?  
                  this.state.allreviews.map(allreview => {
                    return <tr>
                              <td>
                                <Moment format="LL">
                                  {allreview.created_at}
                                </Moment>
                              </td>
                              <td>{allreview.reviewer_email}</td>
                              {/*<td> 
                                {
                                  allreview.review_type=='Google' ?
                                    <img src={googleLogo} alt="google-logo" className="img-responsive" />
                                  :
                                    <img src={facebook_logo} alt="facebook-logo" className="img-responsive" />
                                }
                                { allreview.review_type=='Google' ? 'Google' : 'Facebook'}
                              </td>*/}
                              <td><StarRatingComponent name="rate2" editing={false} starCount={5} value={allreview.reviewer_rating} /></td>
                              <td>{allreview.reviewer_description}</td>
                              <td>{allreview.reviewer_name}</td>
                              {/*<td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>*/}
                          </tr>
                  })
                :
                <tr>
                  <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                </tr>
  } else {
    in_list = this.state.fltopinternalreviews.length > 0
                ?  
                  this.state.fltopinternalreviews.map(fltopinternalreview => {
                    return <tr>
                              <td>
                                <Moment format="LL">
                                  {fltopinternalreview.created_at}
                                </Moment>
                              </td>
                              <td>{fltopinternalreview.reviewer_email}</td>
                              {/*<td> 
                                {
                                  fltopinternalreview.review_type=='Google' ?
                                    <img src={googleLogo} alt="google-logo" className="img-responsive" />
                                  :
                                    <img src={facebook_logo} alt="facebook-logo" className="img-responsive" />
                                }
                                { fltopinternalreview.review_type=='Google' ? 'Google' : 'Facebook'}
                              </td>*/}
                              <td><StarRatingComponent name="rate2" editing={false} starCount={5} value={fltopinternalreview.reviewer_rating} /></td>
                              <td>{fltopinternalreview.reviewer_description}</td>
                              <td>{fltopinternalreview.reviewer_name}</td>
                              {/*<td><a href="#" className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>*/}
                          </tr>
                  })
                :
                <tr>
                  <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                </tr>
    
  }
    return(
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        {/*<div className="bg-color"></div>*/} 
        <div className="button-part">
          {/*<button className="part-of-btn">All Reviews</button>
          <button className="part-of-btn">New Reviews</button>*/}
          <select name="source_type" className="part-of-btn" id="source_type" value={this.state.source_type} onChange={this.handleSourceType}>
            <option value="all">All Sources</option>
            <option value="Google">Google</option>
            <option value="Facebook">Facebook</option>
          </select>
          <select name="chart_type" className="part-of-btn" id="chart_type" value={this.state.chart_type} onChange={this.handleChartType}>
            {/*<option value="">Chart type</option>*/}
            <option value="daily">Daily</option>
            <option value="weekly">Weekly</option>
            <option value="monthly">Monthly</option>
            <option value="yearly">Yearly</option>
          </select> 
          <select name="all_time" className="part-of-btn" id="all_time" onChange={this.handleAllTime} value={this.state.all_time}>
            {/*<option value="">All Time</option>*/}
            <option value="one_month">1 month</option>
            <option value="three_month">3 months</option>
            <option value="six_month">6 months</option>
            <option value="twelve_month">12 months</option>
            <option value="custom_date">Custom Dates</option>
          </select>
          
          <input type="date" name="start_date" id="start_date" className="part-of-btn" value={this.state.start_date} disabled={this.state.all_time != 'custom_date' ? true : false} onChange={this.handleCustomStartDate}/>
          <input type="date" name="end_date" id="end_date" className="part-of-btn" value={this.state.end_date} disabled={this.state.all_time != 'custom_date' ? true : false} onChange={this.handleCustomEndDate}/>
          <button className="part-of-btn reset_btn" onClick={this.ResetFilters}> Reset</button>
        </div>
        <div className="left-part-design"> 
          <div className="container ">
            <div className="row">
              <div className="col-sm-4 shot-part">
                <h2>Overview</h2>
                <p>All-Time Rating & Reviews</p>
                <div className="average">
                  <p>Average Star Rating</p>
                  <h2>{this.state.average_live_review_rating}</h2>
                  <StarRatingComponent name="rate2" editing={false} starCount={5} value={this.state.average_live_review_rating} />
                </div>
                <div className="average part-aver">
                  <p>Total Reviews</p>
                  <h2>{this.state.total_live_review_rating}</h2> 
                </div>
              </div>
              <div className="col-sm-8 big-part">
                <div className="row">
                  <div className="col-sm-6">
                   <h2>Collected Reviews</h2> 
                  </div>
                  {/*<div className="col-sm-6">
                   <p><a href="/sendreviewinvite">Send Review Invite</a></p>
                  </div>*/}
                </div>
                 {/*<CanvasJSChart options = {line_options} />*/}                
                  <LineChart data={this.state.invitecustomer} />
                 {/*<img src={graphImg} alt="graph" className="img-responsive" />*/}
              </div>
            </div>
          </div>
        </div>
        <div className="row chart_section">
          <div className="col-sm-6 left_chart_section graph-img">
            {/*<CanvasJSChart options = {average_live_review_pie_options} />*/}
            <PieChart data={{"Average Star Rating": this.state.average_live_review_rating}} colors={["#ffdc04"]} />
          </div>
          <div className="col-sm-6 right_chart_section graph-img">
            {/*<CanvasJSChart options = {reviews_source_pie_options} />*/}
            <PieChart data={{"Total Reviews": this.state.total_live_review_rating}} colors={["#6954bb"]} />
          </div>
        </div>
        <div className="search-part">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 search-bar">
                <div className="row">
                  <div className="col-sm-2 fa-icon"><i className="fa fa-map-marker" aria-hidden="true"></i></div>
                  <div className="col-sm-10 location">
                    <p><b>Select Business Location</b></p>
                    <select className="part-of-btn business_location_list" id="cars" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} disabled="disabled">
                      {
                        this.state.businesslocationlists.length > 0
                        ?  
                          this.state.businesslocationlists.map(bllist => {
                            return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
                          })
                        :
                          <option> Select Business Location</option>
                      } 
                    </select> 
                  </div>
                </div>
              </div>
              <div className="col-sm-4 search-reviews">
                <div className="row">
                  <div className="col-sm-9 location-2">
                    <p><b>Search Reviews</b></p>
                    <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="ser-part" />
                    <p style={alertStyle}>{this.state.errors.searchreview}</p>
                  </div>
                  <div className="col-sm-3 fa-icon-2"><i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="table-part">
          <h2>Live Reviews</h2>
          <div className="row button-part">
            <div className="col-sm-3">
              <p>Showing 1 to 4 Results</p>
            </div>
            <div className="col-sm-9"> 
              <select name="fl_lv_source_type" className="part-of-btn" id="fl_lv_source_type" onChange={this.handleFlLvSourceType} value={this.state.fl_lv_source_type}>
                <option value="all">All Sources</option>
                <option value="Google">Google</option>
                <option value="Facebook">Facebook</option>
              </select>
              <select name="fl_lv_all_rating" className="part-of-btn" id="fl_lv_all_rating" onChange={this.handleFlLvAllRating} value={this.state.fl_lv_all_rating}>
                <option value="all">All Ratings</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option> 
              </select>
              <select name="fl_lv_all_time" className="part-of-btn" id="fl_lv_all_time" onChange={this.handleFlLvAllTime} value={this.state.fl_lv_all_time}>
                <option value="one_month">1 month</option>
                <option value="three_month">3 months</option>
                <option value="six_month">6 months</option>
                <option value="twelve_month">12 months</option>
              </select>
              <button className="part-of-btn reset_btn" onClick={this.ResetLiveReviewsFilters}> Reset</button>
              {/*<button className="part-of-btn"> CSV</button>*/}
            </div>
          </div>
          <table>
            <tr>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Date</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Source</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Rating</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Review</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Reviewer Name</th>
              {/*<th style={{color: "#a5a5a5", fontWeight: "200"}}>Actions</th>*/}
            </tr>
            {lv_list}
          </table>
          <div className="row bot-area">
            <div className="col-sm-6 right-show"><p>Showing 1 to 4 of Result</p></div>
            <div className="col-sm-6 left-show">
              <p><a href="/livereviews">View All</a></p>
            </div>
          </div>
        </div>

        <div className="table-part">
          <h2>Internal Reviews</h2>
          <div className="row button-part">
            <div className="col-sm-3">
              <p>Showing 1 to 4 Results</p>
            </div>
            <div className="col-sm-9"> 
              {/*<select name="fl_in_source_type" className="part-of-btn" id="fl_in_source_type" onChange={this.handleFlInSourceType}>
                <option value="all">All Sources</option>
                <option value="Google">Google</option>
                <option value="Facebook">Facebook</option>
              </select>*/}
              <select name="fl_in_all_rating" className="part-of-btn" id="fl_in_all_rating" onChange={this.handleFlInAllRating} value={this.state.fl_in_all_rating}>
                <option value="all">All Ratings</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option> 
              </select>
              <select name="fl_in_all_time" className="part-of-btn" id="fl_in_all_time" onChange={this.handleFlInAllTime} value={this.state.fl_in_all_time}>
                <option value="one_month">1 month</option>
                <option value="three_month">3 months</option>
                <option value="six_month">6 months</option>
                <option value="twelve_month">12 months</option>
              </select>
              <button className="part-of-btn reset_btn" onClick={this.ResetInternalReviewsFilters}> Reset</button>
              {/*<button className="part-of-btn"> CSV</button>*/}
            </div>
          </div>
          <table>
            <tr>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Date</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Email</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Rating</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Review</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Reviewer Name</th>
              {/*<th style={{color: "#a5a5a5", fontWeight: "200"}}>Actions</th>*/}
            </tr>
              {in_list}
          </table>
          <div className="row bot-area">
            <div className="col-sm-6 right-show"><p>Showing 1 to 4 of Result</p></div>
            <div className="col-sm-6 left-show">
              <p><a href="/internalreviews">View All</a></p>
            </div>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div> 
        {/*<ToastContainer autoClose={5000} />
        <div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
        <div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>*/}   
      </div>
      </>
    );
  }
}

export default userdashboard;
