import React from 'react';
import { API_TOKEN_NAME, USER_ID } from '../../constants';
import { scAxios } from '../..';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import logoWhite from '../../images/Logo-01.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Leftsidebar extends React.Component {

  state = {
        isprofilecomplete: '',
    }
  
  handleClick() {
    toast.error("Welcome to Review Pro Solutions. Please first provide us some quick information about your business", {
              position: toast.POSITION.BOTTOM_RIGHT
            });
  }

  componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status===true){
              var userdata = res.data;
              this.setState({
                isprofilecomplete: userdata.isprofilecomplete,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

    render() {
        let url;
        if(window.location.hostname==="localhost"){
          url = 'http://'+window.location.hostname+':3000';
        } else {
          url = 'https://'+window.location.hostname;
        }
      return (
        <>
        <div className="right-bar">
          <div className="right-bar-2">
            <p><a href="/"><p className="sidebar_logo_sec"><img src={logoWhite} alt="logo" className="img-responsive sidebar_logo"/></p><p className="powered_by_text">Powered by Review Pro Solutions.</p></a></p>
            { this.state.isprofilecomplete === '1' ?
            (<ul className="das-link dropdown_desktop">
                <a href="/userdashboard"><li className={window.location.href=== url+'/userdashboard'? 'active':''}> Dashboard </li></a>
                <a href="/sendreviewinvite"><li className={window.location.href=== url+'/sendreviewinvite'? 'active':''}> Send Review Invite </li></a>
                <a href="/internalreviews"><li className={window.location.href=== url+'/internalreviews'? 'active':''}> Internal Reviews </li></a>
                <a href="/livereviews"><li className={window.location.href=== url+'/livereviews'? 'active':''}> Monitor Reviews </li></a>
                <a href="/managestaff"><li className={window.location.href=== url+'/managestaff'? 'active':''}> Manage Staff </li></a>
                <a href="/systemsettings"><li className={window.location.href=== url+'/systemsettings'? 'active':''}> System Settings </li></a>
                <a href="/contact" target="_blank"><li> Get Help </li></a>
             </ul>)
            :
            (<ul className="das-link dropdown_desktop">
                <a href="#"><li className="active"> Dashboard </li></a>
                <a href="#"><li> Send Review Invite </li></a>
                <a href="#"><li> Internal Reviews </li></a>
                <a href="#"><li> Monitor Reviews </li></a>
                <a href="#"><li> Manage Staff </li></a>
                <a href="#"><li> System Settings </li></a>
                <a href="#"><li> Get Help </li></a>
             </ul>)
            }
            { this.state.isprofilecomplete === '1' ?
              (<div className="dropdown dropdown_mobile">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-bars" aria-hidden="true"></i> Menu
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a className={window.location.href=== url+'/userdashboard'? 'dropdown-item active':'dropdown-item'} href="/userdashboard"><li> Dashboard </li></a>
                  <a className={window.location.href=== url+'/sendreviewinvite'? 'dropdown-item active':'dropdown-item'} href="/sendreviewinvite"><li> Send Review Invite </li></a>
                  <a className={window.location.href=== url+'/internalreviews'? 'dropdown-item active':'dropdown-item'} href="/internalreviews"><li> Internal Reviews </li></a>
                  <a className={window.location.href=== url+'/livereviews'? 'dropdown-item active':'dropdown-item'} href="/livereviews"><li> Monitor Reviews </li></a>
                  <a className={window.location.href=== url+'/managestaff'? 'dropdown-item active':'dropdown-item'} href="/managestaff"><li> Manage Staff </li></a>
                  <a className={window.location.href=== url+'/systemsettings'? 'dropdown-item active':'dropdown-item'} href="/systemsettings"><li> System Settings </li></a>
                  <a className="dropdown-item" href="/contact" target="_blank"><li> Get Help </li></a>
                </div>
              </div>)
              :
              (<div className="dropdown dropdown_mobile">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-bars" aria-hidden="true"></i> Menu
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a className="dropdown-item" href="#"><li> Dashboard </li></a>
                  <a className="dropdown-item" href="#"><li> Send Review Invite </li></a>
                  <a className="dropdown-item" href="#"><li> Internal Reviews </li></a>
                  <a className="dropdown-item" href="#"><li> Monitor Reviews </li></a>
                  <a className="dropdown-item" href="#"><li> Manage Staff </li></a>
                  <a className="dropdown-item" href="#"><li> System Settings </li></a>
                  <a className="dropdown-item" href="#"><li> Get Help </li></a>
                </div>
              </div>)
            }
          </div>
        </div>     
        </>
        );
    }
}

export default Leftsidebar;
