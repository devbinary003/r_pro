import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, PROFILE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'react-image-crop/dist/ReactCrop.css';
import Parser from 'html-react-parser';
import $ from 'jquery';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

import { LineChart, PieChart } from 'react-chartkick';
import 'chart.js';

import Header from './Header.js';
import Footer from './Footer.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';
import StarRatingComponent from 'react-star-rating-component';

import Moment from 'react-moment';
import 'moment-timezone';
import Popup from "reactjs-popup";
import MessengerCustomerChat from 'react-messenger-customer-chat';
import Pagination from "react-js-pagination";

import dbImg from '../../images/dashboard.png';
import profileImg from '../../images/profile_img.png';
import menuImg from '../../images/menu.png';
import shoppingLlist from '../../images/shopping-list.png';
import settingsImg from '../../images/settings.png';
import lifesaverImg from '../../images/lifesaver.png';
import dbfooterLogo from '../../images/dashboard_footer_logo.png';

import LivloadngImg from '../../images/lvrlbilling.gif';

import graphImg from '../../images/reviewpro/graph.jpg';
import graphImgTwo from '../../images/reviewpro/graph-2.jpg';
import googleLogo from '../../images/reviewpro/google-logo.png';

import CanvasJSReact from '../../js/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;

const getBusinessLocationLists = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/businesslocation/filterbl/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getallInviteUser = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getallinvitecustomer', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getAllInviteCustomerWithMonth = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/review/getalludbreview', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}
const getAllInviteCustomerReviewGivenCount = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/invitecustomer/getallinvitecustomerreviewgivencount', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
                ...data
            }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const getAverageLiveReviewRating = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/review/averagelivereviewsrating', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
            ...data
          }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};

const txtCenter = {
  textAlign: 'center',
};

class staffdashboard extends React.Component {
  state = {
        inviteusercustomer: [],
        blId: '',
        blIdnew: '',
        businesslocationlists: [],
        searchreview: '',
        total: '',
        inviteuserid: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        activePage: 1,
        enableShdo: false,
        enableShdoLive: false,
        invite_customer_id:'',
        invitecustomer:[],
        flinvitecustomer:[],
        invitecustomercount:'',
        reviewgivencount:'',
        average_live_review_rating:'',
        reviewgivencount:'',
        total_live_review_rating:'',
        source_type:'',
        chart_type:'',
        all_time:'',
        start_date:'',
        end_date:'',
    }

  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
    this.refreshReviewsList(pageNumber);
  }
  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });  
  }
  validateSearchReviewForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;
      if (!this.state.searchreview) {
        formIsValid = false;
        errors["searchreview"] = "*Enter search key word.";
      }
      this.setState({
        errors: errors
      });
      return formIsValid;
  }  
  
  validateSearchReview = event => {
    if (this.validateSearchReviewForm()==true) {
       this.handleSearchReview();
    }
  } 

  handleSearchReview = () => {
    var cblid = this.state.blId;
    var searchKey = this.state.searchreview;
    if(cblid!='' && searchKey!='') {
      window.location.href = '/search/'+ cblid +'/?query='+ searchKey;
    }
  } 
  refreshInviteUserListing = (page) => {
    const data = {
        page: page,
        user_id: localStorage.getItem(USER_ID),
    }
    getallInviteUser(data)
        .then(res => {
          if(res.status==true){
            var records = res.data;
            this.setState({ inviteusercustomer: records.data });
            this.setState({ total: res.success.total });
            this.setState({ currentPage: res.success.current_page });
            this.setState({ PerPage: res.success.per_page });
            this.setState({ FirstPageUrl: res.success.first_page_url });
            this.setState({ NextPageUrl: res.success.next_page_url });
            this.setState({ PrevPageUrl: res.success.prev_page_url });
            this.setState({ LastPageUrl: res.success.last_page_url });
            this.setState({ LastPage: res.success.last_page });
            this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
          } else {
            this.setState({ inviteusercustomer: '' });
          }
          this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  }  

handleAllTime = event  => {
    var id = this.state.blId;
    var all_time_val = event.target.value;
    this.setState({all_time: all_time_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ flinvitecustomer: records });
        } else {
          this.setState({ flinvitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleSourceType = event  => {
    var id = this.state.blId;
    var source_type_val = event.target.value;
    this.setState({source_type: source_type_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ flinvitecustomer: records });
          //console.log(this.state.invitecustomer);
        } else {
          this.setState({ flinvitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleChartType = event  => {
    var id = this.state.blId;
    var chart_type_val = event.target.value;
    this.setState({chart_type: chart_type_val});
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
      //start_date: document.getElementById('start_date').value,
      //end_date: document.getElementById('end_date').value,
    }
    getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ flinvitecustomer: records });
        } else {
          this.setState({ flinvitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
  } 

  handleCustomStartDate = event  => {
    var id = this.state.blId;
    var start_date_val = event.target.value;
    this.setState({start_date: start_date_val});
  } 

  handleCustomEndDate = event  => {
    var id = this.state.blId;
    var end_date_val = event.target.value;
    this.setState({end_date: end_date_val});
    var start_date = document.getElementById('start_date').value;
    var end_date = document.getElementById('end_date').value;
    if(start_date > end_date){
      alert('please select end data more than of start date.');
    } else if(start_date=='') {
      alert('please select start date');
    } else {
      const data = {
        all_time: document.getElementById('all_time').value,
        source_type: document.getElementById('source_type').value,
        chart_type: document.getElementById('chart_type').value,
        start_date: document.getElementById('start_date').value,
        end_date: document.getElementById('end_date').value,
        user_id: localStorage.getItem(USER_ID),
      }
      getAllInviteCustomerWithMonth(data)
      .then(res => {
        if(res.status==true){
          var records = res.data;
          this.setState({ flinvitecustomer: records });
        } else {
          this.setState({ flinvitecustomer: '' });
        }
      })
      .catch(err => {
          console.log(err);
      });
    }
  }
  ResetFilters = () => {
    this.setState({ source_type: '' });
    this.setState({ chart_type: '' });
    this.setState({ all_time: '' });
    this.setState({ start_date: '' });
    this.setState({ end_date: '' });
    this.setState({ flinvitecustomer: '' });
    
    const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      start_date: document.getElementById('start_date').value,
      end_date: document.getElementById('end_date').value,
      user_id: localStorage.getItem(USER_ID),
    }
    getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data;
        this.setState({ invitecustomer: records });
        console.log(this.state.invitecustomer);
      } else {
        this.setState({ invitecustomer: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
  }
refreshBlList = () => {
  getBusinessLocationLists()
      .then(res => {
        if(res.status==true){
          this.setState({ businesslocationlists: res.data, blId: res.data[0].id });
          this.refreshSingleBlDeatils(res.data[0].id);
          this.refreshLiveRating(res.data[0].id);
          if(res.data[0].lrvstatus==0) {
            this.refreshmyLiveReviews(res.data[0].id);
            this.handelWrnMsg();
          } else {
            this.refreshTopLiveReviews(res.data[0].id);
          }
          this.refreshAllReviews(res.data[0].id);
          console.log(this.state.businesslocationlists);
        } else {
          this.setState({ businesslocationlists: '', blId: '' });
          /*toast.error(res.message, {
          position: toast.POSITION.BOTTOM_RIGHT
          });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
} 

refreshgetAllInviteCustomerWithMonth = () => {
  const data = {
      all_time: document.getElementById('all_time').value,
      source_type: document.getElementById('source_type').value,
      chart_type: document.getElementById('chart_type').value,
      user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerWithMonth(data)
    .then(res => {
      if(res.status==true){
        var records = res.data
        this.setState({ invitecustomer: records });
      } else {
        this.setState({ invitecustomer: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAllInviteCustomerReviewGivenCount = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
  }
  getAllInviteCustomerReviewGivenCount(data)
    .then(res => {
      if(res.status==true){
        this.setState({ invitecustomercount: res.invite_customer_count });
        this.setState({ reviewgivencount: res.review_given_count });
      } else {
        this.setState({ invitecustomercount: '' });
        this.setState({ reviewgivencount: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}

refreshgetAverageLiveReviewRating = () => {
  const data = {
    user_id: localStorage.getItem(USER_ID),
    //user_role: localStorage.getItem(USER_ROLE),
  }
  getAverageLiveReviewRating(data)
    .then(res => {
      if(res.status==true){
        this.setState({ average_live_review_rating: res.success });
        this.setState({ total_live_review_rating: res.total_review_rating });
      } else {
        this.setState({ average_live_review_rating: '' });
        this.setState({ total_live_review_rating: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });
}
constructor() {
  super();
  this.toggleDataSeries = this.toggleDataSeries.bind(this);
}
toggleDataSeries(e){
  if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  this.chart.render();
}
componentDidMount() {
    this.refreshInviteUserListing();
    this.refreshgetAllInviteCustomerWithMonth();
    this.refreshgetAllInviteCustomerReviewGivenCount();
    this.refreshgetAverageLiveReviewRating();
    this.refreshBlList();
  }

openmodal(invite_custo_id){
    this.setState({invite_customer_id:invite_custo_id});
    alert(this.state.invite_customer_id);
    }

  render(){
    const line_options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2", // "light1", "dark1", "dark2"
      title:{
        text: "Reviews Of Month"
      },
      axisY: {
        title: "Reviews",
        includeZero: false,
        suffix: "%"
      },
      axisX: {
        title: "Review of Month",
        prefix: "M",
        interval: 2
      },
      data: [{
        type: "line",
        toolTipContent: "Month {x}: {y}%",
        dataPoints: 
        [
        //this.state.invitecustomer
        {"x":12,"y":1},
        {"x":17,"y":1},
        {"x":19,"y":1},
        {"x":21,"y":5},
        {"x":25,"y":2}
         /* { x: 1, y: 64 },
          { x: 2, y: 61 },
          { x: 3, y: 64 },
          { x: 4, y: 62 },
          { x: 5, y: 64 },
          { x: 6, y: 60 },
          { x: 7, y: 58 },
          { x: 8, y: 59 },
          { x: 9, y: 53 },
          { x: 10, y: 54 },
          { x: 11, y: 61 },
          { x: 12, y: 60 },
          { x: 13, y: 55 },
          { x: 14, y: 60 },
          { x: 15, y: 56 },
          { x: 16, y: 60 },
          { x: 17, y: 59.5 },
          { x: 18, y: 63 },
          { x: 19, y: 58 },
          { x: 20, y: 54 },
          { x: 21, y: 59 },
          { x: 22, y: 64 },
          { x: 23, y: 59 }*/
        ]
      }]
    }
    const average_live_review_pie_options = {
      animationEnabled: true,
      title: {
        text: "Review By Star Rating"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Average Star Rating", y: this.state.average_live_review_rating, color: "#ffdc04" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    const reviews_source_pie_options = {
      animationEnabled: true,
      title: {
        text: "Total Reviews"
      },
      subtitles: [{
        //text: "71% Positive",
        verticalAlign: "left",
        fontSize: 16,
        dockInsidePlotArea: true,
      }],
      data: [{
        type: "doughnut",
        showInLegend: true,
        //indexLabel: "{name}: {y}, {color}",
        yValueFormatString: "#.###",
        dataPoints: [
          { name: "Total Reviews", y: this.state.total_live_review_rating, color: "#6954bb" },
          /*{ name: "Review Given", y: this.state.reviewgivencount }*/
          /*{ name: "Very Satisfied", y: 40 },
          { name: "Satisfied", y: 17 },
          { name: "Neutral", y: 7 }*/
        ]
      }]
    }
    const currentPage = this.state.currentPage;
    const previousPage = currentPage - 1;
    const NextPage = currentPage + 1;
    const LastPage = this.state.LastPage;
    const pageNumbers = [];
    for (let i = 1; i <= this.state.TotalPages; i++) {
      pageNumbers.push(i);
    }
    let list;
    let flt_data = this.state.flinvitecustomer.length;
    if(flt_data==0){ 
      list = <LineChart data={this.state.invitecustomer} />;
    } else {
      list = <LineChart data={this.state.flinvitecustomer} />;
    }
    return(
      <>
      <Route component={StaffLeftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        {/*<div className="bg-color"></div>*/} 
        <div className="button-part">
          {/*<button className="part-of-btn">All Reviews</button>
          <button className="part-of-btn">New Reviews</button>*/}
          <select name="source_type" className="part-of-btn" id="source_type" value={this.state.source_type} onChange={this.handleSourceType}>
            <option value="all">All Sources</option>
            <option value="Google">Google</option>
            <option value="Facebook">Facebook</option>
          </select>
          <select name="chart_type" className="part-of-btn" id="chart_type" value={this.state.chart_type} onChange={this.handleChartType}>
            {/*<option value="">Chart type</option>*/}
            <option value="daily">Daily</option>
            <option value="weekly">Weekly</option>
            <option value="monthly">Monthly</option>
            <option value="yearly">Yearly</option>
          </select> 
          <select name="all_time" className="part-of-btn" id="all_time" onChange={this.handleAllTime} value={this.state.all_time}>
            {/*<option value="">All Time</option>*/}
            <option value="one_month">1 month</option>
            <option value="three_month">3 months</option>
            <option value="six_month">6 months</option>
            <option value="twelve_month">12 months</option>
            <option value="custom_date">Custom Dates</option>
          </select>
          
          <input type="date" name="start_date" id="start_date" className="part-of-btn" value={this.state.start_date} disabled={this.state.all_time != 'custom_date' ? true : false} onChange={this.handleCustomStartDate}/>
          <input type="date" name="end_date" id="end_date" className="part-of-btn" value={this.state.end_date} disabled={this.state.all_time != 'custom_date' ? true : false} onChange={this.handleCustomEndDate}/>
          <button className="part-of-btn reset_btn" onClick={this.ResetFilters}> Reset</button>
        </div>
        <div className="left-part-design"> 
          <div className="container ">
            <div className="row">
              <div className="col-sm-4 shot-part">
                <h2>Overview</h2>
                <p>All-Time Rating & Reviews</p>
                <div className="average">
                  <p>Average Star Rating</p>
                  <h2>{this.state.average_live_review_rating}</h2>
                  <StarRatingComponent name="rate2" editing={false} starCount={5} value={this.state.average_live_review_rating} />
                </div>
                <div className="average part-aver">
                  <p>Total Reviews</p>
                  <h2>{this.state.total_live_review_rating}</h2> 
                </div>
              </div>
              <div className="col-sm-8 big-part">
                <div className="row">
                  <div className="col-sm-6">
                   <h2>Collected Reviews</h2> 
                  </div>
                  <div className="col-sm-6">
                   <p className="sendreviewinvitebtn"><a href="/sendreviewinvite">Get more reviews</a></p>
                  </div>
                </div>
                 {/*<CanvasJSChart options = {line_options} />*/}
                 {/*<LineChart data={this.state.invitecustomer} />*/}
                 {list}
                {/*<img src={graphImg} alt="graph" className="img-responsive" />*/}
              </div>
            </div>
          </div>
        </div>
        <div className="row chart_section">
          <div className="col-sm-6 left_chart_section graph-img">
            {/*<CanvasJSChart options = {average_live_review_pie_options} />*/}
            <PieChart data={{"Average Star Rating": this.state.average_live_review_rating}} colors={["#ffdc04"]} />
          </div>
          <div className="col-sm-6 right_chart_section graph-img">
            {/*<CanvasJSChart options = {reviews_source_pie_options} />*/}
            <PieChart data={{"Total Reviews": this.state.total_live_review_rating}} colors={["#6954bb"]} />
          </div>
        </div>
        <div className="search-part">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 search-bar">
                <div className="row">
                  <div className="col-sm-2 fa-icon"><i className="fa fa-map-marker" aria-hidden="true"></i></div>
                  <div className="col-sm-10 location">
                    <p><b>Select Business Location</b></p>
                    <select className="part-of-btn business_location_list" id="cars" name="blIdnew" value={this.state.blIdnew} onChange={this.handleChange} disabled="disabled">
                      {
                        this.state.businesslocationlists.length > 0
                        ?  
                          this.state.businesslocationlists.map(bllist => {
                            return <option key={bllist.id} value={bllist.id}>{bllist.business_address}</option>
                          })
                        :
                          <option> Select Business Location</option>
                      } 
                    </select> 
                  </div>
                </div>
              </div>
              <div className="col-sm-4 search-reviews">
                <div className="row">
                  <div className="col-sm-9 location-2">
                    <p><b>Search Reviews</b></p>
                    <input type="text" id="searchreview" name="searchreview" value={this.state.searchreview} onChange={this.handleChange} className="ser-part" />
                    {/*<p style={alertStyle}>{this.state.errors.searchreview}</p>*/}
                  </div>
                  <div className="col-sm-3 fa-icon-2"><i onClick={this.validateSearchReview} style={{cursor:'pointer'}} className="fa fa-search" aria-hidden="true"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="table-part">
          <div className="row button-part">
            <div className="col-sm-5">
              <p className="left_content_section">Showing 1 to {this.state.inviteusercustomer.length} Results <a href={'/sendreviewinvite'} className="part-of-btn add_invite_customer_btn">Invite Customer</a></p>
              
            </div>
            <div className="col-sm-7"> 
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Sources</option> 
              </select>
              <select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Ratings</option> 
              </select>
              <select name="cars" className="part-of-btn" id="cars">
                <option value="">All Time</option>
                <option value="1">1 month</option>
                <option value="3">3 months</option>
                <option value="6">6 months</option>
                <option value="12">12 months</option>
                <option value="custom_date">Custom Dates</option>
              </select>
              <button className="part-of-btn">Reset</button>
              {/*<select name="cars" className="part-of-btn" id="cars">
                <option value="volvo">All Reset</option> 
              </select>*/}
              {/*<a href={'/sendreviewinvite'} className="part-of-btn add_invite_customer_btn">Invite Customer</a>*/}
              {/*<button className="part-of-btn">CSV</button>*/}
              {/*<select name="cars" className="part-of-btn right" id="cars">
                <option value="volvo">CSV</option> 
              </select>*/}
            </div>
          </div>
          <table className="table-responsive">
            <tr>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Invited User</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Email</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Invited Date</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Phone No.</th>
              <th style={{color: "#a5a5a5", fontWeight: "200"}}>Actions</th>
            </tr>
               {
                  this.state.inviteusercustomer.length > 0
                  ?  
                    this.state.inviteusercustomer.map(inviteusercustomerdata => {
                      return <tr>
                              <td>{inviteusercustomerdata.customer_name}</td>
                              <td>{inviteusercustomerdata.customer_Email}</td>
                              <td>
                                <Moment format="MM/DD/YYYY">
                                  {inviteusercustomerdata.created_at}
                                </Moment>
                              </td>
                              <td>{inviteusercustomerdata.ext_1}</td>
                              <td colspan="6"><a href={'/viewinvitecustomer/' + inviteusercustomerdata.id} className="view"> <i className="fa fa-paper-plane" aria-hidden="true"></i> View</a></td>
                          </tr>
                  })
                :
                <tr>
                  <td colSpan="6" style={txtCenter} className="font_12 txt_col fontweight400 "> {this.state.totalLive == '0' ? 'There are currently no Live Reviews.' : ''}</td>
                </tr>
              }
          </table>
          { 
            pageNumbers.length > 1 ?
              <Pagination
              activePage={this.state.activePage}
              totalItemsCount={this.state.total}
              pageRangeDisplayed={5}
              onChange={this.handlePageChange.bind(this)}
              />
            : ''
          }
          <div className="row bot-area">
            <div className="col-sm-6 right-show"><p>Showing 1 to {this.state.inviteusercustomer.length} of Result</p></div>
            <div className="col-sm-6 left-show">  
              <p><a href="/getreviews">View All</a></p>
            </div>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div> 
        <ToastContainer autoClose={5000} />
        <div className={this.state.enableShdoLive ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being downloaded...<br />This may take a few minutes depending on how many reviews you have...</span> </div>  
        <div className={this.state.enableShdoIntAll ? 'rjOverlayShow' : 'rjOverlayHide'} ><img src={LivloadngImg} /><span className="waitcc">Please wait a moment while your reviews are being loading...<br />This may take a few minutes depending on how many reviews you have...</span> </div>   
      </div>
      </> 
    );
  }
}
export default staffdashboard;
