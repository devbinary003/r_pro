import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import StaffLeftsidebar from './StaffLeftsidebar.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';


const AddNewInviteCustomer = (data) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/invitecustomer/addinvitecustomer', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
const getallBusinessLocation = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/invitecustomer/getallbusinesslocation', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class sendreviewinvite extends React.Component {
	state = {
        fields: {},
        errors: {},
        invite_customer_business_location: '',
        invite_customer_name: '',
        invite_customer_email: '',
        invite_customer_phone_number:'',
        business_location: [],
        hd_invite_customer_business_location:'',

        signup_success: false,
        disableBaddr: false,
        disableBemail: false,
        
        disablePurl: false,
        disablePurls: false,
        copied: false,
        Loading: false,
    }
	handleChange = event => {
      	this.setState({
        	[event.target.name]: event.target.value
      	});
  	}
	handleSubmit = event => {
      	event.preventDefault();
  	}
  	validateForm() {
      	let fields = this.state.fields;
      	let errors = {};
      	let formIsValid = true;

	    /*if (!this.state.invite_customer_business_location) {
	        formIsValid = false;
	        errors["invite_customer_business_location"] = "*Please select your business location.";
	    }*/

	    if (!this.state.invite_customer_name) {
	        formIsValid = false;
	        errors["invite_customer_name"] = "*Please enter customer name.";
	    }

	    if (!this.state.invite_customer_email && !this.state.invite_customer_phone_number) {
	        formIsValid = false;
	        errors["invite_customer_email"] = "*Please enter email address.";
	        errors["invite_customer_phone_number"] = "*Please enter Phone number.";
	        if (typeof this.state.invite_customer_email !== "undefined") {
		        if (!this.state.invite_customer_email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
		          formIsValid = false;
		          errors["invite_customer_email"] = "*Please enter a valid email address";
		        }
	    	}
	    } /*else if(!this.state.invite_customer_email && this.state.invite_customer_phone_number!=''){
	    	alert('hello2');
	    	formIsValid = false;
	        errors["invite_customer_email"] = "*Please enter email address.";
	    } else if(!this.state.invite_customer_phone_number && this.state.invite_customer_email!=''){
	    	alert('hello3');
	    	formIsValid = false;
	        errors["invite_customer_phone_number"] = "*Please enter Phone number.";
	    }*/

	    /*if (typeof this.state.invite_customer_email !== "undefined") {
	        if (!this.state.invite_customer_email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
	          formIsValid = false;
	          errors["invite_customer_email"] = "*Please enter a valid email address";
	        }
	    }*/

	    /*if (!this.state.invite_customer_email) {
	        formIsValid = false;
	        errors["invite_customer_email"] = "*Please enter email address.";
	    }

	    if (typeof this.state.invite_customer_email !== "undefined") {
	        if (!this.state.invite_customer_email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
	          formIsValid = false;
	          errors["invite_customer_email"] = "*Please enter a valid email address";
	        }
	    }*/

	   /* if (!this.state.invite_customer_description) {
	        formIsValid = false;
	        errors["invite_customer_description"] = "*Please enter description.";
	    }*/
 
      	this.setState({
        	errors: errors
      	});
      	return formIsValid;
  	}
  	ValidateBasic = event => {
	    if (this.validateForm()==true) {
	      this.setState({ Loading: true }); 
	      this.savedata();
	    }
	}
	savedata = () => {
		const data = {
		    businesslocation_id: document.getElementById("hd_invite_customer_business_location").value,
		    customer_name:this.state.invite_customer_name,
		    customer_email:this.state.invite_customer_email,
		    customer_phone_number: this.state.invite_customer_phone_number,
		    user_id: localStorage.getItem(USER_ID),
		}
	  	AddNewInviteCustomer(data)
      	.then(res => {
          	if (res.status==true) {
              	this.setState({
              		// forwarduId: res.data.user_id,
               		signup_success: true,
              	});
              	toast.success(res.message, {
                	position: toast.POSITION.BOTTOM_RIGHT
              	});
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.setState({
          		invite_customer_name:'',
	    		invite_customer_email:'',
	    		invite_customer_phone_number: '',
            });
          	this.setState({ Loading: false });  
      	})
      	.catch(err => {
          	console.log(err);
      	});
	}
	refreshBusinessLocationListing = () => {
	    const data = {
	        user_id: localStorage.getItem(USER_ID),
	    }
    	getallBusinessLocation(data)
        .then(res => {
          	if(res.status==true){
            	var records = res.data;
            	this.setState({ business_location: records });
          	} else {
            	this.setState({ business_location: '' });
          	}
          	this.setState({ enableShdo: false, });
        })
        .catch(err => {
            console.log(err);
        });
  	}  
  	diasbleCopyMsg() {
    	this.setState({ copied: true });
    	this.change = setTimeout(() => {
      		this.setState({copied: false})
    	}, 5000)
  	}
  	componentDidMount() {
  		this.refreshBusinessLocationListing();
  	}
  	render(){
  		const {Loading} = this.state;
  		return (
  			<>
  			{
  				localStorage.getItem(USER_ROLE)=='2'
  				?
    				<Route component={Leftsidebar} />
    			:
    				<Route component={StaffLeftsidebar} />
    		}
  			<div className="left-bar">
  				<Route component={Header} />
 				{/*<div class="bg-color"></div>*/}	
 				<div className="get-reviews-part form-part">
 					<div className="row">
   						<div className="col-sm-4">
	   						<button className="send-review-2">
	   							<i className="fa fa-pencil-square-o" aria-hidden="true"></i>
								<i className="fa fa-eye" aria-hidden="true"></i><br/>
								<h5>Initial Review Email</h5>
		   						<p>Email to request customer to write a business review.</p>
		   						<span className="timeframe">Timeframe</span>
		   						<b>Now</b>
	   						</button>
	   						<div className="icon icon2 boxes_icon_desktop">
								<div className="arrow"></div>
	  						</div>
	  						<div className="icon icon2 boxes_icon_mobile">
	  							<i class="fa fa-arrow-down" aria-hidden="true"></i>
	  						</div>
	   					</div>
	   					<div className="col-sm-4">
	   						<button className="send-review-2">
	     						<i className="fa fa-pencil-square-o" aria-hidden="true"></i>
	   							<i className="fa fa-eye" aria-hidden="true"></i><br/>
	   							<h5>Email Reminder</h5>
		   						<p>Reminder email to customer to write a review.</p>
		   						<span className="timeframe">Timeframe</span>
		    					<b>24 hours</b>
	   						</button>
	   						<div className="icon icon2 boxes_icon_desktop">
								<div className="arrow"></div>
	  						</div>
	  						<div className="icon icon2 boxes_icon_mobile">
	  							<i class="fa fa-arrow-down" aria-hidden="true"></i>
	  						</div>
	   					</div>
	   					<div className="col-sm-4">
	    					<button className="send-review-2">
	     						<i className="fa fa-pencil-square-o" aria-hidden="true"></i>
	   							<i className="fa fa-eye" aria-hidden="true"></i><br/>
	   							<h5>SMS Reminder</h5>
		   						<p>A final SMS reminder to your customer for a review.</p>
		   						<span className="timeframe">Timeframe</span>
		    					<b>7 Days</b>
	   						</button>
	   					</div>
   					</div><br/>
					<h1>Send Review Invite</h1>
					<p>Complete the form below and we'll instantly send an email to your customer asking for a business review.</p>
					<form className="needs--form-send-review" >                  
		              	<div className="row">
		                	<div className="col-md-12">
		                  		<div className="form-group">
		                    		<select className="form-control form-des" id="invite_customer_business_location" value={this.state.invite_customer_business_location} onChange={this.handleChange.bind(this)} name="invite_customer_business_location" disabled>
		                      			{
		                        			this.state.business_location.length > 0
		                      				?  
		                        				this.state.business_location.map(business_location_data => {

		                          					return(
		                          						<>
		                            					<option value={business_location_data.id}>{business_location_data.find_business_location}</option>
		                          						<input type="hidden" name="hd_invite_customer_business_location" id="hd_invite_customer_business_location" value={business_location_data.id}/>
		                          						</>
		                          					)
		                        				})
		                      				:
		                        				<option>No Business Location Found</option>
		                      			}
		                    		</select>
		                  		</div>
		                	</div>
		              	</div>
		              	<div className="row">
		                	<div className="col-md-12">
		                  		<div className="form-group">
		                    		<input type="text" className="form-control input_custom_style form-des" id="invite_customer_name" name="invite_customer_name" placeholder="Enter Name" value={this.state.invite_customer_name} onChange={this.handleChange} />
		                    		<span style={alertStyle}>{this.state.errors.invite_customer_name}</span>
		                  		</div>
		                	</div>
		              	</div>
		              	<div className="row">
		                	<div className="col-sm-12">
		                  		<div className="form-group">
		                    		<input type="text" className="form-control input_custom_style form-des" id="invite_customer_email" name="invite_customer_email" placeholder="Enter Email" value={this.state.invite_customer_email} onChange={this.handleChange} />
		                    		<span style={alertStyle}>{this.state.errors.invite_customer_email}</span>
		                  		</div>
		                	</div>
		              	</div>
		              	<div className="row">
		                	<div className="col-sm-12">
			                  	<div className="form-group">
			                  		<PhoneInput
					                    country="US"
					                    showCountrySelect={ false }
					                    name="invite_customer_phone_number"
					                    limitMaxLength={ true }
					                    placeholder="Enter Phone Number with Country Code"
					                    className="invite_customer_phone_input input_custom_style form-des"
					                    id="invite_customer_phone_number"
					                    value={ this.state.invite_customer_phone_number }
					                    onChange={ invite_customer_phone_number => this.setState({ invite_customer_phone_number }) } 
                    				/>
                    				<span style={alertStyle}>{this.state.errors.invite_customer_phone_number}</span>
			                  	</div>
		                	</div>
		              	</div>
			            <div className="text-left">
			                <button type="button" className="send-review" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Send</button>
			            </div>
            		</form>
	 				{/*<input type="text" className="form-des" placeholder="Customer Name" />
	 				<input type="text" className="form-des" placeholder="Email Address" />   
	 				<input type="text" className="form-des" placeholder="Mobile Number" />
	 				<button className="send-review">Send</button>*/}
 				</div>
 				<div>
			        <MessengerCustomerChat
			          pageId="415979359011219"
			          appId="448749972524859"
			        />
      			</div>
      			<ToastContainer autoClose={5000} />
 			</div>
 			</>
  		);
  	}
}
export default sendreviewinvite;
