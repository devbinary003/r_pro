import React, { Component } from "react";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL, IS_ACTIVE, PROFILE_URL } from '../../constants';

import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';
import Leftsidebar from './Leftsidebar.js';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import 'react-phone-number-input/style.css';
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber, formatPhoneNumberIntl } from 'react-phone-number-input';

import Parser from 'html-react-parser';
import $ from 'jquery';
import MessengerCustomerChat from 'react-messenger-customer-chat';

import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';

const getStaffDeatils = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/staff/getbusinessstaffsinglerecord/'+id, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const UpdateStaff = (data, id) => {
    return new Promise((resolve, reject) => {
       const req = scAxios.request('/staff/updatestaff/'+id, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            data: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
 
   });
}
const alertStyle = {
  color: 'red',
};

const textStyle = {
  textTransform: 'capitalize',
};


class editstaff extends React.Component {
  state = {
        fields: {},
        errors: {},
        StaffId: '',
        edit_first_name: '',
        edit_last_name: '',
        edit_phone: '',
        edit_email: '',
        edit_password: '',
        edit_profile_pic:'',
        /*customer_first_name:'',
        customer_last_name:'',
        customer_email:'',
        customer_phone:'',
        customer_password:'',*/

        signup_success: false,
        disableBaddr: false,
        disableBemail: false,
        
        disablePurl: false,
        disablePurls: false,
        copied: false,
        Loading: false,
    }

handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }

handleSubmit = event => {
      event.preventDefault();
  }

  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.edit_first_name) {
        formIsValid = false;
        errors["edit_first_name"] = "*Please enter first name.";
      }

      if (!this.state.edit_last_name) {
        formIsValid = false;
        errors["edit_last_name"] = "*Please enter last name.";
      }

      /*if (!this.state.edit_password) {
        formIsValid = false;
        errors["edit_password"] = "*Please enter password.";
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }*/

      if (!this.state.edit_contact_no) {
        formIsValid = false;
        errors["edit_contact_no"] = "*Please enter phone number.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.setState({ Loading: true }); 
      this.savedata();
    }
    //alert('hello');
}

savedata = () => {
  var id = this.state.StaffId;
  const data = {
      edit_first_name: this.state.edit_first_name,
      edit_last_name:this.state.edit_last_name,
      edit_email:this.state.edit_email,
      user_id: localStorage.getItem(USER_ID),
      edit_contact_no: this.state.edit_contact_no,
      edit_password: this.state.edit_password,
  }
  UpdateStaff(data, id)
      .then(res => {
          if (res.status==true) {
              this.setState({
              // forwarduId: res.data.user_id,
               signup_success: true,
              });
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }

  refreshstaffDeatils = (id) => {

    getStaffDeatils(id)
      .then(res => {
        if(res.status==true){
              this.setState({
                StaffId: res.data.id,
                edit_first_name: res.data.firstname,
                edit_last_name: res.data.lastname,
                edit_email: res.data.email,
                edit_contact_no: res.data.phone,
                edit_password: res.data.hdpwd,
              });
        } else {
           /*toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });*/
        }
      })
      .catch(err => {
          console.log(err);
      });
    }  
    
  diasbleCopyMsg() {
    this.setState({ copied: true });
    this.change = setTimeout(() => {
      this.setState({copied: false})
    }, 5000)
  }
    
  componentDidMount() {
    //this.refreshUserDeatils();
    var id = this.props.match.params.id;
    this.refreshstaffDeatils(id);
  }

render(){

const {Loading} = this.state;

  
return (
      <>
      <Route component={Leftsidebar} />
      <div className="left-bar">
        <Route component={Header} />
        <div className="row">
          <div className="col-md-12 form_section">
            <form className="needs-validation step_form mb-4" >
              <h1 className="black_bk_col fontweight500 font_20 mb-4 text-center">Edit Staff</h1>                  
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="edit_first_name" name="edit_first_name" placeholder="Enter First Name" value={this.state.edit_first_name} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.edit_first_name}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input type="text" className="form-control input_custom_style" id="edit_last_name" name="edit_last_name" placeholder="Enter Last Name" value={this.state.edit_last_name} onChange={this.handleChange} />
                    <span style={alertStyle}>{this.state.errors.edit_last_name}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <input disabled={!this.state.disableBaddr} type="text" className="form-control input_custom_style" id="edit_email" name="edit_email" placeholder="Enter Email" value={this.state.edit_email} onChange={this.handleChange} />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <PhoneInput
                    country="US"
                    showCountrySelect={ false }
                    name="edit_contact_no"
                    limitMaxLength="12"
                    placeholder="Enter phone number"
                    className="businessPhone"
                    value={ this.state.edit_contact_no }
                    onChange={ edit_contact_no => this.setState({ edit_contact_no }) } 
                    />
                    {/*<input type="text" className="form-control input_custom_style" id="edit_phone" name="edit_phone" placeholder="Phone number" value={this.state.edit_phone} onChange={this.handleChange}  />*/}
                    <span style={alertStyle}>{this.state.errors.edit_contact_no}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                    <input disabled={!this.state.disableBaddr} type="password" className="form-control input_custom_style" id="edit_password" name="password" placeholder="Enter password" value={this.state.edit_password} onChange={this.handleChange} />
                  </div>
                </div>
              </div>
              <div className="text-center">
                <button type="button" className="blue_btn_box mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Update Staff</button>
              </div>
            </form>
          </div>
        </div>
        <div>
          <MessengerCustomerChat
            pageId="415979359011219"
            appId="448749972524859"
          />
        </div>
        <ToastContainer autoClose={5000} />
      </div>
    </>
  );
 }
}
export default editstaff;
