import React from 'react';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { scAxios } from '../..';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import logoWhite from '../../images/Logo-01.png';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class StaffLeftsidebar extends React.Component {

  state = {
        isprofilecomplete: '',
    }
  
  handleClick() {
    toast.error("Welcome to Review Pro Solutions. Please first provide us some quick information about your business", {
              position: toast.POSITION.BOTTOM_RIGHT
            });
  }

  componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status===true){
              var userdata = res.data;
              this.setState({
                isprofilecomplete: userdata.isprofilecomplete,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

    render() {
      let url;
      if(window.location.hostname==="localhost"){
        url = 'http://'+window.location.hostname+':3000';
      } else {
        url = 'https://'+window.location.hostname;
      }
      return (
        <>
        <div className="right-bar">
          <div className="right-bar-2">
            <p><a href="/"><img src={logoWhite} alt="logo" className="img-responsive" style={{width: "100%"}}/></a></p>
            <ul className="das-link dropdown_desktop">
                <a href="/staffdashboard"><li className={window.location.href=== url+'/staffdashboard'? 'active':''}> Dashboard </li></a>
                <a href="/sendreviewinvite"><li className={window.location.href=== url+'/sendreviewinvite'? 'active':''}> Send Review Invite </li></a>
                <a href="/getreviews"><li className={window.location.href=== url+'/getreviews'? 'active':''}> Get Reviews </li></a>
                <a href="/contact" target="_blank"><li> Get Help </li></a>  
            </ul>
            <div className="dropdown dropdown_mobile">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-bars" aria-hidden="true"></i> Menu
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a className={window.location.href=== url+'/staffdashboard'? 'dropdown-item active':'dropdown-item'} href="/staffdashboard"><li> Dashboard </li></a>
                  <a className={window.location.href=== url+'/sendreviewinvite'? 'dropdown-item active':'dropdown-item'} href="/sendreviewinvite"><li> Send Review Invite </li></a>
                  <a className={window.location.href=== url+'/getreviews'? 'dropdown-item active':'dropdown-item'} href="/getreviews"><li> Get Reviews </li></a>
                  <a className="dropdown-item" href="/contact" target="_blank"><li> Get Help </li></a>
                </div>
            </div>
            {/*{ this.state.isprofilecomplete == 1 
              ?
                <ul className="das-link">
                  <li className="active"><a href="/staffdashboard"> Dashboard </a></li>
                  <li><a href="/getreviews"> Get Reviews </a></li>
                  <li><a href="/#" data-toggle="modal" data-target="#gethelpModal"> Get Help </a></li>  
                </ul>
              :
                <ul className="das-link">
                  <li className="active"><a href="#"> Dashboard </a></li>
                  <li><a href="#"> Get Reviews </a></li>
                  <li><a href="#" data-toggle="modal" data-target="#gethelpModal"> Get Help </a></li>  
                </ul>
            }*/}
          </div>
        </div>
        </>
        );
    }
}

export default StaffLeftsidebar;
