import { TWENTY_MINUTES_TIME_IN_MILLISECONDS, API_TOKEN_NAME, API_TOKEN_EXPIRY_NAME, USER_ROLE, USER_ID, USER_CREATED_BY, USER_EMAIL, IS_ACTIVE, RGSTEP1, RGSTEP2, RGSTEP3, RGSTEP4 } from "../constants";
import { scAxios } from "..";

let userTokenRefreshIntervalId = '';

const userTokenRefreshApi = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/refresh', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

export const userTokenRefreshInterval = () => {
    userTokenRefreshApi()
        .then(res => {
            startUserSession(res.access_token);
        })
        .catch(err => {
            console.log('Error refreshing user token.', err);
            alert('Error refreshing user token.', err);
        })
}

export const startUserSession = (token, user_role, user_id, user_email, isActive, reg_step_1, reg_step_2, reg_step_3, reg_step_4) => {
    localStorage.setItem(API_TOKEN_NAME, token);
    localStorage.setItem(USER_ROLE, user_role);
    localStorage.setItem(USER_ID, user_id);
    localStorage.setItem(USER_EMAIL, user_email);
    localStorage.setItem(IS_ACTIVE, isActive);

    localStorage.setItem(RGSTEP1, reg_step_1);
    localStorage.setItem(RGSTEP2, reg_step_2);
    localStorage.setItem(RGSTEP3, reg_step_3);
    localStorage.setItem(RGSTEP4, reg_step_4);

    let datetime = new Date();
    datetime.setMinutes(datetime.getMinutes() + 999);
    localStorage.setItem(API_TOKEN_EXPIRY_NAME, datetime);

    clearInterval(userTokenRefreshIntervalId);
    userTokenRefreshIntervalId = setInterval(userTokenRefreshInterval, TWENTY_MINUTES_TIME_IN_MILLISECONDS);
}

export const endUserSession = () => {
    clearInterval(userTokenRefreshIntervalId);
    localStorage.clear();
}