import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as userActions from '../actions/userActions';
import PrivateRoute from './PrivateRoute';
import { LOGIN_PAGE_PATH } from '../constants';
import Dashboard from './default/Dashboard';
import home from './default/home';
import register from './default/register'; 
import Login from './default/Login';
import forgetpassword from './default/forgetpassword';
import ResetPassword from './default/ResetPassword';
import Logout from './default/Logout';
import terms from './default/terms';
import contact from './default/contact';
import pricingtable from './default/pricingtable';
import privacypolicy from './default/privacypolicy';

import BusinessReviewLinkHome from './default/SubmitReviewhome';
import BusinessReviewLinkpage from './default/SubmitReviewpostreview';
import './App.css'; 

class App extends Component {
  render() { 
    return (
     <Router> 
        <Switch>
          <Route exact path="/" component={home} />
          <Route path='/terms' component={terms} exact/>
          <Route path='/privacypolicy' component={privacypolicy} />
          <Route path='/pricingtable' component={pricingtable} />
          <Route path='/contact' component={contact} />
          <Route path='/logout' component={Logout} />
          <Route path='/resetpassword' component={ResetPassword} />
          <Route path='/forgetpassword' component={forgetpassword} />
          <Route path={LOGIN_PAGE_PATH} component={Login} />
          <Route path='/signup' component={register} />
          <Route path='/home' component={home} />
          {/*<Route exact path='/businessreview' component={BusinessReviewLinkHome} />*/}
          <Route exact path='/br/:businessname' component={BusinessReviewLinkpage} />
          <PrivateRoute path='/' component={Dashboard} user={this.props.user} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(userActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);