import React from 'react';

class Footer extends React.Component {

    render() {

        return (  
        <>  
        <div className="footer">
         <div className="container">
            <div className="down">
               <div className="row">
                  <div className="col-md-4">
                     <h4>
                        About
                     </h4>
                     <p>Review Pro Solutions makes it easy to collect, display and manage reviews via Google, Facebook and Yelp.
                     </p>
                  </div>
                  {/*<div className="col-md-3">
                     <h4>Company</h4>
                     <ul>
                        <li>Display Reviews</li>
                        <li>Collect Reviews</li>
                        <li>Request Reviews</li>
                        <li>Plan & Pricing</li>
                        <li>Agencies</li>
                        <li>Blog</li>
                     </ul>
                  </div>*/}
                  <div className="col-md-4">
                     <h4>Pages</h4>
                     <ul>
                        <li><a href="/pricingtable">Pricing</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/privacypolicy">Privacy Policy</a></li>
                        <li><a href="/terms">Terms of Service</a></li>
                     </ul>
                  </div>
                  <div className="col-md-3">
                     <div className="social-media ">
                        <h4 >Follow us</h4>
                        <span><a href="https://www.youtube.com/channel/UCjAGJ8a5JUVEunIqggO6LpQ" target="_blank" rel="noopener noreferrer"><i className="fa fa-youtube-play" aria-hidden="true"></i> </a></span>
                        <span><a href="https://www.facebook.com/reviewpro.solutions/" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook-official" aria-hidden="true"></i> </a></span>
                        <span><a href="https://twitter.com/ReviewProSolut1" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter" aria-hidden="true"></i> </a></span>
                        <span><a href="https://www.instagram.com/reviewprosolutions/" target="_blank" rel="noopener noreferrer"><i className="fa fa-instagram" aria-hidden="true"></i> </a></span>
                     </div>
                     <p className="address_text">2931 Adams Street Hollywood 33020</p>
                     <p className="email_text">email: info@tryreviewpro.com</p>
                     <p className="phone_number_text">toll-free number is 866.776.0259</p>
                  </div>
               </div>
               
            </div>
         </div>

      </div>
      <div className="row sub-footer">
         <p>Copyright 2020 All Rights Reserved.</p>
      </div>
      <div id="cookieConsent">
         <div id="closeCookieConsent">x</div>
         We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.<a className="cookieConsentOK" href="#action">OK</a><a className="cookieConsentOK1" href="#action">Reject</a> 
      </div>
        {/*<nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
        <div className="container-fluid">
        <a className="navbar-brand" href="/"><img src={logoWhite} /></a>
        <div className="collapse navbar-collapse" id="navb">
          <ul className="navbar-nav mr-auto">
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="/">Copyright © 2020 - ReviewProSolutions.com</a>
            </li>
          </ul>
        </div>
      </div>
      </nav>
      <div id="myModal" className="modal fade" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              <iframe width="468" height="315" src="https://www.youtube.com/embed/y7rIhwRaiI8" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
            <button type="button" className="close_style" data-dismiss="modal">&times;</button>
          </div>
        </div>
      </div>*/}
      </>
        );
    }
}

export default Footer;
