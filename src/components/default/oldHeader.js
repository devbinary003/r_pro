import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, IMAGE_URL } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import logoWhite from '../../images/Logo-01.png';

class Header extends React.Component {

  state = {
    
  }

  componentWillMount() {

  }

    render() {

      return (
        <>
        <nav className="navbar navbar-expand-lg navbar-dark bg_blue">
        <div className="container-fluid"> 
        <a className="navbar-brand" href="/"><img src={logoWhite} /></a>
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navb">
          <ul className="navbar-nav mr-auto">

          </ul>
          <ul className="navbar-nav">
          { /* <li className="nav-item nav_li">
                        <a className="nav-link nav_link_color" href="javascript:void(0)"><i className="fa fa-facebook" aria-hidden="true"></i></a>
                      </li>
                      <li className="nav-item nav_li">
                        <a className="nav-link nav_link_color" href="javascript:void(0)"><i className="fa fa-twitter" aria-hidden="true"></i></a>
                      </li>*/}
          </ul>
        </div>
      </div>
      </nav> 
        </>
        );
    }
}

export default Header;
