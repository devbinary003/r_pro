import React from 'react';
import { USER_ROLE, USER_ID } from '../../constants';
import 'react-toastify/dist/ReactToastify.css';
import logoWhite from '../../images/Logo-01.png';


class Header extends React.Component {

  state = {
    
  }

  componentWillMount() {

  }

    render() {

      return (

        
        <>
          <nav className="navbar sticky-top navbar-expand-md" id="navbar">
            <a className="navbar-brand" href="/"><img src={logoWhite} className="logo" alt="logo"/></a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span className="navbar-toggler-icon"><i className="fa fa-bars" aria-hidden="true"></i></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavbar">
              <ul className="navbar-nav">
                 <li className="nav-item">
                    <a className="nav-link" href="/">Home</a>
                 </li>
                 {/*<li className="nav-item">
                    <a className="nav-link" href="#">Feature Request</a>
                 </li>*/}
                 <li className="nav-item">
                    <a className="nav-link" href="/pricingtable">Pricing</a>
                 </li>
                 <li className="nav-item">
                    <a className="nav-link" href="/contact">Contact Us</a>
                 </li>
                  { 
                    localStorage.getItem(USER_ID) == null
                    ?
                      <>
                        <li className="nav-item">
                            <a className="nav-link login" href="/login">LOGIN</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link sign up" href="/signup">SIGN UP</a>
                        </li>
                      </> 
                    :
                      localStorage.getItem(USER_ROLE)==='2'
                      ?
                        <> 
                          <li className="nav-item">
                              <a className="nav-link header_dashboard_btn" href="/userdashboard">Dashboard</a>
                          </li>
                        </>
                      :
                        <>
                          <li className="nav-item">
                              <a className="nav-link header_dashboard_btn" href="/staffdashboard">Dashboard</a>
                          </li>
                        </>
                  }
              </ul>
            </div>
          </nav>
        </>
        );
    }
}

export default Header;
