import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL } from '../../constants';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import logoWhite from '../../images/rs_logo_footer.png';
import starLogo from '../../images/rs_logo_header.jpg';
import '../../css/businessreviewcss.css';
const getUserDetail = (businessname) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/getbusinessdetails/'+businessname, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Header extends React.Component {

  state = {
        business_user_id: '',
        business_place_id: '',
        business_address: '',
        business_name: '',
        facebook_link: '',
        twitter_link: '',
        linkedin_link: '',
        instagram_link: ''
  }

componentDidMount() {
  const businessname = this.props.match.params.businessname;
  getUserDetail(businessname)
        .then(res => {
          if(res.status==true){
            var userdetail = res.data;
            this.setState({
              business_user_id: res.user.id,
              business_address: res.data.business_address,
              business_name: res.data.business_name,
              business_place_id: res.data.business_page_id,
              facebook_link: res.data.facebook_link,
              twitter_link: res.data.twitter_link,
              linkedin_link: res.data.linkedin_link,
              instagram_link: res.data.instagram_link,
            });
          } 
        })
        .catch(err => {
            console.log(err);
        });
}

    render() {

      let face;
      let twitt;
      let linkdin;
      let insta;

      if(this.state.facebook_link!=''){
      face = <li className="nav-item nav_li"><a className="nav-link nav_link_color" href={this.state.facebook_link} target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
      }
      if(this.state.twitter_link!=''){
      twitt = <li className="nav-item nav_li"><a className="nav-link nav_link_color" href={this.state.twitter_link} target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
      }
      if(this.state.linkedin_link!=''){
      linkdin = <li className="nav-item nav_li"><a className="nav-link nav_link_color" href={this.state.linkedin_link} target="_blank"><i className="fa fa-linkedin" aria-hidden="true"></i></a></li>
      }
      if(this.state.instagram_link!=''){
      insta = <li className="nav-item nav_li"><a className="nav-link nav_link_color" href={this.state.instagram_link} target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
      }

      return (
        <>
        <nav className="navbar navbar-expand-lg navbar-dark bg_blue">
        <div className="container-fluid"> 
        
          <a className="navbar-brand left_header_section" href="javascript:void(0)"><div className="logo_mobile_img_sec"><img src={starLogo} className="logo_img"/></div><span className="full_location"><span className="location_name">{this.state.business_name}</span><span  className="location_adress">{this.state.business_address}</span></span></a>
        
        {/*<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
                  <span className="navbar-toggler-icon"></span>
                </button>*/}

        <div className="collapse navbar-collapse header_social_icon_sec" id="navb">
          <ul className="navbar-nav mr-auto">

          </ul>
          <ul className="navbar-nav social_icon">
            {face}
            {twitt}
            {linkdin}
            {insta}
          </ul>
        </div>
      </div>
      </nav> 
        </>
        );
    }
}

export default Header;
