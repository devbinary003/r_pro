import React from 'react';
import {Route} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

class privacypolicy extends React.Component {

  state = {
        fields: {},
        errors: {},
  };

    render(){
      
      return (

      <div>
      
      <Route component={Header} />
        <div className="row heading">
            <h2>Review Pro Solution Data Protection Policy</h2>
         </div>
         <div className=" privacy">           
            <div className="row">
               <h2>Context and overview</h2>
            </div>
            <div className="row">
                <h2>Introduction</h2>
            </div>
            <div className="row">
               <p>Review Pro Solutions needs to gather and use certain information about individuals.these can include customers, suppliers, business contacts, employees and other people the organisation has a relationship with or may need to contact.This policy describes how this personal data must be collected, handled and stored to meet the company’s data protection standards — and to comply with the law.</p>
            </div>
            <div className="row">
               <h3>Why this policy exists</h3>
            </div>
            <div className="row">
              <ul className="privacy-list">
                <h5>This data protection policy ensures ZMB Property Investment:</h5>
                <li>
                  Complies with data protection law and follow good practice
                </li>
                <li>
                  Protects the rights of staff, customers and partners
                </li>
                <li>
                  Is open about how it stores and processes individuals’ data
                </li>
                <li>
                  Protects itself from the risks of a data breach
                </li>
              </ul>
            </div>
              <div className="row">
                <ul className="privacy-list">
                  <h3>Data protection law</h3>
                  <p>These rules apply regardless of whether data is stored electronically, on paper or on other materials.<br />To comply with the law, personal information must be collected and used fairly, stored safely and not disclosed unlawfully.<br />The Data Protection Act is underpinned by eight important principles. These say that personal data must:</p>
                  <li>
                    Be processed fairly and lawfully
                  </li>
                  <li>
                    Be obtained only for specific, lawful purposes
                  </li>
                  <li>
                    Be adequate, relevant and not excessive
                  </li>
                  <li>
                    Not be held for any longer than necessary
                  </li>
                  <li>
                    Processed in accordance with the rights of data subjects
                  </li>
                  <li>
                    Be protected in appropriate ways
                  </li>
                </ul>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">People, risks and responsibilities</h1>
                  <h2 className="heading-title">Policy scope</h2>
                  <p className="policy-text">This policy applies to:</p>
                  <ul>
                    <li>The head office of ZMB Property Investment</li>
                    <li>All branches of ZMB Property Investment</li>
                    <li>All staff and volunteers of ZMB Property Investment</li>
                    <li>All contractors, suppliers and other people working on behalf of ZMB Property Investment</li>
                  </ul>
                  <p className="policy-text">It applies to all data that the company holds relating to identifiable individuals, even if that information technically falls outside of the Data Protection Act 1998. This can include:</p>
                  <ul>
                    <li>Names of individuals</li>
                      <li>Postal addresses</li>
                      <li>Email addresses</li>
                      <li>Telephone numbers</li>
                      <li>…plus any other information relating to individuals</li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Data protection risks</h1>
                  <p className="policy-text">This policy helps to protect ZMB Property Investment from some very real data security risks, including:</p>
                  <ul>
                    <li><strong>Breaches of confidentiality.</strong> For instance, information being given out inappropriately.</li>
                    <li><strong>Failing to offer choice.</strong> For instance, all individuals should be free to choose how the company uses data relating to them.</li>
                    <li><strong>Reputational damage.</strong> For instance, the company could suffer if hackers successfully gained access to sensitive data.</li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Responsibilities</h1>
                  <p className="policy-text">Everyone who works for or with ZMB Property Investment has some responsibility for ensuring data is collected, stored and handled appropriately.</p>
                  <p className="policy-text">Each team that handles personal data must ensure that it is handled and processed in line with this policy and data protection principles.</p>
                  <p className="policy--text">However, these people have key areas of responsibility:</p>
                  <ul>
                    <li>The<strong> board of directors</strong> is ultimately responsible for ensuring that ZMB Property Investment meets its legal obligations.</li>
                    <li>The<strong> Managing Director,</strong> is responsible for:
                      <ul>
                        <li>Keeping the board updated about data protection responsibilities, risks and issues.</li>
                        <li>Reviewing all data protection procedures and related policies, in line with an agreed schedule.</li>
                        <li>Arranging data protection ZMB Property Investment and advice for the people covered by this policy.</li>
                        <li>Handling data protection questions from staff and anyone else covered by this policy.</li>
                        <li>Dealing with requests from individuals to see the data ZMB Property Investment holds about them (also called ‘subject access requests’).</li>
                        <li>Checking and approving any contracts or agreements with third parties that may handle the company’s sensitive data.</li>
                      </ul>
                    </li>
                    <li>The <strong>Managing Director,</strong> is responsible for:
                      <ul>
                        <li>Ensuring all systems, services and equipment used for storing data meet acceptable security standards.</li>
                        <li>Performing regular checks and scans to ensure security hardware and software is functioning properly.</li>
                        <li>Evaluating any third-party services the company is considering using to store or process data. For instance, cloud computing services.</li>
                      </ul>
                    </li>
                    <li>The <strong>Managing Director,</strong> is responsible for:
                      <ul>
                        <li>Approving any data protection statements attached to communications such as emails and letters.</li>
                        <li>Addressing any data protection queries from journalists or media outlets like newspapers.</li>
                        <li>Where necessary, working with other staff to ensure marketing initiatives abide by data protection principles.</li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">General staff guidelines</h1>
                  <ul>
                    <li>The only people able to access data covered by this policy should be those who <strong>need it for their work.</strong></li>
                    <li>Data<strong> should not be shared informally.</strong> When access to confidential information is required, employees can request it from their line managers.</li>
                    <li><strong>ZMB Property Investment will provide ZMB Property Investment</strong> to all employees to help them understand their responsibilities when handling data.</li>
                    <li>Employees should keep all data secure, by taking sensible precautions and following the guidelines below.</li>
                    <li>In particular, <strong>strong passwords must be used</strong> and they should never be shared.</li>
                    <li>Personal data <strong>should not be disclosed </strong>to unauthorised people, either within the company or externally.</li>
                    <li>Data should be <strong>regularly reviewed and updated</strong> if it is found to be out of date. If no longer required, it should be deleted and disposed of.</li>
                    <li>Employees <strong>should request help</strong> from their line manager or the data protection officer if they are unsure about any aspect of data protection.</li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Data storage</h1>
                  <p>These rules describe how and where data should be safely stored. Questions about storing data safely can be directed to the IT manager or data controller.</p>
                  <p>When data is <strong>stored on paper,</strong> it should be kept in a secure place where unauthorised people cannot see it.</p>
                  <p>These guidelines also apply to data that is usually stored electronically but has been printed out for some reason:</p>
                  <ul>
                    <li>When not required, the paper or files should be kept in a<strong> locked drawer or filing cabinet.</strong></li>
                    <li>Employees should make sure paper and printouts are <strong>not left where unauthorised people could see them,</strong> like on a printer.</li>
                    <li><strong>Data printouts should be shredded</strong> and disposed of securely when no longer required.</li>
                  </ul>
                  <p>When data is stored electronically, it must be protected from unauthorised access, accidental deletion and malicious hacking attempts:</p>
                  <ul>
                    <li>Data should be<strong> protected by strong passwords</strong> that are changed regularly and never shared between employees.</li>
                    <li>If data is <strong>stored on removable media</strong> (like a CD or DVD), these should be kept locked away securely when not being used.</li>
                    <li>Data should only be stored on<strong> designated drives and servers,</strong> and should only be uploaded to an<strong> approved cloud computing services.</strong></li>
                    <li>Servers containing personal data should be <strong>sited in a secure location,</strong> away from general office space.</li>
                    <li>Data should be<strong> backed up frequently.</strong> Those backups should be tested regularly, in line with the company’s standard backup procedures.</li>
                    <li>Data should <strong>never be saved directly</strong> to laptops or other mobile devices like tablets or smart phones.</li>
                    <li>All servers and computers containing data should be protected by<strong> approved security software and a firewall.</strong></li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Data use</h1>
                  <p>Personal data is of no value to ZMB Property Investment unless the business can make use of it. However, it is when personal data is accessed and used that it can be at the greatest risk of loss, corruption or theft:</p>
                  <ul>
                    <li>When working with personal data, employees should ensure <strong>the screens of their computers are always locked </strong>when left unattended.</li>
                    <li>Personal data <strong>should not be shared informally. </strong>In particular, it should never be sent by email, as this form of communication is not secure.</li>
                    <li>Data must be <strong>encrypted before being transferred electronically.</strong> The IT manager can explain how to send data to authorised external contacts.</li>
                    <li>Personal data should <strong>never be transferred outside of the European Economic Area.</strong></li>
                    <li>Employees <strong>should not save copies of personal data to their own computers. </strong>Always access and update the central copy of any data.</li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Data accuracy</h1>
                  <p>The law requires ZMB Property Investment to take reasonable steps to ensure data is kept accurate and up to date.</p>
                  <p>The more important it is that the personal data is accurate, the greater the effort ZMB Property Investment should put into ensuring its accuracy.</p>
                  <p>It is the responsibility of all employees who work with data to take reasonable steps to ensure it is kept as accurate and up to date as possible.</p>
                  <ul>
                    <li>Data will be held in <strong>as few places as necessary. </strong>Staff should not create any unnecessary additional data sets.</li>
                    <li>Staff should<strong> take every opportunity to ensure data is updated. </strong>For instance, by confirming a customer’s details when they call.</li>
                    <li>Floatopia Ibiza will make it <strong>easy for data subjects to update the information</strong> ZMB Property Investment holds about them. For instance, via the company website.</li>
                    <li>Data should be <strong>updated as inaccuracies are discovered.</strong> For instance, if a customer can no longer be reached on their stored telephone number, it should be removed from the database.</li>
                    <li>It is the <strong>marketing manager’s responsibility to ensure marketing databases are checked against industry suppression files</strong> every six months.</li>
                  </ul>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_13 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Subject access requests</h1>
                  <p>All individuals who are the subject of personal data held by ZMB Property Investment are entitled to:</p>
                  <ul>
                    <li>Ask <strong>what information</strong> the company holds about them and why.</li>
                    <li>Ask<strong> how to gain access</strong> to it.</li>
                    <li>Be informed <strong>how to keep it up to date.</strong></li>
                    <li>Be informed how the company is<strong> meeting its data protection obligations.</strong></li>
                  </ul>
                  <p>If an individual contacts the company requesting this information, this is called a subject access request.</p>
                  <p>Subject access requests from individuals should be made by email, addressed to the data controller at [email address]. The data controller can supply a standard request form, although individuals do not have to use this.</p>
                  <p>The data controller will always verify the identity of anyone making a subject access request before handing over any information.</p>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_14 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Disclosing data for other reasons</h1>
                  <p>In certain circumstances, the Data Protection Act allows personal data to be disclosed to law enforcement agencies without the consent of the data subject.</p>
                  <p>Under these circumstances, ZMB Property Investment will disclose requested data. However, the data controller will ensure the request is legitimate, seeking assistance from the board and from the company’s legal advisers where necessary.</p>
                </div>
              </div>
              <div className="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light et_pb_text_align_left">
                <div className="et_pb_text_inner">
                  <h1 className="heading-title">Providing information</h1>
                  <p>ZMB Property Investment aims to ensure that individuals are aware that their data is being processed, and that they understand:</p>
                  <ul>
                    <li>How the data is being used</li>
                    <li>How to exercise their rights</li>
                  </ul>
                  <p>To these ends, the company has a privacy statement, setting out how data relating to individuals is used by the company.</p>
                  <p>[This is available on request. A version of this statement is also available on the company’s website.]</p>
                  <p>The personal data we collect from you will be used for the following purposes:</p>
                  <ul>
                    <li>To send marketing emails displaying discounts and offers</li>
                    <li>To respond to complaints and enquiries</li>
                    <li>To keep you informed regarding any new products or services that may be of interest to you</li>
                    <li>To record order history to see what you might enjoy in the future</li>
                  </ul>
                </div>
              </div>
          </div>
      {/*<div className="container ">
        <div className="row starbuck_row">
          <div className="col-md-6">
             <h1 className="fontweight500 heading_col font_32">Review Pro Solutions Makes It Easy For You To Get More 5 Star Online Reviews</h1>
            <p className="font_16 heading_col fontweight500 width_435">Do you want to increase your sales? Get more 5 star online reviews for your business. More positive online reviews means more customers and increased sales.</p>
            
          <a href="#" className="blue_small_btn" data-toggle="modal" data-target="#myModal"><i className="fa fa-youtube-play youtube_icon pr-2" aria-hidden="true"></i>Click to Watch Signup Video</a>
          </div>  
          <div className="col-md-6">
            
            <h6 className="heading_col font_16">Privacy Policy</h6>
            <p className="txt_col fontweight500 font_12 mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    
          </div>
        </div>
      </div>*/}

      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default privacypolicy;