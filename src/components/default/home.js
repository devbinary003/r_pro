import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH, IS_ACTIVE, USER_ID, USER_ROLE } from '../../constants';
import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

import logoWhite from '../../images/30days_logo.png';
//import headervideo from '../../images/reviewpro/27633919-preview.mp4';
import headervideo from '../../images/reviewpro/NewEditReviewsPro.mp4';

import registerLogo from '../../images/register_logo.png';

import oneicon from '../../images/reviewpro/1icon.png';
import twoicon from '../../images/reviewpro/icon2.png';
import threeicon from '../../images/reviewpro/icon3.png';
import fouricon from '../../images/reviewpro/4icon.png';
import fifthicon from '../../images/reviewpro/5icon.png';

//import display from '../../images/reviewpro/imgpsh_fullsize_anim_2.png';
import display from '../../images/reviewpro/latestimg/img2.png';
//import collect from '../../images/reviewpro/imgpsh_fullsize_anim.png';
import collect from '../../images/reviewpro/latestimg/img3.png';
//import request1 from '../../images/reviewpro/imgpsh_fullsize_anim_3.png';
import request1 from '../../images/reviewpro/latestimg/imgnew1.png';

import girl_img from '../../images/reviewpro/girl_img.jpg';
import smile_boy from '../../images/reviewpro/smile_boy.jpg';
import winter_girl from '../../images/reviewpro/pexels_photo_324658.jpg';
import Oppenheimer from '../../images/reviewpro/Top_SA_Nicky_Oppenheimer.jpg';
import black_boy_image from '../../images/reviewpro/black_boy_image.jpg';

import contactbg from '../../images/reviewpro/formbg.jpg';

import pricingbgimg from '../../images/reviewpro/pricing_bg_img.png';
//import topbannerimg from '../../images/reviewpro/top_banner_img.jpg';
import visa from '../../images/reviewpro/visa.png';
import amex from '../../images/reviewpro/amex.png';
import mastercard from '../../images/reviewpro/mastercard.png';
import paypal from '../../images/reviewpro/paypal.png';

const ContactMail = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/contact', {
            method: 'post',
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const alertStyle = {
  color: 'red',
};

class home extends React.Component {

  state = {
        fields: {},
        errors: {},
        Loading: false,
        first_name: '',
        last_name: '',
        email:'',
        company:'',
        help_you:'',
        message:'',
  };

  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }
  handleSubmit = event => {
      event.preventDefault();
  }
  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.first_name) {
        formIsValid = false;
        errors["first_name"] = "*Please enter first name.";
      }

      if (!this.state.last_name) {
        formIsValid = false;
        errors["last_name"] = "*Please enter last name.";
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.company) {
        formIsValid = false;
        errors["company"] = "*Please enter company.";
      }

      if (!this.state.help_you) {
        formIsValid = false;
        errors["help_you"] = "*Please enter how we can help you.";
      }

      if (!this.state.message) {
        formIsValid = false;
        errors["message"] = "*Please enter message.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.setState({ Loading: true }); 
      this.sendContactMail();
    }
    //alert('hello');
  }

  sendContactMail = () => {
  const data = {
      first_name: this.state.first_name,
      last_name:this.state.last_name,
      email:this.state.email,
      company: this.state.company,
      help_you: this.state.help_you,
      message: this.state.message,
  }
  ContactMail(data)
      .then(res => {
          if (res.status==true) {
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
              toast.error(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }

  componentDidMount() {

  }

    render(){
      const {Loading} = this.state;
      return (

      <div>
      
      <Route component={Header} />
      <header className="top_banner_section">
        <div className="container">
          <div className="row">
            <div className="col-sm-7 top_banner_left_content_sec">
              {/*<h2 className="top_banner_head">Reap the multiple rewards and benefits of online reviews</h2>
              <p className="top_banner_sub_text">Gain business exposure, increased online visibility, and more positive customer sentiments to improve your ROI. Put yourself on the fastest path to success.</p>*/}
              <h2 className="top_banner_head">Reap the benefits of online reviews</h2>
              <p className="top_banner_sub_text">Gain business exposure and increased online visibility through positive reviews.</p>
              <a className="free_reputation_btn" href="/contact">Free Online Reputation Report</a>
              <a className="free_reputation_btn" href="/#videosection">How It Works</a>
            </div>
            <div className="col-md-5">
            </div>
          </div>
        </div>
        {/*<div className="overlay"></div>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
          <source src={headervideo} type="video/mp4" />
        </video>
        <div className="container h-100">
          <div className="d-flex h-100 text-center align-items-center">
            <div className="w-100 text-white">
              <h2 className="banner-title">
                Experience the Fastest<br/> Way to Get Online<br/> Reviews
              </h2>
              <p className="banner-subhead">
                Request reviews,improve your visibility and increase your sales with our simple , 2 step method.
              </p>
              <button className="getstarted">Get Started for free</button>
            </div>
          </div>
        </div>*/}
      </header>
      <div className="feature">
         <h2 className="feature-title">Our Features</h2>
         <p className="feature-description">Features that helps grow your business</p>
         <div className="row container feature-box">
            <div className="col-md-4">
               <div className="card" data-aos="fade-up" data-aos-duration="3000">
                  <img src={oneicon} className="icons" />
                  <h4 className="card-title">Business Insights</h4>
                  <p className="card-desc">
                     Monitor essential areas of business success and areas of improvement using customer referral marketing and our professional review management software.
                  </p>
               </div>
            </div>
            <div className="col-md-4">
               <div className="card" data-aos="fade-up" data-aos-duration="3000">
                  <img src={twoicon} className="icons" />
                  <h4 className="card-title">Get Alerts</h4>
                  <p className="card-desc">
                     Receive instant notifications the moment your business gets reviewed and respond straightaway via our app.
                  </p>
               </div>
            </div>
            <div className="col-md-4">
               <div className="card" data-aos="fade-up" data-aos-duration="3000">
                  <img src={threeicon} className="icons" />
                  <h4 className="card-title">Mobile App</h4>
                  <p className="card-desc">
                     Quickly manage your reviews and communicate on the go – directly answer customer responses from a mobile device.
                  </p>
               </div>
            </div>
            <div>
            </div>
         </div>
         <div className="row container feature-box second_boxes_section">
            <div className="col-md-2">
            </div>
            <div className="col-md-4">
               <div className="card" data-aos="fade-up" data-aos-duration="3000">
                  <img src={fouricon} className="icons" />
                  <h4 className="card-title">Dynamic Dashboard</h4>
                  <p className="card-desc">
                     Easily navigate around the dashboard to view data or customer feedback in an instant.Track notable trends from your consumer and clients’ reviews to hugely develop your business.
                  </p>
               </div>
            </div>
            <div className="col-md-4">
               <div className="card" data-aos="fade-up" data-aos-duration="3000">
                  <img src={fifthicon} className="icons" />
                  <h4 className="card-title">Receive New Customers</h4>
                  <p className="card-desc">
                     Revolutionize your reputation – make use of customer referral marketing and receive consumers that have heard positive recommendations about you.
                  </p>
               </div>
            </div>
            <div className="col-md-2"></div>
            <div>
            </div>
         </div>
      </div>
      <div className="container video-section" id="videosection">
        <div className="row">
          <div className="col-sm-6">
            <video width="100%" controls className="video_sec">
              <source src={headervideo} type="video/mp4" />
            </video>
          </div>
          <div className="col-sm-6 video_right_content_section">
            <h2 className="video_right_head_section">Watch Our Video Section</h2>
            <p className="video_right_text">Find out in our video just why your online reputation is essential to business success and how our software can put you on the right track.</p>
            { localStorage.getItem(USER_ID)==null 
              ?
                <a href="/signup" className="getstarted learn_more_btn">Sign Up</a>
              :
                <div></div>
            }
          </div>
        </div>
      </div>
      <div className="container reviews">
         <div className="row collect desktop">
            <div className="col-md-6">
               <h3 className="reviews-title">Collect Reviews</h3>
               <p className="reviews-desc">
                  Discover the fastest, most effective way to pull together multiple reviews. Send out requests to your customers and clients within minutes and automate your follow-ups with Review Pro Solutions to make the process as efficient as it can be.
               </p>
               <p className="reviews-desc">
                  Collect your reviews in seconds and discover for yourself that gathering reviews can be instant and easy.
               </p>
               { localStorage.getItem(USER_ID)==null 
                 ?
                  <a href="/signup" className="getstarted learn_more_btn">Sign Up</a>
                 :
                  <div></div>
               }
            </div>
            <div className="col-md-6">
               <img src={collect} className="review-img" data-aos="fade-left"  data-aos-duration="3000" />
            </div>
         </div>
         <div className="row collect mobile">
            <div className="col-md-6">
               <img src={collect} className="review-img" />
            </div>
            <div className="col-md-6">
               <h3 className="reviews-title">Collect Reviews</h3>
               <p className="reviews-desc">
                  Get more online reviews by letting customers rate your business directly from your website. Collect additional feedback from unsatisfied customers to help improve your business.
               </p>
               { localStorage.getItem(USER_ID)==null
                 ?
                  <a href="/signup" className="getstarted learn_more_btn">Sign Up</a>
                 :
                  <div></div>
               }
            </div>
         </div>
         <div className="row request">
            <div className="col-md-6">
               <img src={request1} className="review-img" data-aos="fade-right"  data-aos-duration="3000" />
            </div>
            <div className="col-md-6">
               <h3 className="reviews-title">Manage Reviews</h3>
               <p className="reviews-desc">
                  With the Review Pro Solutions platform, managing your business reviews could not be easier. Track and manage all of your reviews from our straightforward platform. Collate, organize and respond to reviews with top speed and accuracy in real-time, and raise your retention and customer service.
               </p>
               { localStorage.getItem(USER_ID)==null
                 ?
                  <a href="/signup" className="getstarted learn_more_btn">Sign Up</a>
                 :
                  <div></div>
               }
            </div>
         </div>
         <div className="row display collect desktop">
            <div className="col-md-6">
               <h3 className="reviews-title">Display Reviews</h3>
               <p className="reviews-desc">
                  Choose our efficient software to gather and show off qualified leads while building and increasing the confidence of your consumers all at once.
               </p>
               <p className="reviews-desc">
                  Your business’s success depends on online reviews. Display them on your website via the Review Pro Solutions platform to significantly boost your reputation and bring in more clients.
               </p>
               { localStorage.getItem(USER_ID)==null
                 ?
                  <a href="/signup" className="getstarted learn_more_btn">Sign Up</a>
                 :
                  <div></div>
               }
            </div>
            <div className="col-md-6">
               <img src={display} className="review-img" data-aos="fade-left"  data-aos-duration="3000" />
            </div>
         </div>
      </div>

      <div className="container">
        <div className="row starbuck_row">
          <div className="pricing_table_heading_section">
            <h3 className="pricing_table_heading">Take charge of your reviews – manage them professionally.</h3>
          </div>
          <div className="pricing_background_img_section">
              {/*<img src={pricingbgimg} />*/}
          </div>
          <div className="pricing_header_section">
            <h3 className="pricing_main_head_text">Simple Pricing with Review Pro</h3>
            <p className="pricing_sub_head_text">Pricing for all budgets</p>
          </div>
          <div className="table-responsive pricing_table_section">
            <div className="membership-pricing-table">
              <table>
                <tbody>
                  <tr>
                    <th className="pricing-plan-first-box-section">
                      <div className="pricing-plan-name-first-head">Pricing <br />Table</div>
                    </th>
                    <th className="plan-header plan-header-blue">
                      <div className="pricing-plan-name">Business <br />Lite</div>
                      <div className="pricing-plan-price">
                          <sup>$</sup>79<span>mo</span>
                      </div>
                    </th>
                    <th className="plan-header plan-header-standard">
                      <div className="header-plan-inner">
                        <div className="pricing-plan-name">Business Pro <br />( Our Most Popular)</div>
                        <div className="pricing-plan-price">
                            <sup>$</sup>129<span>mo</span>
                        </div>
                        <div className="pricing-plan-recommended-head">(RECOMMENDED)</div>
                      </div>
                    </th>
                    <th className="plan-header plan-header-blue">
                      <div className="pricing-plan-name">Business Pro <br />Premium</div>
                      <div className="pricing-plan-price">
                          <sup>$</sup>179<span>mo</span>
                      </div>
                    </th>
                  </tr>
                  <tr className="main_heading_section">
                    <td className="main_heading">Users:</td>
                    <td>1 user</td>
                    <td>Up to 10 user</td>
                    <td>Unlimited</td>
                  </tr>
                  <tr>
                    <td>Review Monitoring Google/Facebook/Yelp:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Cloud Desktop platform:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Text Invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Email invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr className="main_heading_section">
                    <td className="main_heading">Locations:</td>
                    <td>1 location</td>
                    <td>Up to 2 locations</td>
                    <td>Up to 5 locations</td>
                  </tr>
                  <tr>
                    <td>Customer Service Support:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Review Alerts:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>On demand Reports:</td>
                    <td><i className="fa fa-times" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Automated Review Invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Mobile App  Apple / Android:</td>
                    <td><i className="fa fa-times" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Negative Review Allert:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Negative Review Protection:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr className="signup_btn_main_sec">
                  { 
                    localStorage.getItem(USER_ID) == null
                    ?
                      <>
                        <td className="signup_btn_sub_sec"></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/signup">Signup</a></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/signup">Signup</a></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/signup">Signup</a></td>
                      </>
                    :
                      localStorage.getItem(USER_ROLE)==='2'
                      ?
                        <>
                          <td className="signup_btn_sub_sec"></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/userdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/userdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/userdashboard">Dashboard</a></td>
                        </>
                      :
                        <>
                          <td className="signup_btn_sub_sec"></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/staffdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/staffdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/staffdashboard">Dashboard</a></td>
                        </>
                  }
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      {/*<section className="testimonial-section2">
         <div className="row text-center">
            <div className="col-12">
               <div className="h2">Our Clients Reviews</div>
               <div className="test-subhead">Your kind words means so much for us</div>
            </div>
         </div>
         <div id="testim" className="testim">
            <div className="wrap">
               <span id="right-arrow" className="arrow right fa fa-chevron-right"></span>
               <span id="left-arrow" className="arrow left fa fa-chevron-left "></span>
               <ul id="testim-dots" className="dots">
                  <li className="dot active"></li>
                  <li className="dot"></li>
                  <li className="dot"></li>
                  <li className="dot"></li>
                  <li className="dot"></li>
               </ul>
               <div id="testim-content" className="cont">
                  <div className="active">
                     <div className="img"><img src={girl_img} alt="" /></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                     <div className="h4">Kellie</div>
                     <div className="h4">Brand manager</div>
                     <div className="stars">
                        <span className="fa fa-star" id="star1"></span>
                        <span className="fa fa-star" id="star2"></span>
                        <span className="fa fa-star" id="star3"></span>
                        <span className="fa fa-star" id="star4"></span>
                        <span className="fa fa-star" id="star5"></span>
                     </div>
                  </div>
                  <div>
                     <div className="img"><img src={smile_boy} alt="" /></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                     <div className="h4">Jessica</div>
                     <div className="h4">Brand manager</div>
                     <div className="stars">
                        <span className="fa fa-star" id="star1"></span>
                        <span className="fa fa-star" id="star2"></span>
                        <span className="fa fa-star" id="star3"></span>
                        <span className="fa fa-star" id="star4"></span>
                        <span className="fa fa-star" id="star5"></span>
                     </div>
                  </div>
                  <div>
                     <div className="img"><img src={winter_girl} alt="" /></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                     <div className="h4">Kellie</div>
                     <div className="h4">Brand manager</div>
                     <div className="stars">
                        <span className="fa fa-star" id="star1"></span>
                        <span className="fa fa-star" id="star2"></span>
                        <span className="fa fa-star" id="star3"></span>
                        <span className="fa fa-star" id="star4"></span>
                        <span className="fa fa-star" id="star5"></span>
                     </div>
                  </div>
                  <div>
                     <div className="img"><img src={Oppenheimer} alt="" /></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                     <div className="h4">Jessica</div>
                     <div className="h4">Brand manager</div>
                     <div className="stars">
                        <span className="fa fa-star" id="star1"></span>
                        <span className="fa fa-star" id="star2"></span>
                        <span className="fa fa-star" id="star3"></span>
                        <span className="fa fa-star" id="star4"></span>
                        <span className="fa fa-star" id="star5"></span>
                     </div>
                  </div>
                  <div>
                     <div className="img"><img src={black_boy_image} alt="" /></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                     <div className="h4">Jessica</div>
                     <div className="h4">Brand manager</div>
                     <div className="stars">
                        <span className="fa fa-star" id="star1"></span>
                        <span className="fa fa-star" id="star2"></span>
                        <span className="fa fa-star" id="star3"></span>
                        <span className="fa fa-star" id="star4"></span>
                        <span className="fa fa-star" id="star5"></span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>*/}
      <div className="contact">
        <div className="container">
          <div className="row">
              <div className="col-md-6">
                 <div className="get-in-touch">Get In Touch</div>
                 <div className="cont-subhead">
                    Just fill out the form to get in touch with us. We are here to answer your enquiries.
                 </div>
                 <div className="address">
                   <div className="address"><i className="fa fa-map-marker" aria-hidden="true"></i> 2931 Adams Street Hollywood 33020</div>
                   <div className="email"><i className="fa fa-envelope" aria-hidden="true"></i> info@tryreviewpro.com</div>
                   <div className="call"><i className="fa fa-mobile" aria-hidden="true"></i>  866.776.0259</div>
                 </div>
              </div>
              <div className="col-md-6">
                 <form>
                    <label>
                       <p className="label-txt">FIRST NAME</p>
                       <input type="text" name="first_name" id="first_name" className="input" value={this.state.first_name} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.first_name}</span>
                    </label>
                    <label>
                       <p className="label-txt">LAST NAME</p>
                       <input type="text" name="last_name" id="last_name" className="input" value={this.state.last_name} onChange={this.handleChange} />
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.last_name}</span>
                    </label>
                    <label>
                       <p className="label-txt">EMAIL</p>
                       <input type="text" name="email" id="email" className="input" value={this.state.email} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.email}</span>
                    </label>
                    <label>
                       <p className="label-txt">COMPANY</p>
                       <input type="text" name="company" id="company" className="input" value={this.state.company} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.company}</span>
                    </label>
                    <label className="we_can_help_you">
                       <p className="label-we_can_help_you">How can we help you today?</p>
                       <select name="help_you" id="help_you" className="enquery_field" value={this.state.help_you} onChange={this.handleChange}>
                         <option value="General Enquiry">General Enquiry</option>
                         <option value="Sales Enquiry">Sales Enquiry</option>
                         <option value="Free Online Reputation Report" selected>Free Online Reputation Report</option>
                         <option value="Existing Customer Feedback">Existing Customer Feedback</option>
                         <option value="Technical Difficulties">Technical Difficulties</option>
                         <option value="Other">Other</option>
                       </select>
                       <span style={alertStyle}>{this.state.errors.help_you}</span>
                    </label>
                    <label className="message">
                       <p className="label-txt">MESSAGE</p>
                       {/*<input type="text" className="input" />*/}
                       <textarea name="message" id="message" className="input" value={this.state.message} onChange={this.handleChange}></textarea>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.message}</span>
                    </label>
                    <label className="btn">
                    <button type="button" className="blue_btn_box add_staff_btn mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Send</button>
                    </label>
                 </form>
              </div>
            </div>
         </div>
      </div>
      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default home;