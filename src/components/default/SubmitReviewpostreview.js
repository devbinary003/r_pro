import React, { Component } from 'react';
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL } from '../../constants';

import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Cookies from 'universal-cookie';
import moment from 'moment';
import $ from 'jquery';
import StarRatingComponent from 'react-star-rating-component';

import Footer from './SubmitReviewFooter';
import Header from './SubmitReviewHeader';

import logoWhite from '../../images/logo-white.png';
import registerLogo from '../../images/register_logo.png';
import platformAlx159 from '../../images/Platform-for-Alex-xd_159.png';
import hmStars from '../../images/how_many_stars.png';
import '../../css/businessreviewcss.css';

const getUserDetail = (businessname) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/review/getbusinessdetails/'+businessname, {
            method: 'get'
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const reviewSubmit = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('review/reviewsubmit', {
            method: 'post',
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getReviewerDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('review/checkreviewer', {
            method: 'get',
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
  width: '100%',
  float: 'left'
};

const textStyle = {
  textTransform: 'capitalize',
};


class postreview extends React.Component {
  state = {
        reviewer_name: '',
        reviewer_email: '',
        reviewer_rating: '',
        reviewer_description: '',
        business_user_id: '',
        business_place_id: '',
        business_address: '',
        business_name: '',
        reviewer_res_message: '',
        reviewer_res_status: '',
        business_review_link: '',
        fields: {},
        errors: {},
        enableReviewBtn: false,
    }

handleChange = event => {
  this.setState({ errors:''});
  this.setState({
    [event.target.name]: event.target.value
  });
}

handleBlur = event => {
  const data = {
    userid: localStorage.getItem(USER_ID),
    business_slug: this.props.match.params.businessname,
    reviewer_email: this.state.reviewer_email,
    reviewer_rating: this.state.reviewer_rating
  }
  getReviewerDetails(data)
  .then(res => {
    if(res.status==true){
       this.setState({ enableReviewBtn: true });
    } else {
      toast.error(res.message, {
        autoClose:10000,
        position: toast.POSITION.BOTTOM_RIGHT
      });
    }
  })
  .catch(err => {
      console.log(err);
  });
}

validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.reviewer_rating) {
        formIsValid = false;
        errors["reviewer_rating"] = "*Please select star rating.";
      }

      if (!this.state.reviewer_description) {
        formIsValid = false;
        errors["reviewer_description"] = "*Please enter description.";
      }

      if (!this.state.reviewer_name) {
        formIsValid = false;
        errors["reviewer_name"] = "*Please enter name.";
      }

      if (!this.state.reviewer_email) {
        formIsValid = false;
        errors["reviewer_email"] = "*Please enter email.";
      }

      if (typeof this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email";
        }
      }

      this.setState({
        errors: errors
      });
      return formIsValid;

}

handleSubmit = event => {
    event.preventDefault();
    if (this.validateForm()) {
        const data = {
            name: this.state.reviewer_name,
            email: this.state.reviewer_email,
            reviewer_rating: this.state.reviewer_rating,
            reviewer_description: this.state.reviewer_description,
            business_user_id: this.state.business_user_id,
            business_place_id: this.state.business_place_id,
            business_name: this.props.match.params.businessname,
    }
    reviewSubmit(data)
      .then(res => {
          if (res.status==true) {
                toast.success(res.message, {
                  autoClose:10000,
                  position: toast.POSITION.BOTTOM_RIGHT
                });
                this.setState({ reviewer_name:'', reviewer_email:'', reviewer_rating:'', reviewer_description:'', enableReviewBtn: false, });
                if(res.google_review_link!=''){
                  var google_review_link = 'https://search.google.com/local/writereview?placeid='+res.data.business_place_id;
                  setTimeout(function(){
                    window.location.href = google_review_link; 
                  }, 11000);
                  
                  //window.open = (google_review_link, '_blank');
                } else {

                }
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }

      })
      .catch(err => {
          console.log(err);
      });
        } else
            toast.error('Please fill all mandatory fields.', {
                position: toast.POSITION.BOTTOM_RIGHT
          });
}

componentDidMount() {
  const businessname = this.props.match.params.businessname;
  getUserDetail(businessname)
        .then(res => {
          if(res.status==true){
            var userdetail = res.data;
            this.setState({
              business_user_id: res.user.id,
              business_name: res.data.business_name,
              business_address: res.data.business_address,
              business_place_id: res.data.business_page_id,
              business_review_link: res.data.business_review_link,
            });
          } 
        })
        .catch(err => {
            console.log(err);
        });
//  this.renderClickAlert();
}

onStarClick(nextValue, prevValue, name) {
    this.setState({reviewer_rating: nextValue});
    console.log(this.state.reviewer_rating);
  } 

  render(){
        const { reviewer_rating } = this.state;
  return (
      
    <div>

     <Route component={Header} />

  
      <div className="container ">
      <div className="row starbuck_row justify_center">
        <div className="col-md-12 width_520">
          <h1 className="heading_col font_32 heading_main_text">We would love some feedback about your experience!</h1>

          <form onSubmit={this.handleSubmit} className="needs-validation_form" novalidate>

           <p className="font_16 txt_col fontweight500 heading_sub_text">Please take a moment to leave us a review. Your feedback will help us serve you better in the future.</p>

          <div className="row">
              <div className="col-sm-6">
                <div className="form-group">
                  <StarRatingComponent emptyStarColor={'#ccc'} name="rate1" starCount={5} value={reviewer_rating} onStarClick={this.onStarClick.bind(this)}/>
                  <span style={alertStyle}>{this.state.errors.reviewer_rating}</span>
                </div>
              </div>

               <div className="col-sm-6">
                <div className="form-group">
                  <img src={hmStars} width="100%" className="strPngImg" />
                </div>
              </div>

            </div>

            <div className="row">
              <div className="col-sm-6">
                <div className="form-group">
                  <input type="text" className="form-control input_custom_style" id="reviewer_name" name="reviewer_name" placeholder="Full Name" value={this.state.reviewer_name} onChange={this.handleChange} />
                  <span style={alertStyle}>{this.state.errors.reviewer_name}</span>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="form-group">
                  <input type="email" className="form-control input_custom_style" id="email" name="reviewer_email" placeholder="Email" value={this.state.reviewer_email} onChange={this.handleChange} onBlur={this.handleBlur} />
                  <span style={alertStyle}>{this.state.errors.reviewer_email}</span>
                </div>
              </div>
            </div>
            <div className="form-group">
              <textarea className="form-control input_custom_style" rows="5" id="reviewer_description" name="reviewer_description" placeholder="Write a Review ..." value={this.state.reviewer_description} onChange={this.handleChange}></textarea>
              <span style={alertStyle}>{this.state.errors.reviewer_description}</span>
            </div>
            <button type="submit" className="blue_btn">Submit Review</button>
          </form>
        </div>
        
      </div>
    </div>

    <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

    </div>

      );
    }

}

export default postreview;
