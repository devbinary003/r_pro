import React from 'react';
import {Route} from 'react-router-dom';
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

class terms extends React.Component {

  state = {
        fields: {},
        errors: {},
  };

    render(){
      
      return (

      <div>
      
      <Route component={Header} />

      {/*<div className="container ">
        <div className="row starbuck_row">*/}
          <div className="row heading">
            <h2>Terms And Conditions</h2>
         </div>
         <div className="row terms">
           <ul>
              <li>
                 1. Access to and use of any information on this website is conditional on your acceptance of these website use conditions without modification. Please read them carefully. We recommend you print out and keep a copy of them for your future reference. If you do not wish to accept any part of them, you must not use our website. All bookings of course, workshop or tour arrangements are also subject to our Booking Conditions shown above. Again, we recommend you print out and keep a copy of these for your future reference.
              </li>
              <li>
                2. In these Conditions, ‘you’ and ‘your’ means any and all persons using this website ‘We’, ‘us’ and ‘our’ means Aspect2i Limited.
              </li>
              <li>
                3. Nothing on this website constitutes an offer on our part. The matters detailed constitute an invitation to you to make an offer to us on the stated terms to purchase arrangements we feature. We may accept or decline any such offer. All arrangements featured or referred to are at all times prior to specific confirmation subject to availability and no warranties, promises or representations are given as to availability.
              </li>
              <li>
                 4. As a condition of your use of this website, you warrant to us that you will not use it or any material or information on it for any purpose that is unlawful or prohibited by these Conditions.
              </li>
              <li>
                 5. This website is for your personal and non-commercial use. No part of this website may be reproduced in any form without our prior consent, other than temporarily in the course of using our service or to keep a record of a transaction entered into using our service. You may not modify, copy, distribute, transmit, display, reproduce, publish, license, create derivative works from, transfer, sell or in any other way use any material, information, products or services contained or featured on this website. The copyright in the material contained on this site belongs to us or its licensors.              </li>
              <li>
                 6. We are an English registered Company. Our business and the services we offer are governed exclusively by the applicable laws of England and Wales except where otherwise stated in our Booking Conditions. No warranties and/or representations of any kind, express or implied, are given as to the compliance of the information shown on this website, the services offered by or on behalf of us, any information relating to such services and/or our business in any respect with any laws of any other country. Such laws do not, in any event, affect or apply to the same.
              </li>
              <li>
                 7. Access to this website is conditional on your agreement that all information contained in it and all matters which arise between you and us will be governed by English law. Access is further conditional on your agreement that any dispute or matter which arises between you and us will be dealt with by the courts of England and Wales only to the exclusion of the courts of any other country.
              </li>
              <li>
                 8. We accept responsibility for any course, workshop or tour arrangements booked with us in accordance with our then current, applicable Booking Conditions from the time a binding legal contract between us comes into existence. We cannot, however, accept any other liability whatsoever.
              </li>
              <li>
                 9. Except as set out in clause 8 of these Conditions above, no warranties, promises and/or representations of any kind, express or implied, are given as to the accuracy or completeness of any of the material or information contained on this website or as to the nature, standard, suitability or otherwise of any services offered by us or on our behalf. We shall not be liable for any loss or damage or other sum or claim of any nature whatsoever (direct, indirect, consequential or other) which arises, directly or indirectly, in connection with this website including, for the avoidance of doubt and not by way of limitation, any use of any information or material contained in this website or any inability to access or use (or delay in doing so) this website.
              </li>
              <li>
               10. All exclusions of liability apply only to the extent permitted by law and where consistent with clause 8 of these Conditions.
              </li>
              <li>
             11. If any exclusion(s) or limitation(s) contained in these Conditions is found, in whole or part, to be unlawful, void or for any other reason unenforceable for any purpose(s), that exclusion(s) or limitation(s) or the part(s) in question shall be deemed severable and omitted from these Conditions for that purpose / those purposes. Such omission shall not affect the validity, effectiveness or enforceability of the other provisions of these Conditions.
              </li>
              <li>
               12. Without prejudice to the foregoing provisions, we are entitled to the benefit of any applicable exclusions and/or limitations of liability permitted by the laws of any country found to be applicable to the information shown on this web site and/or any services offered by us or on our behalf.
              </li>
              <li>
               13. The information contained on this website may contain technical inaccuracies and typographical and other errors. The information on these pages may be updated from time to time and may at times be out of date. We have the right to change the prices of the course, workshop or tour arrangements featured or mentioned on this website at any time without prior notice. If any price is obviously incorrect, we will not be bound by it. We accept no responsibility for keeping the information on these pages up to date or liability for failure to do so. You must ensure you check all details of the chosen course, workshop or tour arrangements (including the price) with us by telephone or other approved means at the time of booking.
              </li>
              <li>
               14. This website may contain links to other websites. Except where they belong to us, such other websites are not under our control or maintained by us. We are not responsible for the content of such websites. We provide these links for your convenience only but do not monitor or endorse the material on them. We cannot accept any liability whatsoever and howsoever arising in relation to any such other websites (including, for the avoidance of doubt and not by way of limitation, any inability to access or delay in accessing any such other website) or in relation to any material or information appearing on them or which you may otherwise come across after leaving our site by way of a hypertext link or any other means.

              </li>
              <li>
               15. We make no warranty that this website (or any websites that are linked to this website) is free from technical errors, computer viruses or any other malicious or impairing computer programs. It is your responsibility to ensure you carry out sufficient checks (including virus checks) to satisfy your particular requirements.
              </li>
              <li>
               16. We may alter these Conditions at any time. If we do so, all subsequent use of our website will be governed by the newer version. You must check these Conditions regularly.
              </li>

           </ul>
         </div>
          {/*<div className="col-md-6">
             <h1 className="fontweight500 heading_col font_32">Review Pro Solutions Makes It Easy For You To Get More 5 Star Online Reviews</h1>
            <p className="font_16 heading_col fontweight500 width_435">Do you want to increase your sales? Get more 5 star online reviews for your business. More positive online reviews means more customers and increased sales.</p>
            
          <a href="#" className="blue_small_btn" data-toggle="modal" data-target="#myModal"><i className="fa fa-youtube-play youtube_icon pr-2" aria-hidden="true"></i>Click to Watch Signup Video</a>
          </div>  
          <div className="col-md-6">
            
            <h6 className="heading_col font_16">Terms of Service</h6>
            <p className="txt_col fontweight500 font_12 mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    
          </div>
        </div>
      </div>*/}

      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default terms;