import React, { Component } from 'react';
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID, IMAGE_URL } from '../../constants';

import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Cookies from 'universal-cookie';
import moment from 'moment';
import $ from 'jquery';
import StarRatingComponent from 'react-star-rating-component';

import Footer from './SubmitReviewFooter';
import Header from './SubmitReviewHeader';
import '../../css/businessreviewcss.css';

import logoWhite from '../../images/logo-white.png';
import registerLogo from '../../images/register_logo.png';
import platformAlx159 from '../../images/Platform-for-Alex-xd_159.png';

class home extends React.Component {
  state = {
        fields: {},
        errors: {},
    }

  componentDidMount() {
  } 

    render(){

    return (
      
      <div>

      <Route component={Header} />

      <div className="container ">
        <div className="row">
          <div className="col-md-12 profile_not_found">
            <h1 className="fontweight500 heading_col font_32">Sorry not a valid profile url!</h1>
          </div> 
          
        </div>
      </div>

      <Route component={Footer} />
    
   <ToastContainer autoClose={5000} /> 
  </div>

      );
    }

}

export default home;
