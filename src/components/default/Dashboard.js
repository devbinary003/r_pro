import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Route, Switch, Redirect } from 'react-router-dom';

import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID, USER_ROLE, RGSTEP1, RGSTEP2, RGSTEP3, RGSTEP4} from '../../constants';
import PrivateRoute from '../PrivateRoute.js';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import findbusiness from '../user/findbusiness';
import businessinfo from '../user/businessinfo';
import customizereview from '../user/customizereview';
import billing from '../user/billing';
import userdashboard from '../user/userdashboard';
import staffdashboard from '../user/staffdashboard';
import accountsettings from '../user/accountsettings';
import systemsettings from '../user/systemsettings';
import livereviews from '../user/livereviews';
import lvsatisfiedreviews from '../user/lvsatisfiedreviews';
import lvunsatisfiedreviews from '../user/lvunsatisfiedreviews';
import internalreviews from '../user/internalreviews';
import managestaff from '../user/managestaff';
import addstaff from '../user/addstaff';
import editstaff from '../user/editstaff';
import staffinvitecustomerreport from '../user/staffinvitecustomerreport';
import addinvitecustomer from '../user/addinvitecustomer';
import viewinvitecustomer from '../user/viewinvitecustomer';
import getreviews from '../user/staffgetreviews';
import satisfiedreviews from '../user/satisfiedreviews';
import unsatisfiedreviews from '../user/unsatisfiedreviews';
import addbusinesslocation from '../user/addbusinesslocation';
import businesslocationlist from '../user/businesslocationlist';
import editbusinesslocation from '../user/editbusinesslocation';
import search from '../user/search';
import sendreviewinvite from '../user/sendreviewinvite';

const getUserDeatils = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user/detail/'+localStorage.getItem(USER_ID), {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Dashboard extends React.Component {

   state = {
        reg_step_1: '',
        reg_step_2: '',
        reg_step_3: '',
        reg_step_4: '',
        isprofilecomplete: '',
    }

    componentDidMount() {
    getUserDeatils()
      .then(res => {
          if(res.status===true){
              var userdata = res.data;
              this.setState({
                reg_step_1: userdata.reg_step_1,
                reg_step_2: userdata.reg_step_2,
                reg_step_3: userdata.reg_step_3,
                reg_step_4: userdata.reg_step_4,
                isprofilecomplete: userdata.isprofilecomplete,
              });
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
      })
      .catch(err => {
          console.log(err);
      });
  }

    render() { 

        let user_role = localStorage.getItem(USER_ROLE); 
        let reg_step_1 = localStorage.getItem(RGSTEP1); 
        let reg_step_2 = localStorage.getItem(RGSTEP2); 
        let reg_step_3 = localStorage.getItem(RGSTEP3); 
        let reg_step_4 = localStorage.getItem(RGSTEP4); 
        let userlink;
        let isprofilecomplete = this.state.isprofilecomplete; 
        if(user_role === '1'){
          userlink = <>
                  <Route/>
                  <Switch>
                    
                  </Switch>
                </>
        }
        if(user_role === '2'){
          if(isprofilecomplete === '1') {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact />
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />
                       
                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />
                      
                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                      <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                      <Redirect to="/userdashboard" />
                    </Switch>
                  </>
          } else {
          if(reg_step_1 !== '1') {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact />
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />

                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />
                      
                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                       <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                       <PrivateRoute path='/billing' component={billing} user={this.props.user} exact />
                       <PrivateRoute path='/customizereview' component={customizereview} user={this.props.user} exact />
                       <PrivateRoute path='/businessinfo' component={businessinfo} user={this.props.user} exact />
                       <PrivateRoute path='/findbusiness' component={findbusiness} user={this.props.user} exact />
                       <Redirect to="/findbusiness" />
                    </Switch>
                  </>
          } else if(reg_step_2 !== '1') {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact />
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />

                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />

                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                       <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                       <PrivateRoute path='/billing' component={billing} user={this.props.user} exact />
                       <PrivateRoute path='/customizereview' component={customizereview} user={this.props.user} exact />
                       <PrivateRoute path='/findbusiness' component={findbusiness} user={this.props.user} exact />
                       <PrivateRoute path='/businessinfo' component={businessinfo} user={this.props.user} exact />
                       <Redirect to="/businessinfo" />
                    </Switch>
                  </>
          } else if(reg_step_3 !== '1') {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact /> 
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />

                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />

                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                       <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                       <PrivateRoute path='/billing' component={billing} user={this.props.user} exact />
                       <PrivateRoute path='/businessinfo' component={businessinfo} user={this.props.user} exact />
                       <PrivateRoute path='/findbusiness' component={findbusiness} user={this.props.user} exact />
                       <PrivateRoute path='/customizereview' component={customizereview} user={this.props.user} exact />
                       <Redirect to="/customizereview" />
                    </Switch>
                  </>
          } else if(reg_step_4 !== '1') {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact />
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />
                       
                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />


                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                       <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                       <PrivateRoute path='/customizereview' component={customizereview} user={this.props.user} exact />
                       <PrivateRoute path='/businessinfo' component={businessinfo} user={this.props.user} exact />
                       <PrivateRoute path='/findbusiness' component={findbusiness} user={this.props.user} exact />
                       <PrivateRoute path='/billing' component={billing} user={this.props.user} exact />
                       <Redirect to="/billing" />
                    </Switch>
                  </>
          } else {
            userlink = <>
                  <Route/>
                    <Switch>
                       <PrivateRoute path='/search/:id' component={search} user={this.props.user} exact />
                       <PrivateRoute path='/editbusinesslocation/:id' component={editbusinesslocation} user={this.props.user} exact />
                       <PrivateRoute path='/businesslocations' component={businesslocationlist} user={this.props.user} exact />
                       <PrivateRoute path='/addlocation' component={addbusinesslocation} user={this.props.user} exact />
                       
                       <PrivateRoute path='/unsatisfiedreviews/:id' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/unsatisfiedreviews' component={unsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews/:id' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/satisfiedreviews' component={satisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews/:id' component={internalreviews} user={this.props.user} exact />
                       <PrivateRoute path='/internalreviews' component={internalreviews} user={this.props.user} exact />

                       <PrivateRoute path='/managestaff' component={managestaff} user={this.props.user} exact />
                       <PrivateRoute path='/addstaff' component={addstaff} user={this.props.user} exact />
                       <PrivateRoute path='/editstaff/:id' component={editstaff} user={this.props.user} exact />
                       <PrivateRoute path='/staffinvitecustomerreport/:id' component={staffinvitecustomerreport} user={this.props.user} exact />
                       <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />

                       <PrivateRoute path='/lvsatisfiedreviews/:id' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvsatisfiedreviews' component={lvsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews/:id' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/lvunsatisfiedreviews' component={lvunsatisfiedreviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews/:id' component={livereviews} user={this.props.user} exact />
                       <PrivateRoute path='/livereviews' component={livereviews} user={this.props.user} exact />
                       
                       <PrivateRoute path='/systemsettings' component={systemsettings} user={this.props.user} exact />
                       <PrivateRoute path='/accountsettings' component={accountsettings} user={this.props.user} exact />
                      <PrivateRoute path='/userdashboard' component={userdashboard} user={this.props.user} exact />
                      <Redirect to="/userdashboard" />
                    </Switch>
                  </>
             }
          }    
        }
        if(user_role === '3'){
          userlink = <>
                  <Route/>
                    <Switch>
                      <PrivateRoute path='/viewinvitecustomer/:id' component={viewinvitecustomer} user={this.props.user} exact />
                      <PrivateRoute path='/addinvitecustomer' component={addinvitecustomer} user={this.props.user} exact />
                      <PrivateRoute path='/getreviews' component={getreviews} user={this.props.user} exact />
                      <PrivateRoute path='/sendreviewinvite' component={sendreviewinvite} user={this.props.user} exact />
                      <PrivateRoute path='/staffdashboard' component={staffdashboard} user={this.props.user} exact />
                      <Redirect to="/staffdashboard" />
                    </Switch>
                  </>
        } 
        console.log(this.props.user);
        return (
            <>
            
             {userlink}

            </>
        );
    }
}


const mapStateToProps = (state /*, ownProps*/) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(userActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);