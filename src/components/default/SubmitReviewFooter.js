import React, { Component } from 'react';
import logoWhite from '../../images/rs_logo_footer.png';
import '../../css/businessreviewcss.css';

class Footer extends React.Component {

    render() {

        return (  
        <>  
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
        <div className="container-fluid">
        <a className="navbar-brand" href="javascript:void(0)"><img src={logoWhite} className="logo_img"/></a>
        <div className="collapse navbar-collapse footer_section" id="navb">
          <ul className="navbar-nav mr-auto">
          </ul>
          <ul className="navbar-nav footer_copyright_section">
            <li className="nav-item">
              <a className="nav-link" href="javascript:void(0)">Copyright © 2020 - Tryreviewpro.com</a>
            </li>
          </ul>
        </div>
      </div>
      </nav>
        </>
        );
    }
}

export default Footer;
