import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH, IS_ACTIVE, } from '../../constants';
import { validateUserToken } from '../PrivateRoute';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './Header.js';
import Footer from './Footer.js';

import logoWhite from '../../images/Logo-01.png';

const ContactMail = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/contact', {
            method: 'post',
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const alertStyle = {
  color: 'red',
};

class contact extends React.Component {

  state = {
        fields: {},
        errors: {},
        Loading: false,
        first_name: '',
        last_name: '',
        email:'',
        company:'',
        help_you:'',
        message:'',
  };
  handleChange = event => {
      this.setState({
        [event.target.name]: event.target.value
      });
  }
  handleSubmit = event => {
      event.preventDefault();
  }
  validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!this.state.first_name) {
        formIsValid = false;
        errors["first_name"] = "*Please enter first name.";
      }

      if (!this.state.last_name) {
        formIsValid = false;
        errors["last_name"] = "*Please enter last name.";
      }

      if (!this.state.email) {
        formIsValid = false;
        errors["email"] = "*Please enter email address.";
      }

      if (this.state.email !== "undefined") {
        if (!this.state.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
          formIsValid = false;
          errors["email"] = "*Please enter a valid email address";
        }
      }

      if (!this.state.company) {
        formIsValid = false;
        errors["company"] = "*Please enter company.";
      }

      if (!this.state.help_you) {
        formIsValid = false;
        errors["help_you"] = "*Please enter how we can help you.";
      }

      if (!this.state.message) {
        formIsValid = false;
        errors["message"] = "*Please enter message.";
      }

      /*if (typeof this.state.phone !== "undefined") {
        if (!this.state.phone.match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter a valid phone number";
        }
      }*/
 
      this.setState({
        errors: errors
      });
      return formIsValid;
  }

  ValidateBasic = event => {
    if (this.validateForm()==true) {
      this.setState({ Loading: true }); 
      this.sendContactMail();
    }
    //alert('hello');
  }

  sendContactMail = () => {
  const data = {
      first_name: this.state.first_name,
      last_name:this.state.last_name,
      email:this.state.email,
      company: this.state.company,
      help_you: this.state.help_you,
      message: this.state.message,
  }
  ContactMail(data)
      .then(res => {
          if (res.status==true) {
              toast.success(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            } else {
              toast.error(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
            }
          this.setState({ Loading: false });  
      })
      .catch(err => {
          console.log(err);
      });
  }

    render(){
      const {Loading} = this.state;
      
      return (

      <div>
      
      <Route component={Header} />
        <div className="contact">
         <div className="row container">
            <div className="get-in-touch">GET IN TOUCH</div>
            <div className="row">
              <div className="col-md-6">
                 <div className="cont-subhead">
                    {/*Now that you see how it works you can try it yourself even before you activate it.*/}
                    Just fill out the form to get in touch with us. We are here to answer your enquiries.
                 </div>
                 <div className="address">
                   <div className="address"><i className="fa fa-map-marker" aria-hidden="true"></i> 2931 Adams Street Hollywood 33020</div>
                   <div className="email"><i className="fa fa-envelope" aria-hidden="true"></i> info@tryreviewpro.com</div>
                   <div className="call"><i className="fa fa-mobile" aria-hidden="true"></i>  866.776.0259</div>
                 </div>
              </div>
              <div className="col-md-6">
                 <form>
                     <label>
                       <p className="label-txt">FIRST NAME</p>
                       <input type="text" name="first_name" id="first_name" className="input" value={this.state.first_name} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.first_name}</span>
                    </label>
                    <label>
                       <p className="label-txt">LAST NAME</p>
                       <input type="text" name="last_name" id="last_name" className="input" value={this.state.last_name} onChange={this.handleChange} />
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.last_name}</span>
                    </label>
                    <label>
                       <p className="label-txt">EMAIL</p>
                       <input type="text" name="email" id="email" className="input" value={this.state.email} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.email}</span>
                    </label>
                    <label>
                       <p className="label-txt">COMPANY</p>
                       <input type="text" name="company" id="company" className="input" value={this.state.company} onChange={this.handleChange}/>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.company}</span>
                    </label>
                    <label className="we_can_help_you">
                       <p className="label-we_can_help_you">How can we help you today?</p>
                       <select name="help_you" id="help_you" className="enquery_field" value={this.state.help_you} onChange={this.handleChange}>
                         <option value="General Enquiry">General Enquiry</option>
                         <option value="Sales Enquiry">Sales Enquiry</option>
                         <option value="Free Online Reputation Report" selected>Free Online Reputation Report</option>
                         <option value="Existing Customer Feedback">Existing Customer Feedback</option>
                         <option value="Technical Difficulties">Technical Difficulties</option>
                         <option value="Other">Other</option>
                       </select>
                       <span style={alertStyle}>{this.state.errors.help_you}</span>
                    </label>
                    <label className="message">
                       <p className="label-txt">MESSAGE</p>
                       {/*<input type="text" className="input" />*/}
                       <textarea name="message" id="message" className="input" value={this.state.message} onChange={this.handleChange}></textarea>
                       <div className="line-box">
                          <div className="line"></div>
                       </div>
                       <span style={alertStyle}>{this.state.errors.message}</span>
                    </label>
                    <label className="btn">
                    <button type="button" className="blue_btn_box add_staff_btn mt-5" onClick={this.ValidateBasic} disabled={Loading}> {Loading && <i className="fa fa-refresh fa-spin"></i>} Send</button>
                    </label>
                 </form>
              </div>
            </div>
         </div>
      </div>
      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default contact;