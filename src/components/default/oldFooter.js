import React, { Component } from 'react';
import logoWhite from '../../images/Logo-01.png';

class Footer extends React.Component {

    render() {

        return (  
        <>  
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
        <div className="container-fluid">
        <a className="navbar-brand" href="/"><img src={logoWhite} /></a>
        <div className="collapse navbar-collapse" id="navb">
          <ul className="navbar-nav mr-auto">
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="/">Copyright © 2020 - ReviewProSolutions.com</a>
            </li>
          </ul>
        </div>
      </div>
      </nav>
      <div id="myModal" className="modal fade" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              <iframe width="468" height="315" src="https://www.youtube.com/embed/y7rIhwRaiI8" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
            <button type="button" className="close_style" data-dismiss="modal">&times;</button>
          </div>
        </div>
      </div>
      </>
        );
    }
}

export default Footer;
