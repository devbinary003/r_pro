import React from 'react';
import {Route} from 'react-router-dom';
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { USER_ROLE, USER_ID } from '../../constants';
import Header from './Header.js';
import Footer from './Footer.js';

import visa from '../../images/reviewpro/visa.png';
import amex from '../../images/reviewpro/amex.png';
import mastercard from '../../images/reviewpro/mastercard.png';
import paypal from '../../images/reviewpro/paypal.png';

class pricingtable extends React.Component {

  state = {
        fields: {},
        errors: {},
  };

    render(){
      
      return (

      <div>
      
      <Route component={Header} />
      {/*<div className="page_first_header_section">
        <h2 className="main_head_text">Simple Pricing with Review Pro</h2>
        <p className="sub_head_text">Pricing for all budgets</p>
      </div>*/}
      <div className="container ">
        <div className="row starbuck_row">
          {/*<div className="pricing_background_img_section">
              <img src={pricingbgimg} />
          </div>*/}
          <div className="table-responsive">
            <div className="membership-pricing-table">
              <table>
                <tbody>
                  <tr>
                    <th className="pricing-plan-first-box-section">
                      <div className="pricing-plan-name-first-head">Pricing <br />Table</div>
                    </th>
                    {/*<th className="plan-header plan-header-free">
                      <div className="pricing-plan-name">FREE</div>
                      <div className="pricing-plan-price">
                        <sup>$</sup>0<span>.00</span>
                      </div>
                      <div className="pricing-plan-period">month</div>
                    </th>*/}
                    <th className="plan-header plan-header-blue">
                      <div className="pricing-plan-name">Business <br />Lite</div>
                      <div className="pricing-plan-price">
                          <sup>$</sup>79<span>mo</span>
                      </div>
                      {/*<div className="pricing-plan-period">Monthly</div>
                      <div className="pricing-plan-period">Features</div>*/}
                    </th>
                    {/*<th className="plan-heplanader plan-header-blue">
                      <div className="pricing-plan-name">Business Pro <br />( Our Most Popular)</div>
                      <div className="pricing-plan-price">
                          <sup>$</sup>129<span>.00</span>
                      </div>
                      <div className="pricing-plan-period">Monthly</div>
                      <div className="pricing-plan-period">Features</div>
                    </th>*/}
                    <th className="plan-header plan-header-standard">
                      <div className="top_standard_section"></div>
                      <div className="header-plan-inner">
                        {/*<span className="plan-head"> </span>*/}
                        {/*<span className="recommended-plan-ribbon">RECOMMENDED</span>*/}
                        <div className="pricing-plan-name">Business Pro <br />( Our Most Popular)</div>
                        <div className="pricing-plan-price">
                            <sup>$</sup>129<span>mo</span>
                        </div>
                        <div className="pricing-plan-recommended-head">(RECOMMENDED)</div>
                        
                        {/*<div className="pricing-plan-period">Monthly</div>
                        <div className="pricing-plan-period">Features</div>*/}
                      </div>
                    </th>
                    <th className="plan-header plan-header-blue">
                      <div className="pricing-plan-name">Business Pro <br />Premium</div>
                      <div className="pricing-plan-price">
                          <sup>$</sup>179<span>mo</span>
                      </div>
                      {/*<div className="pricing-plan-period">Monthly</div>
                      <div className="pricing-plan-period">Features</div>*/}
                    </th>
                  </tr>
                  {/*<tr>
                    <td></td>
                    <td className="action-header">
                      <a className="btn btn-info">Downgrade</a>
                    </td>
                    <td className="action-header">
                      <a className="btn btn-info">Downgrade</a>
                    </td>
                    <td className="action-header">
                      <div className="current-plan">
                        <div className="with-date">Current Plan</div>
                        <div><em className="smaller block">renews Feb 19, 2015</em></div>
                      </div> 
                    </td>
                    <td className="action-header">
                      <a className="btn btn-success">Upgrade</a>
                    </td>
                    <td className="action-header">
                      <a className="btn btn-info">Upgrade</a>
                    </td>
                  </tr>*/}
                  <tr className="main_heading_section">
                    <td className="main_heading">Users:</td>
                    <td>1 user</td>
                    <td>Up to 10 user</td>
                    <td>Unlimited</td>
                  </tr>
                  <tr>
                    <td>Review Monitoring Google/Facebook/Yelp:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Cloud Desktop platform:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Text Invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Email invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr className="main_heading_section">
                    <td className="main_heading">Locations:</td>
                    <td>1 location</td>
                    <td>Up to 2 locations</td>
                    <td>Up to 5 locations</td>
                  </tr>
                  <tr>
                    <td>Customer Service Support:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Review Alerts:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>On demand Reports:</td>
                    <td><i className="fa fa-times" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Automated Review Invites:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Mobile App  Apple / Android:</td>
                    <td><i className="fa fa-times" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Negative Review Allert:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr>
                    <td>Negative Review Protection:</td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                    <td><i className="fa fa-check" aria-hidden="true"></i></td>
                  </tr>
                  <tr className="signup_btn_main_sec">
                  { 
                    localStorage.getItem(USER_ID) == null
                    ?
                      <>
                        <td className="signup_btn_sub_sec"></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/signup">Signup</a></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/signup">Signup</a></td>
                        <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/signup">Signup</a></td>
                      </>
                    :
                      localStorage.getItem(USER_ROLE)==='2'
                      ?
                        <>
                          <td className="signup_btn_sub_sec"></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/userdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/userdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/userdashboard">Dashboard</a></td>
                        </>
                      :
                        <>
                          <td className="signup_btn_sub_sec"></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/staffdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_red" href="/staffdashboard">Dashboard</a></td>
                          <td className="signup_btn_sub_sec"><a className="sign_up_btn_blue" href="/staffdashboard">Dashboard</a></td>
                        </>
                  }
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          {/*<div className="row footer-btn-section">
              <div className="col-sm-6">.</div>
              <div className="col-sm-2"><a href="#">Select</a></div>
              <div className="col-sm-2"><a href="#">Select</a></div>
              <div className="col-sm-2"><a href="#">Select</a></div>
          </div>*/}
          <div className="pay-methods padded-content margin-div text-center">
            <span className="mob-block mob-auto-line-height">Payment Methods</span>
            <img src={visa} alt="Visa" />
            <img src={amex} alt="American Express" />
            <img src={mastercard} alt="Mastercard" />
            <img src={paypal} alt="Paypal" />
            {/*<div className="small grey">We accept Visa, American Express, Mastercard, Paypal, and Bank Transfers (with Annual billing accounts of Pro and above). To set up a Bank Transfer account, please <a href="/contact">contact support</a>.</div>*/}
          </div>
          <div className="frequently-asked-questions">
            <h2>Frequently Asked Questions</h2>
            <div className="faqs">
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">
                    Why is customer feedback important?
                </button>
                <div className="collapse faq_content_section" id="collapseExample1">
                  <div className="card card-body">
                    <p>The voice of the customer is a very powerful, influential, and essential marketing tool. Brands and businesses that prioritize an open and honest dialogue with their clients or customers via online tools and channels generally perform better, build trust and confidence in their consumers, and boost their overall sales and revenue figures. When customer feedback is positive, it motivates other potential buyers to consider your brand. And while negative feedback may send up some warning flags to prospective clients, these are the reviews that you should use as a business owner to improve on issues that you may not have considered. Purchases are often based on the recommendations, referrals, and reviews of other customers. So proactively acting on this feedback can ensure that you ‘fix’ these identified issues before they become big and impact more customers.</p>
                  </div>
                </div>
              </div>
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                    Do I have to download any software to my computers?
                </button>
                <div className="collapse faq_content_section" id="collapseExample2">
                  <div className="card card-body">
                    <p>No. Review Pro Solutions is a secure, cloud-based software. There is no need to download any additional software to use the app. Installing the application software is fast and easy. It is available from the iOS and Android app-stores and you simply need to download our software from the cloud to begin managing, viewing and displaying all types of reviews for your business.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="faqs">
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample">
                    Do you have a contract requirement?
                </button>
                <div className="collapse faq_content_section" id="collapseExample3">
                  <div className="card card-body">
                    <p>No, our software packages are provided on a month to month, cancel anytime basis. Choose from various options including Lite, Pro, and Premium business review management bundles, and select from our multiple pricing options to unlock the most suitable review management package for your business’s needs.</p>
                  </div>
                </div>
              </div>
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample4" aria-expanded="false" aria-controls="collapseExample">
                    Will you send out a bill every month?
                </button>
                <div className="collapse faq_content_section" id="collapseExample4">
                  <div className="card card-body">
                    <p>We offer the option of sending paperless billing if needed. You can choose to receive electronic bills via email and get notified when renewing Review Pro Solutions software is due.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="faqs">
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample5" aria-expanded="false" aria-controls="collapseExample">
                    Can I change a bad review?
                </button>
                <div className="collapse faq_content_section" id="collapseExample5">
                  <div className="card card-body">
                    <p>Our system allows you to identify any reviews that may be damaging to your business and gives you the opportunity to talk to the customer and fix the customer experience. Prioritizing negative reviews and taking honest feedback into consideration can be important for businesses, and it’s important not to ignore them. But we understand that some negative reviews can harm your reputation and we can help you address that situation if working with a customer does not work out. Google and some other review sites have their own guidelines on removing a bad review.</p>
                  </div>
                </div>
              </div>
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample6" aria-expanded="false" aria-controls="collapseExample">
                    How do I cancel my subscription?
                </button>
                <div className="collapse faq_content_section" id="collapseExample6">
                  <div className="card card-body">
                    <p>Simply email info@tryreviewpro.com and your subscription will be canceled. A confirmation email will be sent to your email inbox to inform you that you have successfully unsubscribed.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="faqs">
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample7" aria-expanded="false" aria-controls="collapseExample">
                    Can I add other locations after my subscription?
                </button>
                <div className="collapse faq_content_section" id="collapseExample7">
                  <div className="card card-body">
                    <p>Yes. As your business grows we can help you add multiple locations to your account. Our review management software packages offers multiple locations. You can choose to add additional ones depending on the requirements of your business as it expands.</p>
                  </div>
                </div>
              </div>
              <div className="faq">
                <button className="btn btn-primary faq_head" type="button" data-toggle="collapse" data-target="#collapseExample8" aria-expanded="false" aria-controls="collapseExample">
                    Can anyone in my office be the admin on the account?
                </button>
                <div className="collapse faq_content_section" id="collapseExample8">
                  <div className="card card-body">
                    <p>Yes. Our system allows you to give permission rights to anyone in your office to handle responses to any reviews. With Review Pro Solutions software you can easily enable other users to act as administrators, and to manage and answer the business reviews that you have received.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*<div className="pay-methods padded-content margin-div text-center">
            <span className="mob-block mob-auto-line-height">Payment Methods</span>
            <img src={visa} alt="Visa" />
            <img src={amex} alt="American Express" />
            <img src={mastercard} alt="Mastercard" />
            <img src={paypal} alt="Paypal" />
            <div className="small grey">We accept Visa, American Express, Mastercard, Paypal, and Bank Transfers (with Annual billing accounts of Pro and above). To set up a Bank Transfer account, please <a href="/contact">contact support</a>.</div>
          </div>*/}
        </div>
      </div>
      <Route component={Footer} />
    
    <ToastContainer autoClose={5000} /> 

  </div>

      );
    }

}

export default pricingtable;