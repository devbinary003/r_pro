export const HOME_PAGE_PATH = '/home';
export const LOGOUT_PAGE_PATH = '/logout';
export const REG_PAGE_PATH = '/signup';
export const LOGIN_PAGE_PATH = '/login';
export const TWENTY_MINUTES_TIME_IN_MILLISECONDS = '59940000';
export const API_TOKEN_NAME = 'api_token';
export const API_TOKEN_EXPIRY_NAME = 'api_token_expiry';
export const USER_ROLE = 'USER_ROLE';
export const USER_ID = 'USER_ID';
export const USER_EMAIL = 'USER_EMAIL';
export const IS_ACTIVE = 'IS_ACTIVE';

export const RGSTEP1 = 'RGSTEP1';
export const RGSTEP2 = 'RGSTEP2';
export const RGSTEP3 = 'RGSTEP3';
export const RGSTEP4 = 'RGSTEP4';

let dynUrl;

if(window.location.hostname =='localhost'){
	dynUrl = 'http://localhost/reviewgrowthapi/api'; 
	//dynUrl = 'https://sponsorkingdom.com/reviewgrowthapi/api/';
} else {
    dynUrl = 'https://tryreviewpro.com/reviewgrowthapi/api/';
}

export const IMAGE_URL = 'https://tryreviewpro.com/reviewgrowthapi/api/public/';
export const ADMIN_IMAGE_URL = 'https://tryreviewpro.com/admin/public/';
export const ADMIN_URL = 'https://tryreviewpro.com/admin/';
export const BASE_URL = dynUrl; 
export const PROFILE_URL = 'https://tryreviewpro.com/br/';


